$(document).ready(function(){
	
	$("img").hide().not(function() {
		return this.complete && $(this).fadeIn();
	}).bind("load", function(){ $(this).fadeIn(); });

	[].slice.call( document.querySelectorAll( 'input.input_field')).forEach( function( inputEl ) {
		if( inputEl.value.trim() !== '' ) {
			$(this).parents('p').addClass('filled-text' );
		}
		inputEl.addEventListener( 'focus', onInputFocus );
		inputEl.addEventListener( 'blur', onInputBlur );
	});

	$('p .eyes a').click(function(){
		if(!$(this).parents('p').hasClass('show-pass')){
			$(this).parents('p').find('input').attr('type','text');
			$(this).parents('p').addClass('show-pass');
			$(this).parents('p').find('.fa').removeClass('fa-eye-slash').addClass('fa-eye');
		}else{
			$(this).parents('p').find('input').attr('type','password');
			$(this).parents('p').removeClass('show-pass');
			$(this).parents('p').find('.fa').removeClass('fa-eye').addClass('fa-eye-slash');
		}
	});
});


function onInputFocus( ev ) {
	$(ev.target.parentNode).addClass('filled-text');
}

function onInputBlur( ev ) {
	if( ev.target.value.trim() === '' ) {
		$(ev.target.parentNode).removeClass('filled-text');
	}
}

function showToast(type, position, content){
    if(type == 'success'){
    	title = "Success!";
    } else if(type == 'warning'){
    	title = "Warning!";
    } else if(type == 'info'){
    	title = "Information!";
    } else if(type == 'error'){
    	title = "Error!";
    }

    if(position == 'top'){
    	newPosition = 'toast-top-right';
    } else if(position == 'bottom'){
    	newPosition = 'toast-bottom-right';
    } else {
    	newPosition = '';
    }

    toastr.options = {
    	positionClass: newPosition || 'toast-top-right'
    }

    toastr[type](content, title);
}