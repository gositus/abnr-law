<?php
/**
 *      @desc CMS integration code: CodeIgniter
 *   @package KCFinder-CodeIgniter
 *   @version 0.3
 *    @author Tiger Fok <tiger@tiger-workshop.com>
 * @copyright 2007-2015 Tiger-Workshop
 *   @license http://opensource.org/licenses/GPL-3.0 GPLv3
 *   @license http://opensource.org/licenses/LGPL-3.0 LGPLv3
 *      @link http://tiger-workshop.com
 */
class CodeIgniter {
    static function getSession() {
        session_start();
        define('ENVIRONMENT', 'development');
        define('BASEPATH', '../../../../../system/');
        define('APPPATH', '../../../../../application/');
        require('../../../../../application/config/database.php');
        require('../../../../../system/core/Common.php');        
        require('../../../../../system/database/DB.php');
        
        $database = DB($db['default']);
        require('../../../../../application/config/config.php');
        
        if (!isset($_COOKIE[$config['sess_cookie_name']])) return;
        $database->where('id', $_COOKIE[$config['sess_cookie_name']]);
        $query = $database->get($config['sess_save_path']);
        $result = $query->row();
        $results = $query->row_array();
 
        if ($result) {
            $ci_session = self::decode_session($result->data);
            if(!empty($ci_session['admin_login'])){
                $_SESSION['admin_login'] = $ci_session['admin_login'];
                $_SESSION['folder_find'] = $config['folder_find'];
            }else{
                unset($_SESSION);
            }
            
            if (!isset($_SESSION['KCFINDER'])) $_SESSION['KCFINDER'] = array();
            
            if (isset($ci_session['KCFINDER'])) {                
                $_SESSION['KCFINDER'] = array_merge($ci_session['KCFINDER'], $_SESSION['KCFINDER']);
            }
        }
    }
    
    static function decode_session($session_data) {
        $return_data = array();
        $offset = 0;
        while ($offset < strlen($session_data)) {
            if (!strstr(substr($session_data, $offset), "|")) {
                throw new Exception("invalid data, remaining: " . substr($session_data, $offset));
            }
            $pos = strpos($session_data, "|", $offset);
            $num = $pos - $offset;
            $varname = substr($session_data, $offset, $num);
            $offset += $num + 1;
            $data = unserialize(substr($session_data, $offset));
            $return_data[$varname] = $data;
            $offset += strlen(serialize($data));
        }
        return $return_data;
    }
}
CodeIgniter::getSession();