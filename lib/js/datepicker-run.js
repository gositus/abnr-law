$(function(){
		$('input.datepicker').datepicker({
        dateFormat: 'dd-M-yy',
    	 changeMonth: true,
	    changeYear: true,
	    yearRange: '-65:-0',
	    minDate: '-65Y',
	    maxDate: '-0Y'
    });
})