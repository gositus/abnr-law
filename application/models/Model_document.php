<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_document extends CI_Model
{

	public function get_list()
	{

		$default_language = setting_value('default_language');
		$query = $this->db->order_by('date_added','DESC')->order_by('id', 'desc')->select("p.* , c.name as name,c.name as c_name,pc.name as category_name,pl.name as publisher_name")
			->join("content_to_publication_category pc", "pc.publication_category_id = p.category", "left")
			->join("publisher pl", "pl.id = p.publisher", "left")
			->join("content_to_" . $this->url . " c", "c.document_id = p.id", "left")
			->get_where($this->url . " p", array('c.language_id' => $default_language, 'pc.language_id' => $default_language, 'p.flag !=' => 3))->result_array();
		return $query;
	}

	public function get_category($table = "publication_category ")
	{

		$default_language = setting_value('default_language');
		$query = $this->db->order_by('p.id', 'desc')
			->select("p.* , c.name as name, c.name as c_name")
			->join("content_to_" . $table . " c", "c.publication_category_id = p.id", "left")
			->get_where($table . " p", array('c.language_id' => $default_language, 'p.flag !=' => 3))->result_array();
		return $query;
	}
	public function get_detail($item_id = "")
	{

		$default_language = setting_value('default_language');
		foreach (language(TRUE)->result_array() as $lang) {
			foreach ($lang as $val) {
				$data[$val] = $this->db->join('content_to_' . $this->url . ' c', 'c.' . $this->url . '_id= p.id', 'left')->get_where($this->url . ' p', array('p.id' => $item_id, 'flag !=' => 3, 'language_id' => $val))->row_array();
			}
		}
		return ($data);
	}

	public function insert()
	{

		$language         = language()->result_array();
		$default_language = setting_value('default_language');
		$default_name     = input_clean($this->input->post('name'));
		// $image            = str_replace(base_url(), "", input_clean($this->input->post('image')));

		$filename = "";

		$image =  file_upload_name('image', 'lib/images/document', strtolower($default_name), FALSE, $this->image_width, $this->image_height);

		if ($image) {
			$filename = $image['file_name'];
		}

		$input = array(
			'image'      => $filename,
			'category' => input_clean($this->input->post('category')),
			'publisher' => input_clean($this->input->post('publisher')),
			'author' => input_clean($this->input->post('author')),
			'publish_date' => input_clean(format_date($this->input->post('publish_date'), TRUE)),
			'due_date' => input_clean(format_date($this->input->post('due_date'), TRUE)),
			'date_added' => date("Y-m-d"),
			'flag'       => input_clean($this->input->post('flag')),
			'flag_memo'  => input_clean($this->input->post('flag_memo'))
		);

		$this->db->insert($this->url, $input);
		$id = $this->db->insert_id();

		foreach ($language as $lang_data) {
			//upload files
			$filename = '';
			$file = upload_anything('file_' . $lang_data['id'], 'files/document/');
			if ($file) {
				$filename = $file['file_name'];
			}

			$data = array(
				'document_id'     => $id,
				'language_id' => $lang_data['id'],
				'file' => $filename,
				'name'        => input_clean($this->input->post('name')),
				'content'     => $this->input->post('description_' . $lang_data['id'])
			);

			$this->db->insert('content_to_' . $this->url, $data);
		}

		$row = $this->db->get_where($this->url, array('id' => $id))->row_array();
		action_log('ADD', $this->url, $row['id'], $default_name, 'ADDED ' . $this->title . ' ( ' . $default_name . ' ) ');
	}

	public function update()
	{

		$id['id']         = input_clean($this->input->post('id'));
		$row = $this->db->get_where($this->url, array('id' => $id['id']))->row_array();

		$language         = language()->result_array();
		$default_language = setting_value('default_language');
		$default_name     = input_clean($this->input->post('name'));


		$image =  file_upload_name('image', 'lib/images/document', strtolower($default_name), FALSE, $this->image_width, $this->image_height);
		if ($image) {
			$filename = $image['file_name'];
			unlink(FCPATH . 'lib/images/document/' . $row['image']);
		} else {
			$filename = $row['image'];
		}
		$input = array(
			'image'      => $filename,
			'category' => input_clean($this->input->post('category')),
			'publisher' => input_clean($this->input->post('publisher')),
			'author' => input_clean($this->input->post('author')),
			'publish_date' => input_clean(format_date($this->input->post('publish_date'), TRUE)),
			'due_date' => input_clean(format_date($this->input->post('due_date'), TRUE)),
			'flag'       => input_clean($this->input->post('flag')),
			'flag_memo'  => input_clean($this->input->post('flag_memo'))
		);

		$this->db->update($this->url, $input, $id);
		$this->db->delete('content_to_' . $this->url, array('document_id' => $id['id']));

		foreach ($language as $lang_data) {
			//upload files
			$filename = '';
			$file = upload_anything('file_' . $lang_data['id'], 'files/document/');

			if ($file) {
				$filename = $file['file_name'];
			} else {
				$filename = $this->input->post('current_file_' . $lang_data['id']);
			}

			$data = array(
				'document_id'     => $id['id'],
				'language_id' => $lang_data['id'],
				'file' => $filename,
				'name'        => input_clean($this->input->post('name')),
				'content'     => $this->input->post('description_' . $lang_data['id'])
			);

			$this->db->insert('content_to_' . $this->url, $data);
		}

		action_log('UPDATE', $this->url, $row['id'], $default_name, 'MODIFY ' . $this->title . ' ( ' . $default_name . ' ) ');
	}
}
