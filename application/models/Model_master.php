	<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_master extends CI_Model {

	public function update_doku(){
		
		$id['id']         = input_clean($this->input->post('id'));
		$default_name     = 'DOKU SETTING';

		setting_update("doku_mode", input_clean($this->input->post('mode')));
		setting_update("doku_mallid",input_clean($this->input->post('mall_id')));
		setting_update("doku_sharedkey", input_clean($this->input->post('shared_key')));

		
		action_log('UPDATE', $this->url, $id['id']  , $default_name, 'MODIFY ' . $default_name . ' ( ' . $default_name . ' ) ');
		

	}

	public function update_midtrans(){
		
		$id['id']         = input_clean($this->input->post('id'));
		$default_name     = 'MIDTRANS SETTING';

		setting_update("midtrans_mode", input_clean($this->input->post('mode')));
		setting_update("midtrans_clientkey", input_clean($this->input->post('client_key')));
		setting_update("midtrans_serverkey", input_clean($this->input->post('server_key')));
		setting_update("midtrans_status", input_clean($this->input->post('status')));

		
		action_log('UPDATE', $this->url, $id['id']  , $default_name, 'MODIFY ' . $default_name . ' ( ' . $default_name . ' ) ');
		

	}

	public function get_all_city(){
		$data = $this->db->query("select c.name as ciname, p.name as prname from master_city c left join master_province p on (c.province_id = p.id)")->result_array();
		return ($data);
	}

	public function insert_banned($table ,$num){
		$row = select_all_row($table, array('id'=>$num),TRUE);
		if(!empty($row['ip'])){
			$ip = select_all_row('master_banned_ip',array('name'=>$row['ip']));
			if(empty($ip)){
				$this->db->insert('master_banned_ip', array('name'=>$row['ip'],'flag_memo'=>'Ban from contact'));
			} else{
				$this->db->update('master_banned_ip', array('flag'=>2), array('name'=> $row['ip']));
			}
		}
		$default_name = 'Banned IP';

		action_log('ADD', $table, $num, $row['ip'], 'ADDED master_banned_ip ( ' . $row['ip'] . ' ) ');


	}
}
?>
