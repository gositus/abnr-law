<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_practice extends CI_Model
{
	public function get_list()
	{

		$default_language = setting_value('default_language');
		$query = $this->db->order_by('id','DESC')->select("p.* , c.name as name,c.name as c_name")
			->join("content_to_practice c", "c.practice_id = p.id", "left")
			->get_where("practice p", array('c.language_id' => $default_language, 'p.flag !=' => 3))->result_array();
		return $query;
	}

    public function get_practice()
    {

        $default_language = setting_value('default_language');
        $query = $this->db->order_by('name','asc')->select("p.* , c.name as name,c.name as c_name")
            ->join("content_to_practice c", "c.practice_id = p.id", "left")
            ->get_where("practice p", array('c.language_id' => $default_language, 'p.flag' => 1))->result_array();
        return $query;
    }

	public function get_detail($item_id = "")
	{

		$default_language = setting_value('default_language');
		foreach (language(TRUE)->result_array() as $lang) {
			foreach ($lang as $val) {
				$data[$val] = $this->db->join('content_to_' . $this->url . ' c', 'c.' . $this->url . '_id= p.id', 'left')->get_where($this->url . ' p', array('p.id' => $item_id, 'flag !=' => 3, 'language_id' => $val))->row_array();
			}
		}
		return ($data);
	}

	public function insert()
	{

		$language         = language()->result_array();
		$default_language = setting_value('default_language');
		$default_name     = input_clean($this->input->post('name_' . $default_language));
		// $image            = str_replace(base_url(), "", input_clean($this->input->post('image')));

		$filename = "";

		// $image =  file_upload_name('image', 'lib/images/practice', strtolower($default_name), FALSE, $this->image_width, $this->image_height);

		// if ($image) {
		// 	$filename = $image['file_name'];
		// }

		$input = array(
			'seo_url'	=> create_seo_url(input_clean($this->input->post('name_' . $default_language)), $this->url),
			// 'image'      => $filename,
			'date_added' => date("Y-m-d"),
			'flag'       => input_clean($this->input->post('flag')),
			'flag_memo'  => input_clean($this->input->post('flag_memo'))
		);

		$this->db->insert($this->url, $input);
		$id = $this->db->insert_id();

		foreach ($language as $lang_data) {
			//upload files
			$filename = '';
			$file = upload_anything('file_' . $lang_data['id'], 'files/practice/');
			if ($file) {
				$filename = $file['file_name'];
			}

			$filename_image = "";
			$filename_right = "";

			$image =  file_upload_name('image_' . $lang_data['id'], 'lib/images/practice', strtolower($default_name), FALSE, $this->image_width, $this->image_height);
			if ($image) {
				$filename_image = $image['file_name'];
			}

			$image_right =  file_upload_name('image_right_' . $lang_data['id'], 'lib/images/practice', strtolower($default_name), FALSE, $this->image_width_right, $this->image_height_right);
			if ($image_right) {
				$filename_right = $image_right['file_name'];
			}

			$data = array(
				'practice_id'     => $id,
				'language_id' => $lang_data['id'],
				'image' 	  => $filename_image,
				'image_right' 	  => $filename_right,
				'file' => $filename,
				'name'        => input_clean($this->input->post('name_' . $lang_data['id'])),
				'content'     => $this->input->post('content_' . $lang_data['id'])
			);

			$this->db->insert('content_to_' . $this->url, $data);
		}

		$row = $this->db->get_where($this->url, array('id' => $id))->row_array();
		action_log('ADD', $this->url, $row['id'], $default_name, 'ADDED ' . $this->title . ' ( ' . $default_name . ' ) ');
	}

	public function update()
	{

		$id['id']         = input_clean($this->input->post('id'));
		$row = $this->db->get_where($this->url, array('id' => $id['id']))->row_array();

		$language         = language()->result_array();
		$default_language = setting_value('default_language');
		$default_name     = input_clean($this->input->post('name_' . $default_language));


		// $image =  file_upload_name('image', 'lib/images/practice', strtolower($default_name), FALSE, $this->image_width, $this->image_height);
		// if ($image) {
		// 	$filename = $image['file_name'];
		// 	unlink(FCPATH . 'lib/images/practice/' . $row['image']);
		// } else {
		// 	$filename = $row['image'];
		// }

		$seo_url = input_clean($this->input->post('seo_url'));
		if (empty($seo_url)) {
			$seo_url = $row['seo_url'];
		}

		$input = array(
			'seo_url'	=> $seo_url,
			// 'image'      => $filename,
			'flag'       => input_clean($this->input->post('flag')),
			'flag_memo'  => input_clean($this->input->post('flag_memo'))
		);

		$this->db->update($this->url, $input, $id);
		$this->db->delete('content_to_' . $this->url, array('practice_id' => $id['id']));

		foreach ($language as $lang_data) {
			//upload files
			$filename = '';
			$file = upload_anything('file_' . $lang_data['id'], 'files/practice/');

			if ($file) {
				$filename = $file['file_name'];
			} else {
				$filename = $this->input->post('current_file_' . $lang_data['id']);
			}

			$filename_image = "";
			$filename_right = "";

			$image =  file_upload_name('image_' . $lang_data['id'], 'lib/images/practice', strtolower($default_name), FALSE, $this->image_width, $this->image_height);
			if ($image) {
				$filename_image = $image['file_name'];
			} else {
				$filename_image = $this->input->post('image_' . $lang_data['id']);
			}

			$image_right =  file_upload_name('image_right_' . $lang_data['id'], 'lib/images/practice', strtolower($default_name), FALSE, $this->image_width_right, $this->image_height_right);
			if ($image_right) {
				$filename_right = $image_right['file_name'];
			} else {
				$filename_right = $this->input->post('image_right_' . $lang_data['id']);
			}

			$data = array(
				'practice_id'     => $id['id'],
				'language_id' => $lang_data['id'],
				'image' 	  => $filename_image,
				'image_right' 	  => $filename_right,
				'file' => $filename,
				'name'        => input_clean($this->input->post('name_' . $lang_data['id'])),
				'content'     => $this->input->post('content_' . $lang_data['id'])
			);

			$this->db->insert('content_to_' . $this->url, $data);
		}

		action_log('UPDATE', $this->url, $row['id'], $default_name, 'MODIFY ' . $this->title . ' ( ' . $default_name . ' ) ');
	}
}
