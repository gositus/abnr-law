<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_banned_domain extends CI_Model {


	public function insert_banned_domain(){



		$input = array(
				'name'		=> input_clean(strtolower($this->input->post('name'))),
				'flag'		=> input_clean($this->input->post('flag')) ,
				'flag_memo'	=> input_clean($this->input->post('flag_memo')) 

		);
				
		$this->db->insert($this->tabl,$input);
		$id = $this->db->insert_id();
	
		$row = $this->db->get_where($this->tabl, array('id' => $id))->row_array();
		action_log('ADD', $this->url, $row['id'], $row['name'], 'ADDED ' . $this->title. ' ( ' . $row['name'] . ' ) ');

	}

	public function update_banned_domain(){
		
		$id['id']         = input_clean($this->input->post('id'));
		$row = $this->db->get_where($this->tabl, array('id' => $id['id']))->row_array();
	
		$input = array(
				'name'		=> input_clean(strtolower($this->input->post('name'))),		
				'flag'		=> input_clean($this->input->post('flag')) ,
				'flag_memo'	=> input_clean($this->input->post('flag_memo')) ,

		);

		$this->db->update($this->tabl,$input,$id);
		
		action_log('UPDATE', $this->url, $row['id'], $row['name'], 'MODIFY ' . $this->title . ' ( ' . $row['name'] . ' ) ');
		

	}
}
?>
