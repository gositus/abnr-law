<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_publication_category extends CI_Model
{



	public function get_list($table = "publication_category")
	{

		$default_language = setting_value('default_language');
		$query = $this->db->select("p.*, c.name as c_name")
			->join("content_to_" . $table . " c", "c.publication_category_id = p.id", "left")
			->get_where($table . " p", array('c.language_id' => $default_language, 'p.flag !=' => 3))->result_array();
		return $query;
	}

	public function get_detail($career_id = "")
	{

		$default_language = setting_value('default_language');
		foreach (language(TRUE)->result_array() as $lang) {
			foreach ($lang as $val) {
				$data[$val] = $this->db->join('content_to_' . $this->url . ' c', 'c.' . $this->url . '_id= p.id', 'left')->get_where($this->url . ' p', array('p.id' => $career_id, 'flag !=' => 3, 'language_id' => $val))->row_array();
			}
		}
		return ($data);
	}

	public function insert()
	{

		$language         = language()->result_array();
		$default_language = setting_value('default_language');
		$default_name     = input_clean($this->input->post('name_' . $default_language));

		$input = array(
			'flag'       => input_clean($this->input->post('flag')),
			'flag_memo'  => input_clean($this->input->post('flag_memo'))
		);

		$this->db->insert($this->url, $input);
		$id = $this->db->insert_id();

		foreach ($language as $lang_data) {
			$data = array(
				'publication_category_id'     => $id,
				'language_id' => $lang_data['id'],
				'name'        => input_clean($this->input->post('name_' . $lang_data['id']))
			);

			$this->db->insert('content_to_' . $this->url, $data);
		}

		$row = $this->db->get_where($this->url, array('id' => $id))->row_array();
		action_log('ADD', $this->url, $row['id'], $default_name, 'ADDED ' . $this->title . ' ( ' . $default_name . ' ) ');
	}


	public function update()
	{

		$id['id']         = input_clean($this->input->post('id'));
		$row = $this->db->get_where($this->url, array('id' => $id['id']))->row_array();

		$language         = language()->result_array();
		$default_language = setting_value('default_language');
		$default_name     = input_clean($this->input->post('name_' . $default_language));


		$input = array(
			'flag'      => input_clean($this->input->post('flag')),
			'flag_memo' => input_clean($this->input->post('flag_memo'))
		);

		$this->db->update($this->url, $input, $id);
		$this->db->delete('content_to_' . $this->url, array('publication_category_id' => $id['id']));

		foreach ($language as $lang_data) {
			$data = array(
				'publication_category_id'     => $id['id'],
				'language_id' => $lang_data['id'],
				'name'        => input_clean($this->input->post('name_' . $lang_data['id']))
			);

			$this->db->insert('content_to_' . $this->url, $data);
		}


		action_log('UPDATE', $this->url, $row['id'], $default_name, 'MODIFY ' . $this->title . ' ( ' . $default_name . ' ) ');
	}
}
