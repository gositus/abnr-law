<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_profile extends CI_Model
{

    public function get_list($data)
    {

        $default_language = setting_value('default_language');
        $query = $this->db->order_by('p.type', 'asc')->order_by('p.sort', 'asc')->order_by('p.join_date', 'asc')->select("p.*, c.name as name,c.image as image")
            ->join("content_to_" . $this->url . " c", "c.profile_id = p.id", "left")
            ->get_where($this->url . " p", array('type' => $data, 'c.language_id' => $default_language, 'p.flag !=' => 3))->result_array();
        $type = 0;
        foreach ($query as $key => $value) {
            if ($type != $value['type']) {
                if ($type != 0) {
                    $query[$key - 1]['last'] = 1;
                }
                $type = $value['type'];
                $query[$key]['first'] = 1;
            } else {
                $query[$key - 1]['last'] = 0;
                $type = $value['type'];
                $query[$key]['first'] = 0;
            }
            if ($key === count($query) - 1)
                $query[$key]['last'] = 1;
        }
        return $query;
    }

    public function get_profile(){
        $default_language = setting_value('default_language');
       return $this->db->order_by('name','asc')
           ->select('id,name')
            ->join('content_to_profile c','c.profile_id=t.id','inner')
            ->get_where("profile t", array('c.language_id' => $default_language, 't.flag !=' => 3))
           ->result_array();
    }

    public function get_detail($item_id = "")
    {

        $default_language = setting_value('default_language');
        foreach (language(TRUE)->result_array() as $lang) {
            foreach ($lang as $val) {
                $data[$val] = $this->db->join('content_to_' . $this->url . ' c', 'c.' . $this->url . '_id= p.id', 'left')->get_where($this->url . ' p', array('p.id' => $item_id, 'flag !=' => 3, 'language_id' => $val))->row_array();
            }
        }
        return ($data);
    }

    public function get_award($item_id = "")
    {

        $default_language = setting_value('default_language');

        $data = $this->db->group_by('id')->select('id')->get_where('profile_award', array('profile_id' => $item_id))->result_array();

        $data_award = array();
        foreach ($data as $key => $value) {
            foreach (language(TRUE)->result_array() as $lang) {
                foreach ($lang as $val) {
                    $data_award[$key][$val] = $this->db->get_where('profile_award p', array('p.id' => $value['id'], 'language_id' => $val))->row_array();
                }
            }
        }
        return ($data_award);
    }

    public function insert()
    {
        $language = language()->result_array();
        $default_language = setting_value('default_language');
        $section = input_clean($this->input->post('section'));
        $default_name = input_clean($this->input->post('name'));
        $filename = "";

        $image_profile = file_upload_name('image_profile', 'lib/images/profile', strtolower($default_name), FALSE, $this->image_profile_width, $this->image_profile_height);
        if ($image_profile) {
            $filename = $image_profile['file_name'];
        }

        $input = array(
            'seo_url' => create_seo_url(input_clean($this->input->post('name')), $this->url),
            'type' => input_clean($this->input->post('type')),
            'sort' => input_clean($this->input->post('sort')),
// 			'position'   => input_clean($this->input->post('position')),
            'phone' => input_clean($this->input->post('phone')),
            'email' => input_clean($this->input->post('email')),
            'join_date'     => input_clean(format_date($this->input->post('join_date'),TRUE)),
            'promotion_date'     => !empty(input_clean($this->input->post('promotion_date'))) ? input_clean(format_date($this->input->post('promotion_date'),TRUE)): NULL,
            'profile_image' => $filename,
            'date_added' => date("Y-m-d"),
            'flag' => input_clean($this->input->post('flag')),
            'flag_memo' => input_clean($this->input->post('flag_memo'))
        );


        $this->db->insert($this->url, $input);
        $id = $this->db->insert_id();


        //award

        $award_file = 'award_file';
        if (!empty($_FILES[$award_file])) {
            $limit = count($_FILES[$award_file]['name']);
            $files = $_FILES;

            for ($i = 0; $i < $limit; $i++) {
                $_FILES[$award_file]['name'] = $files[$award_file]['name'][$i];
                $_FILES[$award_file]['type'] = $files[$award_file]['type'][$i];
                $_FILES[$award_file]['tmp_name'] = $files[$award_file]['tmp_name'][$i];
                $_FILES[$award_file]['error'] = $files[$award_file]['error'][$i];
                $_FILES[$award_file]['size'] = $files[$award_file]['size'][$i];

                $award = upload_anything($award_file, 'files/profile/award/');
                $unique_id = unique_id('profile_award');
                foreach ($language as $lang_data) {
                    $award_upload = array(
                        'id' => $unique_id,
                        'profile_id' => $id,
                        'language_id' => $lang_data['id'],
                        'name' => $this->input->post('award_name_' . $lang_data['id'])[$i],
                        'file' => $award['file_name'],
                        'url' => $this->input->post('award_url')[$i],

                    );
                    $this->db->insert('profile_award', $award_upload);
                }
            }
        }

        $filename_image = "";
        $image = file_upload_name('image', 'lib/images/profile', strtolower($default_name), FALSE, $this->image_width, $this->image_height);
        if ($image) {
            $filename_image = $image['file_name'];
        }

        foreach ($language as $lang_data) {

            //upload files
            $filename = '';
            $file = upload_anything('file_' . $lang_data['id'], 'files/profile/');
            if ($file) {
                $filename = $file['file_name'];
            }


            $filename_right = "";
// 			$image_right =  file_upload_name('image_right_' . $lang_data['id'], 'lib/images/profile', strtolower($default_name), FALSE, $this->image_width_right, $this->image_height_right);
// 			if ($image_right) {
// 				$filename_right = $image_right['file_name'];
// 			}

            $data = array(
                'profile_id' => $id,
                'language_id' => $lang_data['id'],
                'image' => $filename_image,
                // 'image_right' => $filename_right,
                'name' => input_clean($this->input->post('name')),
                'file' => $filename,
                'content' => $this->input->post('content_' . $lang_data['id']),
                'professional_membership' => $this->input->post('professional_membership_' . $lang_data['id']),
                'citizenship' => $this->input->post('citizenship_' . $lang_data['id']),
                'language' => $this->input->post('language_' . $lang_data['id']),
                'experience' => $this->input->post('experience_' . $lang_data['id']),
            );

            $this->db->insert('content_to_' . $this->url, $data);
        }

        $row = $this->db->get_where($this->url, array('id' => $id))->row_array();
        action_log('ADD', $this->url, $row['id'], $default_name, 'ADDED ' . $this->title . ' ( ' . $default_name . ' ) ');
    }

    public function update()
    {

        $id['id'] = input_clean($this->input->post('id'));
        $row = $this->db->get_where($this->url, array('id' => $id['id']))->row_array();

        $language = language()->result_array();
        $default_language = setting_value('default_language');
        $default_name = input_clean($this->input->post('name'));
        $section = input_clean($this->input->post('section'));
        $filename = "";

        $seo_url = input_clean($this->input->post('seo_url'));
        if (empty($seo_url)) {
            $seo_url = $row['seo_url'];
        }

        $image_profile = file_upload_name('image_profile', 'lib/images/profile', strtolower($default_name), false, $this->image_profile_width, $this->image_profile_height);
        if ($image_profile) {
            $filename_profile = $image_profile['file_name'];
        } else {
            $filename_profile = $row['profile_image'];
        }


        $input = array(
            'seo_url' => $seo_url,
            'type' => input_clean($this->input->post('type')),
            'sort' => input_clean($this->input->post('sort')),
// 			'position'   => input_clean($this->input->post('position')),
            'phone' => input_clean($this->input->post('phone')),
            'email' => input_clean($this->input->post('email')),
            'join_date'     => input_clean(format_date($this->input->post('join_date'),TRUE)),
            'promotion_date'     => !empty(input_clean($this->input->post('promotion_date'))) ? input_clean(format_date($this->input->post('promotion_date'),TRUE)): NULL,
            'profile_image' => $filename_profile,
            'flag' => input_clean($this->input->post('flag')),
            'flag_memo' => input_clean($this->input->post('flag_memo'))
        );


        $this->db->update($this->url, $input, $id);


        //award

        $award_file = 'award_file';
        $sec_id = $this->input->post('award_id');
        $count = $this->input->post('count');

        if (!empty($count)) {
            $limit = count($count);
            $files = $_FILES;

            for ($i = 0; $i < $limit; $i++) {
                if (!empty($files[$award_file]['name'][$i])) {
                    $_FILES[$award_file]['name'] = $files[$award_file]['name'][$i];
                    $_FILES[$award_file]['type'] = $files[$award_file]['type'][$i];
                    $_FILES[$award_file]['tmp_name'] = $files[$award_file]['tmp_name'][$i];
                    $_FILES[$award_file]['error'] = $files[$award_file]['error'][$i];
                    $_FILES[$award_file]['size'] = $files[$award_file]['size'][$i];
                    $award = upload_anything($award_file, 'files/profile/award/');
                }
                $unique_id = unique_id('profile_award');
                foreach ($language as $lang_data) {
                    if (empty($sec_id[$i])) { // insert baru
                        $award_upload = array(
                            'id' => $unique_id,
                            'profile_id' => $id['id'],
                            'language_id' => $lang_data['id'],
                            'name' => $this->input->post('award_name_' . $lang_data['id'])[$i],
                            'file' => $award['file_name'],
                            'url' => $this->input->post('award_url')[$i],

                        );
                        $this->db->insert('profile_award', $award_upload);
                    } else {
                        $award_upload = array(
                            'profile_id' => $id['id'],
                            'language_id' => $lang_data['id'],
                            'name' => $this->input->post('award_name_' . $lang_data['id'])[$i],
                            'url' => $this->input->post('award_url')[$i],
                        );
                        $this->db->update('profile_award', $award_upload, array('id' => $sec_id[$i], 'language_id' => $lang_data['id']));
                    }
                }
            }
        }


        $filename_image = "";
        $image = file_upload_name('image', 'lib/images/profile', strtolower($default_name), FALSE, $this->image_width, $this->image_height);
        if ($image) {
            $filename_image = $image['file_name'];
        } else {
            $filename_image = $this->input->post('image');
        }
        $this->db->delete('content_to_' . $this->url, array('profile_id' => $id['id']));
        foreach ($language as $lang_data) {
            //upload files
            $filename = '';
            $file = upload_anything('file_' . $lang_data['id'], 'files/profile/');

            if ($file) {
                $filename = $file['file_name'];
            } else {
                $filename = $this->input->post('current_file_' . $lang_data['id']);
            }


            $filename_right = "";
// 			$image_right =  file_upload_name('image_right_' . $lang_data['id'], 'lib/images/profile', strtolower($default_name), FALSE, $this->image_width_right, $this->image_height_right);
// 			if ($image_right) {
// 				$filename_right = $image_right['file_name'];
// 			} else {
// 				$filename_right = $this->input->post('image_right_' . $lang_data['id']);
// 			}

            $data = array(
                'profile_id' => $id['id'],
                'language_id' => $lang_data['id'],
                'image' => $filename_image,
                // 'image_right' => $filename_right,
                'name' => input_clean($this->input->post('name')),
                'file' => $filename,
                'content' => $this->input->post('content_' . $lang_data['id']),
                'professional_membership' => $this->input->post('professional_membership_' . $lang_data['id']),
                'citizenship' => $this->input->post('citizenship_' . $lang_data['id']),
                'language' => $this->input->post('language_' . $lang_data['id']),
                'experience' => $this->input->post('experience_' . $lang_data['id']),
            );


            $this->db->insert('content_to_' . $this->url, $data);
        }
        action_log('UPDATE', $this->url, $row['id'], $default_name, 'MODIFY ' . $this->title . ' ( ' . $default_name . ' ) ');
    }

    public function order_up($id)
    {
        $row = select_all_row('profile', array('id' => $id), true);
        $atas = $this->db->order_by('sort', 'desc')->get_where('profile', array('type' => $row['type'], 'sort<' => $row['sort']))->row_array();

        $update_current_data = array(
            'sort' => $atas['sort']
        );
        $this->db->update('profile', $update_current_data, array('id' => $id));
        $update_atas_data = array(
            'sort' => $row['sort']
        );
        $this->db->update('profile', $update_atas_data, array('id' => $atas['id']));
    }

    public function order_down($id)
    {
        $row = select_all_row('profile', array('id' => $id), true);
        $bawah = $this->db->order_by('sort', 'asc')->get_where('profile', array('type' => $row['type'], 'sort>' => $row['sort']))->row_array();

        $update_current_data = array(
            'sort' => $bawah['sort']
        );
        $this->db->update('profile', $update_current_data, array('id' => $id));
        $update_bawah_data = array(
            'sort' => $row['sort']
        );
        $this->db->update('profile', $update_bawah_data, array('id' => $bawah['id']));
    }
}
