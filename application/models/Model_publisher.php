<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_publisher extends CI_Model
{

	public function insert()
	{

		$data_post = $this->input->post();
		$input = array(
			'name'       => input_clean($data_post['name']),
			'content' => $data_post['content'],
			'flag'       => input_clean($data_post['flag']),
			'flag_memo'  => input_clean($data_post['flag_memo'])
		);
		$this->db->insert($this->url, $input);

		$row = $this->db->get_where($this->url, array('id' => $this->db->insert_id()))->row_array();

		action_log('ADD', $this->url, $row['id'], $row['name'], 'ADDED ' . $this->title . ' ( ' . $row['name'] . ' ) ');
	}

	public function update()
	{

		$data_post = $this->input->post();
		$id = array('id' => input_clean($data_post['id']));
		$input = array(
			'name'       => input_clean($data_post['name']),
			'content' => $data_post['content'],
			'flag'       => input_clean($data_post['flag']),
			'flag_memo'  => input_clean($data_post['flag_memo'])
		);

		$this->db->update($this->url, $input, $id);

		// Query for log :)
		$row = $this->db->get_where($this->url, array('id' => $data_post['id']))->row_array();

		action_log('UPDATE', $this->url, $row['id'], $row['name'], 'MODIFY ' . $this->title . ' ( ' . $row['name'] . ' ) ');
	}
}
