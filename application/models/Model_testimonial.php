<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_testimonial extends CI_Model {


	public function get_detail($item_id=""){

		$default_language = setting_value('default_language');
		foreach (language(TRUE)->result_array() as $lang)
			{			
				foreach ($lang as $val)
				{
					$data[$val] = $this->db->join('content_to_'.$this->url . ' c' , 'c.'.$this->url.'_id= p.id','left')->get_where($this->url . ' p', array('p.id' => $item_id, 'flag !=' => 3, 'language_id' => $val))->row_array();
				}
			}
			return ($data);
	}

	public function insert_testimonial(){

		

		$filename ="";

		// $image            = str_replace(base_url(), "", input_clean($this->input->post('image')));
		// if(input_clean($this->input->post('image'))) {

		// 	$info = pathinfo(input_clean($this->input->post('image')));
		// 	$filename = $info['basename'];
		// 	resize_from_url(FCPATH . $image, FCPATH . 'lib/images/testimonial/', $this->image_width, $this->image_height,$info);
			
		// }

		$image =  file_upload_name('image', 'lib/images/testimonial', uniqid(), FALSE,$this->image_width, $this->image_height);
		
		if ($image)
		{
			$filename = $image['file_name'];

		}



		$input = array(
				'name'		=> input_clean($this->input->post('name')) ,
				'content'	=> input_clean($this->input->post('content')) ,
				'date'		=> date("Y-m-d"),
				'image'		=> $filename,
				'email'		=> input_clean($this->input->post('email')) ,
				'phone'		=> input_clean($this->input->post('phone')) ,
				'ip'		=> $this->input->ip_address()

		);
				
		$this->db->insert($this->url,$input);
		$id = $this->db->insert_id();
	
		$row = $this->db->get_where($this->url, array('id' => $id))->row_array();
		action_log('ADD', $this->url, $row['id'], $row['name'], 'ADDED ' . $this->title. ' ( ' . $row['name'] . ' ) ');

	}

	public function update_testimonial(){
		
		$id['id']         = input_clean($this->input->post('id'));
		$row = $this->db->get_where($this->url, array('id' => $id['id']))->row_array();

		$image            = str_replace(base_url(), "", input_clean($this->input->post('image')));
		// if(input_clean($this->input->post('image'))) {

		// 	$info = pathinfo(input_clean($this->input->post('image')));
		// 	$filename = $info['basename'];
		// 	resize_from_url(FCPATH . $image, FCPATH . 'lib/images/testimonial/', $this->image_width, $this->image_height,$info);
			
		// } 
		$image =  file_upload_name('image', 'lib/images/testimonial', uniqid(), FALSE,$this->image_width, $this->image_height);
		
		if ($image)
		{
			$filename = $image['file_name'];

		}
		else 
		{
			$filename = $row['image'];
			
			unlink(FCPATH.'lib/images/'.$this->url.'/'. $row['image']);
		}	
		$input = array(
				'name'		=> input_clean($this->input->post('name')) ,
				'content'	=> input_clean($this->input->post('content')) ,
				'image'		=> $filename,
				'email'		=> input_clean($this->input->post('email')) ,
				'phone'		=> input_clean($this->input->post('phone')) ,
				'flag'		=> input_clean($this->input->post('flag')) ,
				'flag_memo'		=> input_clean($this->input->post('flag_memo')) ,

		);

		$this->db->update($this->url,$input,$id);
		
		action_log('UPDATE', $this->url, $row['id'], $row['name'], 'MODIFY ' . $this->title . ' ( ' . $row['name'] . ' ) ');
		

	}
}
?>
