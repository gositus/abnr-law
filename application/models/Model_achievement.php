<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_achievement extends CI_Model
{

	public function get_list()
	{

		$default_language = setting_value('default_language');
		$query = $this->db->order_by('date_added', 'desc')->order_by('id', 'desc')->select("p.* , c.name as name,c.name as c_name")
			->join("content_to_" . $this->url . " c", "c.achievement_id = p.id", "left")
			->get_where($this->url . " p", array('c.language_id' => $default_language, 'p.flag !=' => 3))->result_array();
		return $query;
	}

	public function get_detail($item_id = "")
	{

		$default_language = setting_value('default_language');
		foreach (language(TRUE)->result_array() as $lang) {
			foreach ($lang as $val) {
				$data[$val] = $this->db->join('content_to_' . $this->url . ' c', 'c.' . $this->url . '_id= p.id', 'left')->get_where($this->url . ' p', array('p.id' => $item_id, 'flag !=' => 3, 'language_id' => $val))->row_array();
			}
		}
		return ($data);
	}

	public function insert()
	{

		$language         = language()->result_array();
		$default_language = setting_value('default_language');
		$default_name     = input_clean($this->input->post('name'));
		// $image            = str_replace(base_url(), "", input_clean($this->input->post('image')));

		$filename = "";
		if ($this->input->post('affiliate') == 1) {
			$width = $this->image_width_affiliations;
			$height = $this->image_height_affiliations;
		} else {
			$width = $this->image_width;
			$height = $this->image_height;
		}

		$image =  file_upload_name('image', 'lib/images/achievement', strtolower($default_name), FALSE, $width, $height);

		if ($image) {
			$filename = $image['file_name'];
		}

		$input = array(
			'image'      => $filename,
			'year' => input_clean($this->input->post('year')),
			'url' => input_clean($this->input->post('url')),
			'sort' => input_clean($this->input->post('sort')),
			'sort_home' => input_clean($this->input->post('sort_home')),
			'is_affiliate'   =>  input_clean($this->input->post('affiliate')),
            'publication_id'   =>  input_clean($this->input->post('publication_id')),
			'date_added' => date("Y-m-d"),
			'flag'       => input_clean($this->input->post('flag')),
			'flag_memo'  => input_clean($this->input->post('flag_memo'))
		);


		$this->db->insert($this->url, $input);
		$id = $this->db->insert_id();

		foreach ($language as $lang_data) {
			$data = array(
				'achievement_id'     => $id,
				'language_id' => $lang_data['id'],
				'name'        => input_clean($this->input->post('name')),
				'content'     => $this->input->post('content_' . $lang_data['id'])
			);

			$this->db->insert('content_to_' . $this->url, $data);
		}

        $practice=$this->input->post('practice');
        foreach ($practice as $value){
            $data = array(
                'achievement_id'     => $id,
                'practice_id' => $value,
            );

            $this->db->insert('achievement_to_practice', $data);
        }

        $practice=$this->input->post('profile');
        foreach ($practice as $value){
            $data = array(
                'achievement_id'     => $id,
                'profile_id' => $value,
            );

            $this->db->insert('achievement_to_profile', $data);
        }


		$row = $this->db->get_where($this->url, array('id' => $id))->row_array();
		action_log('ADD', $this->url, $row['id'], $default_name, 'ADDED ' . $this->title . ' ( ' . $default_name . ' ) ');
	}

	public function update()
	{

		$id['id']         = input_clean($this->input->post('id'));
		$row = $this->db->get_where($this->url, array('id' => $id['id']))->row_array();

		$language         = language()->result_array();
		$default_language = setting_value('default_language');
		$default_name     = input_clean($this->input->post('name'));

		if ($this->input->post('affiliate') == 1) {

			$width = $this->image_width_affiliations;
			$height = $this->image_height_affiliations;
		} else {
			$width = $this->image_width;
			$height = $this->image_height;
		}

		$image =  file_upload_name('image', 'lib/images/achievement', strtolower($default_name), false, $width, $height);
		if ($image) {
			$filename = $image['file_name'];
			unlink(FCPATH . 'lib/images/achievement/' . $row['image']);
		} else {
			$filename = $row['image'];
		}
		$input = array(
			'image'      => $filename,
			'year' => input_clean($this->input->post('year')),
			'url' => input_clean($this->input->post('url')),
			'sort' => input_clean($this->input->post('sort')),
			'sort_home' => input_clean($this->input->post('sort_home')),
			'is_affiliate'   =>  input_clean($this->input->post('affiliate')),
            'publication_id'   =>  input_clean($this->input->post('publication_id')),
			'flag'       => input_clean($this->input->post('flag')),
			'flag_memo'  => input_clean($this->input->post('flag_memo'))
		);

		$this->db->update($this->url, $input, $id);
		$this->db->delete('content_to_' . $this->url, array('achievement_id' => $id['id']));

		foreach ($language as $lang_data) {
			$data = array(
				'achievement_id'     => $id['id'],
				'language_id' => $lang_data['id'],
				'name'        => input_clean($this->input->post('name')),
				'content'     => $this->input->post('content_' . $lang_data['id'])
			);

			$this->db->insert('content_to_' . $this->url, $data);
		}

        $practice=$this->input->post('practice');
        $this->db->delete('achievement_to_practice', array('achievement_id' => $id['id']));
        foreach ($practice as $value){
            $data = array(
                'achievement_id'     => $id['id'],
                'practice_id' => $value,
            );

            $this->db->insert('achievement_to_practice', $data);
        }

        $practice=$this->input->post('profile');
        $this->db->delete('achievement_to_profile', array('achievement_id' => $id['id']));
        foreach ($practice as $value){
            $data = array(
                'achievement_id'     => $id['id'],
                'profile_id' => $value,
            );

            $this->db->insert('achievement_to_profile', $data);
        }

		action_log('UPDATE', $this->url, $row['id'], $default_name, 'MODIFY ' . $this->title . ' ( ' . $default_name . ' ) ');
	}
}
