<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_achievement_publication extends CI_Model
{


    public function get_list()
    {
        $query = select_all_row($this->url, array('flag !=' => 3), '', 'asc', 'sort');
        foreach ($query as $key => $value) {

            if($key==0){
                $query[$key]['first'] = 1;
            }else{
                $query[$key]['first'] = 0;
            }

            if(count($query)==$key+1){
                $query[$key]['last'] = 1;
            }else{
                $query[$key]['last'] = 0;
            }
        }
        return $query;
    }

    public function insert_branch()
    {

        $data_post = $this->input->post();
        $input = array(
            'name' => input_clean($data_post['name']),
            'sort' => input_clean($data_post['sort']),
            'flag' => input_clean($data_post['flag']),
            'flag_memo' => input_clean($data_post['flag_memo'])
        );
        $this->db->insert($this->url, $input);

        $row = $this->db->get_where($this->url, array('id' => $this->db->insert_id()))->row_array();

        action_log('ADD', $this->url, $row['id'], $row['name'], 'ADDED ' . $this->title . ' ( ' . $row['name'] . ' ) ');

    }

    public function update_branch()
    {

        $data_post = $this->input->post();
        $id = array('id' => input_clean($data_post['id']));
        $input = array(
            'name' => input_clean($data_post['name']),
            'sort' => input_clean($data_post['sort']),
            'flag' => input_clean($data_post['flag']),
            'flag_memo' => input_clean($data_post['flag_memo'])
        );

        $this->db->update($this->url, $input, $id);

        // Query for log :)
        $row = $this->db->get_where($this->url, array('id' => $data_post['id']))->row_array();

        action_log('UPDATE', $this->url, $row['id'], $row['name'], 'MODIFY ' . $this->title . ' ( ' . $row['name'] . ' ) ');

    }

    public function order_up($id)
    {
        $row = select_all_row('achievement_publication', array('id' => $id), true);
        $atas = $this->db->order_by('sort', 'desc')->get_where('achievement_publication', array( 'sort<' => $row['sort']))->row_array();

        $update_current_data = array(
            'sort' => $atas['sort']
        );
        $this->db->update('achievement_publication', $update_current_data, array('id' => $id));
        $update_atas_data = array(
            'sort' => $row['sort']
        );
        $this->db->update('achievement_publication', $update_atas_data, array('id' => $atas['id']));
    }

    public function order_down($id)
    {
        $row = select_all_row('achievement_publication', array('id' => $id), true);
        $bawah = $this->db->order_by('sort', 'asc')->get_where('achievement_publication', array('sort>' => $row['sort']))->row_array();

        $update_current_data = array(
            'sort' => $bawah['sort']
        );
        $this->db->update('achievement_publication', $update_current_data, array('id' => $id));
        $update_bawah_data = array(
            'sort' => $row['sort']
        );
        $this->db->update('achievement_publication', $update_bawah_data, array('id' => $bawah['id']));
    }

}

?>
