<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_front extends CI_Model
{

    // get banner berdasarkan section
    public function get_banner($section = "", $single = FALSE)
    {
        $table = 'banner';
        $array = array(
            'flag' => 1,
            'language_id' => current_language('id'),
            'start <= ' => date("Y-m-d")
        );
        $where_or = '(end = "0000-00-00 00:00:00" OR end >= CURDATE() )';
        if (!empty($section)) {
            $array['section_id'] = $section;
        }

        $query = $this->db
            ->order_by('sort', 'asc')
            ->order_by('start', 'desc')
            ->select("*")
            ->join('content_to_' . $table . ' ct', 'ct.' . $table . '_id = t.id', 'left')
            ->where($array)
            ->where($where_or)
            ->get($table . ' t');
        if ($single) {
            return $query->row_array();
        } else {
            return $query->result_array();
        }
    }

    public function get_page($section = "", $single = FALSE)
    {

        $table = 'page';
        $array = array(
            'flag' => 1,
            'language_id' => current_language('id'),
        );
        if (!empty($section)) {
            $array['section_id'] = $section;
        }

        $query = $this->db->select("*")
            ->join('content_to_' . $table . ' ct', 'ct.' . $table . '_id = t.id', 'left')
            ->where($array)
            ->get($table . ' t');

        if ($single) {
            return $query->row_array();
        } else {
            return $query->result_array();
        }
    }

    // Display all ACTIVE News
    public function get_news($seo_url = "", $year = "", $month = "", $single = FALSE, $limit = "", $exclude = "")
    {
        $where_or = '1=1';
        if($this->input->get('preview') && empty($exclude)){
            $flag=0;
        }else{
            $flag=1;
            $where_or='(end = "0000-00-00 00:00:00" OR end >= CURDATE() )';
        }

        $table = 'news';
        $array = array(
            'flag' => $flag,
            'language_id' => current_language('id'),
            'start <=' => date("Y-m-d"),
            'type' => 1
        );

        if (!empty($year)) {
            $array['year(start)'] = $year;
        }
        if (!empty($month)) {
            $array['month(start)'] = $month;
        }
        if (!empty($seo_url)) {
            $array['seo_url'] = $seo_url;
        }

        if (!empty($limit)) {
            $this->db->limit($limit);
        }

        if (!empty($exclude)) {
            $array['id<>'] = $exclude;
        }

        $this->db
            ->order_by('t.start', 'desc')
            ->order_by('t.id', 'desc')
            ->select("*,year(start) as year, month(start) as month")
            ->join('content_to_' . $table . ' ct', 'ct.' . $table . '_id = t.id', 'left')
            ->where($array)
            ->where($where_or);
        if(!empty($this->input->get('search'))){
            $text=$this->db->escape_str($this->input->get('search'));
            $this->db->where("(MATCH(name) AGAINST('" . $text . "' IN NATURAL LANGUAGE MODE) or content like '%" . $text . "%')");
        }
        $query = $this->db->get($table . ' t');
        if ($single) {
            return $query->row_array();
        } else {
            return $query->result_array();
        }
    }

    public function get_news_homepage()
    {
        $table = 'news';
        $array = array(
            'flag' => 1,
            'language_id' => current_language('id'),
            'start <=' => date("Y-m-d"),
            'type' => 1,
            'banner !=' => "",
            'banner !=' => NULL
        );
        $where_or = '(end = "0000-00-00 00:00:00" OR end >= CURDATE() )';
        if (!empty($year)) {
            $array['year(start)'] = $year;
        }
        if (!empty($month)) {
            $array['month(start)'] = $month;
        }
        if (!empty($seo_url)) {
            $array['seo_url'] = $seo_url;
        }

        $query = $this->db
            ->order_by('t.start', 'desc')
            ->order_by('t.id', 'desc')
            ->select("*,year(start) as year, month(start) as month")
            ->join('content_to_' . $table . ' ct', 'ct.' . $table . '_id = t.id', 'left')
            ->where($array)
            ->where($where_or)
            ->limit(5)
            ->get($table . ' t');
        return $query->result_array();
    }

    // Display all ACTIVE media
    public function get_media($seo_url = "", $type = "", $single = FALSE)
    {
        $table = 'media';
        $array = array(
            'flag' => 1,
            'language_id' => current_language('id')
        );

        if (!empty($type)) {
            $array['type'] = $type;
        }
        if (!empty($seo_url)) {
            $array['seo_url'] = $seo_url;
        }

        $query = $this->db
            ->order_by('t.id', 'desc')
            ->select("*")
            ->join('content_to_' . $table . ' ct', 'ct.' . $table . '_id = t.id', 'left')
            ->where($array)
            ->get($table . ' t');

        if ($single) {
            return $query->row_array();
        } else {
            return $query->result_array();
        }
    }

    public function get_product($seo_url = "", $type = "", $single = FALSE, $category_id = "")
    {

        $table = 'item';
        $array = array(
            'flag' => 1,
            'language_id' => current_language('id'),
        );
        if (!empty($type)) {
            $array['type'] = $type;
        }
        if (!empty($seo_url)) {
            $array['seo_url'] = $seo_url;
        }
        if (!empty($category_id)) {
            $array['item_category_id'] = $category_id;
        }

        $query = $this->db->select("*")
            ->join('content_to_' . $table . ' ct', 'ct.' . $table . '_id = t.id', 'left')
            ->where($array)
            ->get($table . ' t');

        if ($single) {
            return $query->row_array();
        } else {
            return $query->result_array();
        }
    }

    public function get_product_category($seo_url = "", $type = "", $single = FALSE)
    {

        $table = 'item_category';
        $array = array(
            'flag' => 1,
            'language_id' => current_language('id'),
        );
        if (!empty($type)) {
            $array['type'] = $type;
        }
        if (!empty($seo_url)) {
            $array['seo_url'] = $seo_url;
        }

        $query = $this->db->select("*")
            ->join('content_to_' . $table . ' ct', 'ct.' . $table . '_id = t.id', 'left')
            ->where($array)
            ->get($table . ' t');

        if ($single) {
            return $query->row_array();
        } else {
            return $query->result_array();
        }
    }

    public function add_subscriber()
    {

        $data = $this->input->post();
        $exist = select_all_row('newsletter', array('email' => strtolower($data['email_newsletter'])), TRUE);
        if (!empty($exist)) {
            $this->db->update("newsletter", array("flag" => 1), array("id" => $exist['id']));
        } else {
            $this->db->insert("newsletter", array('email' => strtolower($data['email_newsletter']), 'name' => $data['email_newsletter']));
        }
        return true;
    }

    public function get_featured_achievement($limit = 7)
    {
        $where = array(
            'ct.language_id' => current_language('id'),
            't.flag' => 1,
            't.is_affiliate' => 0,
            't.sort_home<>' => 0,
        );
        $query = $this->db
            ->order_by('sort_home', 'asc')
            ->select("*")
            ->join('content_to_achievement ct', 'ct.achievement_id = t.id', 'left')
            ->where($where)
            ->limit($limit)
            ->get('achievement t')
            ->result_array();
        // pre($query);
        return $query;
    }

    public function get_affiliate($limit = 2)
    {
        $where = array(
            'ct.language_id' => current_language('id'),
            't.flag' => 1,
            't.is_affiliate' => 1
        );
        $query = $this->db
            ->order_by('sort_home', 'asc')
            ->select("*")
            ->join('content_to_achievement ct', 'ct.achievement_id = t.id', 'left')
            ->where($where)
            ->limit($limit)
            ->get('achievement t')
            ->result_array();
        return $query;
    }

    public function get_profile($type = "", $limit = "", $start = 0)
    {
        $where = array(
            'ct.language_id' => current_language('id'),
            't.flag' => 1,
            't.type<>'=>9
        );

        if (!empty($limit)) {
            $this->db->limit($limit, $start);
        }
        
        if (empty($type) || $type == 'all') {
            $this->db->order_by('name', 'asc');
//            $where['t.type<>']=4;

        } else {
            $this->db->order_by("FIELD(t.type, '8')", '');
            $this->db->order_by("FIELD(t.type, '4')", '');
            $this->db->order_by('sort', 'asc');
            $this->db->order_by('join_date', 'asc');
            if($type!=2) {
                $where['t.type'] = $type;
            }
            
        }


//        $this->db->order_by('name', 'asc');

        $this->db->select("*")
            ->join('content_to_profile ct', 'ct.profile_id = t.id', 'left')
            ->where($where);
        if (!empty($type) and $type == '2') {
            $this->db->group_start()
                ->where('t.type', 2)
                ->or_where('t.type', 4)
                ->or_where('t.type', 8)
                ->group_end();
        }
        if(!empty($this->input->get('search'))){
            $text=$this->db->escape_str($this->input->get('search'));
            $this->db->where("(MATCH(name) AGAINST('" . $text . "' IN NATURAL LANGUAGE MODE) or content like '%" . $text . "%')");
        }
        $query = $this->db->get('profile t')
            ->result_array();
        return $query;
    }

    public function get_profile_by_seo_url($seo_url = "")
    {
        $where = array(
            'ct.language_id' => current_language('id'),
            't.flag' => 1,
            't.seo_url' => $seo_url
        );
        $query = $this->db
            ->order_by('sort', 'asc')
            ->select("*")
            ->join('content_to_profile ct', 'ct.profile_id = t.id', 'left')
            ->where($where)
            ->get('profile t')
            ->row_array();
        return $query;
    }


    public function get_achievement_year($year="")
    {
        $query = $this->db
            ->group_by('year')
            ->order_by('year', 'DESC')
            ->select("year")
            ->where(array('flag' => 1,'year>='=>date('Y')-5))
            ->get('achievement t')
            ->result_array();
        $array = array_column($query, 'year');
        return $array;
    }

    public function get_achievement_publication()
    {
        $query = $this->db
            ->order_by('sort', 'ASC')
            ->select("*")
            ->where(array('flag' => 1))
            ->get('achievement_publication t')
            ->result_array();
        return $query;
    }


    public function get_achievement($year = "")
    {
        if (empty($year)) {
            $year = date("Y");
        }

        $where = array(
            'ct.language_id' => current_language('id'),
            't.flag' => 1,
            't.year' => $year,
            'is_affiliate'=>0,
        );
        $this->db
            ->order_by('sort', 'asc')
            ->select("*")
            ->join('content_to_achievement ct', 'ct.achievement_id = t.id', 'left')
            ->where($where);
        if (!empty($this->input->get('search'))) {
            $text = $this->db->escape_str($this->input->get('search'));
            $this->db->where("(MATCH(name) AGAINST('" . $text . "' IN NATURAL LANGUAGE MODE) or content like '%" . $text . "%')");
        }
        $query = $this->db->get('achievement t')
            ->result_array();

        return $query;
    }

    public function get_publication($filter = array())
    {
        $where = array(
            'ct.language_id' => current_language('id'),
            't.flag' => 1,
        );

        if ((isset($filter['start']) || isset($filter['limit'])) && !isset($filter['total'])) {
            if ($filter['start'] < 0) {
                $filter['start'] = 0;
            }

            if ($filter['limit'] < 1) {
                $filter['limit'] = 20;
            }

            $this->db->limit($filter['limit'], $filter['start']);
        }

        $this->db
            ->order_by('publish_date, id', 'DESC')
            ->select("t.*,ct.*,p.name as publisher")
            ->join('publisher p', 't.publisher = p.id', 'left')
            ->join('content_to_document ct', 'ct.document_id = t.id', 'left')
            ->where($where);
        if(!empty($this->input->get('search'))){
            $text=$this->db->escape_str($this->input->get('search'));
            $this->db->where("(MATCH(ct.name) AGAINST('" . $text . "' IN NATURAL LANGUAGE MODE) or ct.content like '%" . $text . "%')");
        }
        $query =   $this->db->get('document t')
            ->result_array();

        return $query;
    }

    public function get_practice($seo_url = "")
    {
        $where = array(
            'ct.language_id' => current_language('id'),
            't.flag' => 1,
        );

        if (!empty($seo_url)) {
            $where['seo_url'] = $seo_url;
        }

        $this->db
            ->order_by('name', 'asc')
            ->select("*")
            ->join('content_to_practice ct', 'ct.practice_id = t.id', 'left')
            ->where($where);
        if(!empty($this->input->get('search'))){
            $text=$this->db->escape_str($this->input->get('search'));
            $this->db->where("(MATCH(name) AGAINST('" . $text . "' IN NATURAL LANGUAGE MODE) or content like '%" . $text . "%')");
        }
        $query = $this->db->get('practice t');

        if (!empty($seo_url)) {
            return $query->row_array();
        } else {
            return $query->result_array();
        }
    }

    public function get_news_year()
    {
        $query = $this->db
            ->group_by('year')
            ->order_by('year', 'DESC')
            ->select("year(start) as year")
            ->where(array('flag' => 1))
            ->get('news t')
            ->result_array();
        $array = array_column($query, 'year');
        return $array;
    }

    public function get_news_month($month = "")
    {
        $query = $this->db
            ->group_by('month')
            ->order_by('month', 'DESC')
            ->select("month(start) as month")
            ->where(array('flag' => 1, 'year(start)' => $month))
            ->get('news t')
            ->result_array();
        $array = array_column($query, 'month');
        return $array;
    }

    public function search($text = "", $page = "",$limit="",$offset=0)
    {

        $query = "SELECT * FROM `" . $page . "` `t`
				LEFT JOIN `content_to_" . $page . "` `ct` ON `ct`.`" . $page . "_id` = `t`.`id`
				WHERE `ct`.`language_id` = ?
				AND (MATCH(name) AGAINST(? IN NATURAL LANGUAGE MODE) or content like ?) 
				AND `flag`=1";

        if($page=='profile'){
            $query.=' AND (type=3 or type=1)';
            $query.=' Order by join_date asc, name asc';
        }elseif($page=='achievement'){
            $query.='  Order by year desc';
        } else{
            $query.=' Order by id desc';
        }

        if(!empty($limit)){
            $query.=' LIMIT '.$offset.', '.$limit;
        }
        $textSearch="%$text%";

        $query = $this->db->query($query,array(current_language('id'), $text, $textSearch))->result_array();
        return $query;
    }

    public function get_award_profile($id = "")
    {
        $where = array(
            't.language_id' => current_language('id'),
            't.profile_id' => $id,

        );
        $query = $this->db
            ->order_by('t.id', 'ASC')
            ->select("*")
            ->where($where)
            ->get('profile_award t')
            ->result_array();
        return $query;
    }

    public function get_career($section = "", $single = FALSE)
    {
        $table = 'career';
        $array = array(
            'flag' => 1,
            'language_id' => current_language('id'),
            'start <= ' => date("Y-m-d")
        );
        $where_or = '(end = "0000-00-00 00:00:00" OR end >= CURDATE() )';
        if (!empty($section)) {
            $array['seo_url'] = $section;
        }

        $query = $this->db
            ->order_by('start', 'desc')
            ->select("*")
            ->join('content_to_' . $table . ' ct', 'ct.' . $table . '_id = t.id', 'left')
            ->where($array)
            ->where($where_or)
            ->get($table . ' t');
        if ($single) {
            return $query->row_array();
        } else {
            return $query->result_array();
        }
    }

    public function get_achievement_by_practice($id=""){
        $where=array('a.flag'=>1,'t.practice_id'=>$id,'c.language_id' => current_language('id'));
        $query = $this->db
            ->order_by('c.name', 'asc')
            ->select("a.*,c.name")
            ->join('achievement a', 'a.id = t.achievement_id', 'inner')
            ->join('content_to_achievement c', 'c.achievement_id = t.achievement_id', 'inner')
            ->where($where)
            ->get('achievement_to_practice t')
            ->result_array();

        return $query;
    }

    public function get_achievement_by_profile($id=""){
        $where=array('a.flag'=>1,'t.profile_id'=>$id,'c.language_id' => current_language('id'));
        $query = $this->db
            ->order_by('c.name', 'asc')
            ->select("a.*,c.name")
            ->join('achievement a', 'a.id = t.achievement_id', 'inner')
            ->join('content_to_achievement c', 'c.achievement_id = t.achievement_id', 'inner')
            ->where($where)
            ->get('achievement_to_profile t')
            ->result_array();

        return $query;
    }
}
