<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_member extends CI_Model {

	var $table = 'member';

	public function check_login(){
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$user = $this->db->get_where('member',array('email'=>$email))->row_array();
		if(password_verify($password,$user['password']))
		{
			$this->session->set_userdata('member_id', $user['id']);
			$input = array( 
				'member_id'  => $user['id'],
				'ip'         => $this->input->ip_address(),
				'user_agent' => get_user_agent(),
				'action'	 => 'LOGIN'
			);
			$this->db->insert('member_log', $input);
			$now = date('Y-m-d H:i:s');
			$this->db->query("update `member` set `last_login` = '{$now}' where id = {$user['id']}");
			return true;
		} else 
		{
			return false;
		}
	}

	public function insert_member(){

		$password = bcrypt_it($this->input->post('password'));


		$input = array( 
			'password'   => $password,
			'name'       => input_clean($this->input->post('name')),
			'phone'      => input_clean($this->input->post('phone')),
			'dob'        => format_date(input_clean($this->input->post('dob')),TRUE),
			'gender'     => input_clean($this->input->post('gender')),
			'email'      => input_clean($this->input->post('email')),
			'date_added' => date("Y-m-d H:i:s"),
			'ip'         => $this->input->ip_address(),
			'user_agent' => get_user_agent()
			);
		$this->db->insert($this->table, $input);
		return $this->db->insert_id();
	}

	public function update_member(){

		$input = array( 
			
			'name'   => input_clean($this->input->post('name')),
			'phone'  => input_clean($this->input->post('phone')),
			'dob'    => format_date(input_clean($this->input->post('dob')),TRUE),
			'gender' => input_clean($this->input->post('gender'))
		);
		$this->db->update($this->table, $input, array('id'=> $this->session->userdata("member_id")));

		if($this->input->post('password') != ''){
			$password = bcrypt_it($this->input->post('password'));
			$this->db->update($this->table, array('password'=>$password), array('id'=> $this->session->userdata("member_id")));
		}
		return $this->session->userdata("member_id");
	}

	public function update_member_cms($item_id=""){

		$row = $this->db->get_where($this->url, array('id' => $item_id))->row_array();
		$input = array( 
			
			'name'   => input_clean($this->input->post('name')),
			'phone'  => input_clean($this->input->post('phone')),
			'dob'    => format_date(input_clean($this->input->post('dob')),TRUE),
			'gender' => input_clean($this->input->post('gender')),
			'email'  => input_clean($this->input->post('email')),
			'flag'  => input_clean($this->input->post('flag')),
			'flag_memo'  => input_clean($this->input->post('flag_memo'))
		);
	
		
		$this->db->update($this->table, $input, array('id'=> $item_id));
	

		if($this->input->post('password') != ''){
			$password = bcrypt_it($this->input->post('password'));
			$this->db->update($this->table, array('password'=>$password), array('id'=> $item_id));
		}
		
		action_log('UPDATE', $this->url, $row['id'], $row['name'], 'MODIFY ' . $this->title . ' ( ' . $row['name'] . ' ) ');
		
		return $this->session->userdata("member_id");
	}

	public function forgot(){
		$email = $this->security->xss_clean($this->input->post('email'));
		if(!$this->input->post() || empty($email)){
			redirect('member');
		}

		$user = $this->db->get_where("member",array('email' =>$email, 'flag' => 1))->row_array();
		if(empty($user))
		{
			return 'Inactive or Invalid email';
		} else
		{
			if($user['last_forget'] > date("Y-m-d H:i:S"))
			{
				return 'Please try in 30 minutes again';
			} else
			{
				$token['forget_token'] = Date("d") . strtoupper(uniqid()) . Date("y") . uniqid() . Date("m") . strtoupper(uniqid());
				$token['last_forget'] = date("Y-m-d H:i:s", strtotime('+ 30 minutes'));
				$token['forget_times'] = $user['forget_times'] + 1;
				$mail['email'] = $user['email']; 
				$this->db->update("member",$token,$mail);

				$email_data = array(
					'to' 		=> $user['email'],
					'title' 	=> "Forgot Password",
					'subject'	=> "Forgot Password",
					'to_name'	=> $user['name'],
					'name'		=> 'NOTIFICATION',
				);
				$message = '
				Hello  ' . $user['name'] . '!<br><br>
				Someone, hopefully you, has requested to reset the password for your account on '.base_url().'.<br><br>
				If you did not perform this request, you can safely ignore this email.<br>Otherwise, click the link below to complete the process.<br<br>


				<table style="border-collapse:separate!important;" cellspacing="0" cellpadding="0" border="0">
		          <tbody>
				  	<tr><td height="10"></td></tr>
		            <tr>
		              <td class="td-button" valign="middle" align="center">
		                <a class="a-button" href="'.base_url('member/reset-password/') . $token['forget_token'] .'">Reset Password</a>
		              </td>
		            </tr>
					<tr><td height="10"></td></tr>
		          </tbody>
		        </table>
		        <br>
		        <i> Reset Before : ' . date("d M Y H:i", strtotime($token['forgot_time'])) . '  </i>

				' ;

				$email_data['message'] = $message;

					sendemail($email_data);
				return 'success';
			}
		}

	}

	public function reset($token="")
	{
		if(empty($token))
		{
			redirect(base_url('member'));
		} else
		{
			$user = $this->db->get_where("member",array('forget_token' =>$token, 'flag' => 1))->row_array();
			if(!empty($user)){
				if($user['last_forget'] < date("Y-m-d H:i:S"))
				{
					return 'Token Expired';
				} else
				{
					if($this->input->post('reset'))
					{
						$captcha = trim($this->input->post("g-recaptcha-response"));
			       		$cek = verify_captcha($captcha);
			       		if($cek['success']==1)
			       		{
							$data_post = $this->security->xss_clean($this->input->post());
							$id= array('forget_token'=>$token);
							$password = array(
								'password' => bcrypt_it($data_post['password']),
								'forget_token'	=> ''
							);
						}
						
						$this->db->update('member',$password,$id);
						return 'success';
					}else
					{
						return $user;
					}
				}
			} else {
				return 'Token Invalid';
			}
		}

	}

	
}