<?php
//Greetings
$lang['selamat_pagi']  = 'Good morning';
$lang['selamat_siang'] = 'Good afternoon';
$lang['selamat_sore']  = 'Good day';
$lang['selamat_malam'] = 'Good evening';

//Time
$lang['waktu_barusan']          = 'just now';
$lang['waktu_baru_saja']        = 'just now';
$lang['waktu_menit_yang_lalu']  = ' mins ago';
$lang['waktu_jam_yang_lalu']    = ' hours ago';
$lang['waktu_kemarin']          = 'yesterday';
$lang['waktu_hari_yang_lalu']   = ' days ago';
$lang['waktu_minggu_lalu']      = 'last week';
$lang['waktu_minggu_yang_lalu'] = ' weeks ago';
$lang['waktu_bulan_kemarin']    = 'last month';
$lang['waktu_bulan_yang_lalu']  = ' month ago';
$lang['waktu_tahun_lalu']       = 'last year';
$lang['waktu_tahun_yang_lalu']  = ' years ago';

//navigation
$lang['abnr']            = 'abnr';
$lang['practice_area']   = 'practice areas';
$lang['profiles']        = 'profiles';
$lang['global']          = 'global reach';
$lang['achievements']    = 'achievements';
$lang['news']            = 'news';
$lang['news_archive']    = 'News Archive';
$lang['publication']     = 'publication';
$lang['contact']         = 'contact';
$lang['join_us']         = 'Career at ABNR';
$lang['abnr_indonesia']  = 'indonesia overview';
$lang['abnr_foundation'] = 'abnr foundation';
$lang['partners']        = 'partner';
$lang['foreign_counsel'] = 'foreign counsel';
$lang['counsel']         = 'counsel';
$lang['of_counsel']      = 'of counsel';
$lang['associates']      = 'associate';
$lang['senior_associates']      = 'senior associate';
$lang['trainee_associate']   = 'trainee associate';
$lang['assistant_law']   = 'assistant lawyer';
$lang['search']          = 'search';
$lang['search_result']   = 'search result';
$lang['knowledge_center']= 'Knowledge Center';
$lang['citizenship']     = 'CITIZENSHIP';
$lang['language']        = 'LANGUAGES';
$lang['awards']          = 'AWARDS';


//button
$lang['to_be_involved'] = 'HOW TO BE<br> INVOLVED';
$lang['gov_link'] = 'RESOURCES & <br> GOVERMENT LINKS';
$lang['published_article'] = 'PUBLISHED<br> ARTICLES';

//title
$lang['important_link'] = 'Important Link';
$lang['search_not_found'] = "Keyword '%s' not found!";
