<?php
//Greetings
$lang['selamat_pagi']  = 'Selamat pagi';
$lang['selamat_siang'] = 'Selamat siang';
$lang['selamat_sore']  = 'Selamat sore';
$lang['selamat_malam'] = 'Selamat malam';

// Time
$lang['waktu_barusan']          = 'barusan';
$lang['waktu_baru_saja']        = 'baru saja';
$lang['waktu_menit_yang_lalu']  = ' menit yang lalu';
$lang['waktu_jam_yang_lalu']    = ' jam yang lalu';
$lang['waktu_kemarin']          = 'kemarin';
$lang['waktu_hari_yang_lalu']   = ' hari yang lalu';
$lang['waktu_minggu_lalu']      = 'minggu lalu';
$lang['waktu_minggu_yang_lalu'] = ' minggu yang lalu';
$lang['waktu_bulan_kemarin']    = 'bulan kemarin';
$lang['waktu_bulan_yang_lalu']  = ' bulan yang lalu';
$lang['waktu_tahun_lalu']       = 'tahun lalu';
$lang['waktu_tahun_yang_lalu']  = ' tahun yang lalu';

//navigation
$lang['abnr']            = 'abnr';
$lang['practice_area']   = 'bidang praktek';
$lang['profiles']        = 'profil';
$lang['global']          = 'jangkauan global';
$lang['achievements']    = 'penghargaan';
$lang['news']            = 'berita';
$lang['news_archive']    = 'Arsip Berita';
$lang['publication']     = 'publikasi';
$lang['contact']         = 'hubungi kami';
$lang['join_us']         = 'karir';
$lang['abnr_indonesia']  = 'sekilas tentang indonesia';
$lang['abnr_foundation'] = 'yayasan abnr';
$lang['partners']        = 'partner';
$lang['foreign_counsel'] = 'foreign counsel';
$lang['counsel']         = 'counsel';
$lang['of_counsel']      = 'of counsel';
$lang['associates']      = 'associate';
$lang['senior_associates']      = 'senior associate';
$lang['trainee_associate']   = 'trainee associate';
$lang['assistant_law']   = 'assistant lawyer';
$lang['search']          = 'pencarian';
$lang['search_result']   = 'hasil pencarian';
$lang['knowledge_center']= 'Pusat Pengetahuan';
$lang['citizenship']     = 'KEWARGANEGARAAN';
$lang['language']        = 'BAHASA';
$lang['awards']          = 'PENGHARGAAN';

//button
$lang['to_be_involved'] = 'CARA UNTUK<br> MELIBATKAN DIRI';
$lang['gov_link'] = 'LINK NARA-SUMBER &<br> PEMERINTAH';
$lang['published_article'] = 'PUBLIKASI<br> ARTIKEL ';

//title
$lang['important_link'] = 'Tautan Penting';
$lang['search_not_found'] = "Kata kunci '%s' tidak ditemukan!";
