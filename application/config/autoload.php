<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$autoload['packages'] = array();


$autoload['libraries'] = array('session', 'database', 'form_validation');

$autoload['drivers'] = array();

$autoload['helper'] = array('url', 'text','go','captcha','date','admin');

$autoload['config'] = array();

//untuk load file language harus ditambahkan sesuai kebutuhan
$autoload['language'] = array('language');

$autoload['model'] = array();
$autoload['model'] = array('model_process');