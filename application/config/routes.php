<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller']      = 'home';
$route['404_override']            = '';
$route['translate_uri_dashes']    = TRUE;
$route['goadmin']                 = "goadmin/login";
$route['goadmin/profile']         = "goadmin/profile";
$route['goadmin/profile/(:num)']  = "goadmin/profile/index/$1";
$route['goadmin/forgot-password'] = "goadmin/login/forgot_password";
$route['goadmin/reset-password/(:any)'] = "goadmin/login/reset_password/$1";
$route['news']                    = "news/index";
$route['news/(:num)']             = "news/index/$1";
$route['news/(:num)/(:num)']             = "news/index/$1/$2";
$route['news/(:any)']             = "news/detail/$1";


$route['news-archive']                    = "news_archive/index";
$route['news-archive/(:num)']             = "news_archive/index/$1";
$route['news-archive/(:num)/(:num)']             = "news_archive/index/$1/$2";
$route['news-archive/(:any)']             = "news_archive/detail/$1";

$route['product']                 = "product/index";
$route['product/category/(:any)'] = "product/category/$1";
$route['product/(:any)']          = "product/detail/$1";
$route['count/(:any)/(:num)']     = "home/count/$1/$2";
$route['abnr']                    = "about/index";
$route['indonesia-overview']      = "about/indonesia-overview";
$route['abnr-foundation']         = "about/abnr-foundation";
$route['profiles']                = "profiles/index";
$route['profiles/all/(:num)']         = "profiles/index/all/$1";

$route['profiles/partners']       = "profiles/index/1";
$route['profiles/foreign-counsel'] = "profiles/index/3";
$route['profiles/counsel']       = "profiles/index/6";
$route['profiles/of-counsel']    = "profiles/index/5";
$route['profiles/associates']    = "profiles/index/2";
$route['profiles/senior-associates']    = "profiles/index/7";
$route['profiles/assistant-law'] = "profiles/index/4";


$route['profiles/partners/(:num)']       = "profiles/index/1/$1";
$route['profiles/foreign-counsel/(:num)'] = "profiles/index/3/$1";
$route['profiles/counsel/(:num)']       = "profiles/index/6/$1";
$route['profiles/of-counsel/(:num)']    = "profiles/index/5/$1";
$route['profiles/associates/(:num)']    = "profiles/index/2/$1";
$route['profiles/senior-associates/(:num)']    = "profiles/index/7/$1";
$route['profiles/assistant-law/(:num)'] = "profiles/index/4/$1";
$route['profiles/pdf/(:any)'] = "profiles/getPDF/$1";
$route['search/get_data'] 		 = "search/get_data";
$route['search/(:any)'] 		 = "search/index/$1";
$route['career/(:any)']             = "career/details/$1";


$route['profiles/(:any)']         = "profiles/detail/$1";
$route['50anniversary']           = "anniversary/index";
$route['sitemap.xml']             = "sitemap/xml";
$route['site.webmanifest']        = "home/favicon";
$route['achievements/(:num)']            = "achievements/index/$1";
$route['publication/(:num)']            = "publication/index/$1";
$route['practice']                    = "practice/index";
$route['practice/(:any)']             = "practice/detail/$1";
$route['50th-anniversary']             ='anniversary/index'; 
// URI like '/en/about' -> use controller 'about'
// $route['lib/js/goadmin/plugins/ckfinder/core/connector/php/connector.php?(:any)'] = 'goadmin/ckfinder/connector';
// $route['goadmin/upload/(:any)']                                                = "goadmin/upload/images/$1";
// UNCOMMENT FOR INTERNATIONALIZATION


// '/en' and '/fr' URIs -> use default controller
// UNCOMMENT FOR INTERNATIONALIZATION
require_once(BASEPATH . 'database/DB.php');
$db = &DB();
$query = $db->select('attr')->get_where('language', array('flag' => 1))->result_array();
$query = array_map(function ($val) {
	return $val['attr'];
}, $query);
$query = implode("|", $query);

$route['^(' . $query . ')/(.+)$'] = "$2";
$route['^(' . $query . ')$'] = $route['default_controller'];
// echo "<pre>";
// print_r($route);
// echo "</pre>";
