<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class News_archive extends CI_Controller
{
    var $model         = 'model_front';
    public function __construct()
	{
		parent::__construct();
		$this->load->model($this->model);
    }

	public function index($year = "", $month = "")
	{
		$title = ucfirst($this->lang->line('news_archive'));

		$page  = $this->model_front->get_page(11, TRUE); // section about us
		$achievement = $this->model_front->get_featured_achievement();
		$affiliate = $this->model_front->get_affiliate();


		$asset = array(
			'js'     => array(),
			'css'    => array(),
			'active' => 'news',
			'achievement' => $achievement,
			'affiliate'	=> $affiliate,
			'page'	 => $page,
			'meta'   => meta_create($title),
		);
		$asset['news_year'] = $this->model_front->get_news_year();
		if (!empty($asset['news_year'])) {
			if (empty($year)) {
				$year = $asset['news_year'][0];
			}
		}
		$asset['news_month'] = $this->model_front->get_news_month($year);

		// if (empty($month)) {
		// 	$month = $asset['news_month'][0];
		// }
		$asset['year'] = $year;
		$asset['month'] = $month;

		$asset['news'] = $this->model_front->get_news('', $year, $month);
		// pre($asset['news']);

		$this->load->view('template/header', $asset);
		$this->load->view('archive_view');
		$this->load->view('template/footer');
	}
}