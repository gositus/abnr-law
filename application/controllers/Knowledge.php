<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Knowledge extends CI_Controller
{
    var $model         = 'model_front';
    public function __construct()
	{
		parent::__construct();
		$this->load->model($this->model);
    }

	public function index()
	{
        $title = ucwords($this->lang->line('knowledge_center'));
        $page  = $this->model_front->get_page(17, TRUE); // section about us
        $data_filter = array(
            'start' => 0,
            'limit' => 6
        );

        $achievement = $this->model_front->get_featured_achievement();
        $affiliate = $this->model_front->get_affiliate();


		$asset = array(
			'js'     => array(),
			'css'    => array(),
			'active' => 'knowledge',
            'page'	 => $page,
            'achievement' => $achievement,
            'affiliate'	=> $affiliate,
			'news' => $this->model_front->get_news('', '','','',6),
            'publications'=>$this->model_front->get_publication($data_filter),
            'meta'   => meta_create($title)
		);



		$this->load->view('template/header', $asset);
		$this->load->view('knowledge_center_view');
		$this->load->view('template/footer');
	}
}