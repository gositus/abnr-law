<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Anniversary extends CI_Controller {

	var $model         = 'model_front';

	public function __construct()
	{
		parent::__construct();
		$this->load->model($this->model);
	}

	public function index()
	{		
		$page  = $this->model_front->get_page(16,TRUE); // section about us
		$title=$page['heading'];
		
		
		$asset = array(
			'js'	 => array(),
			'css'	 => array(),
			'active' => 'abnr',
			'page'   => $page,
			'title'  => $title,
			'meta'   => meta_create($title)
		);
		
		$this->load->view('template/header', $asset);	
		$this->load->view('anniversary_view');
		$this->load->view('template/footer');
	}
}