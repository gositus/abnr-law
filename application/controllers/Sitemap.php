<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Sitemap extends CI_Controller
{

	public function xml()
	{
		$asset = array(
			'news' 			=> $this->db->query("select * from news where flag =1")->result_array(),
			'profile'	    => $this->db->query("select * from profile where flag =1")->result_array(),
			'practice'		=> $this->db->query("select * from practice where flag =1")->result_array()
		);

		$this->load->view('sitemap_xml', $asset);
	}
	public function cek_cron()
	{
		$date = date("Y-m-d H:i:s");
		$this->db->insert('crontes', array('date' => $date));
	}
}
