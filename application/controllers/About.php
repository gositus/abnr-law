<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class About extends CI_Controller
{

	var $model         = 'model_front';

	public function __construct()
	{
		parent::__construct();
		$this->load->model($this->model);
	}

	public function index()
	{
		$title = 'ABNR';
		$achievement = $this->model_front->get_featured_achievement();
		$affiliate = $this->model_front->get_affiliate();
		$page  = $this->model_front->get_page(6, TRUE); // section about us


		$asset = array(
			'js'	 => array(),
			'css'	 => array(),
			'active' => 'abnr',
			'page'   => $page,
			'achievement' => $achievement,
			'affiliate'	=> $affiliate,
			'title'  => $title,
			'meta'   => meta_create($title)
		);

		$this->load->view('template/header', $asset);
		$this->load->view('about_view');
		$this->load->view('template/footer');
	}

	public function indonesia_overview()
	{
		$title = ucwords($this->lang->line('abnr_indonesia'));
		$achievement = $this->model_front->get_featured_achievement();
		$affiliate = $this->model_front->get_affiliate();
		$page  = $this->model_front->get_page(4, TRUE); // section about us


		$asset = array(
			'js'	 => array(),
			'css'	 => array(),
			'active' => 'abnr',
			'page'   => $page,
			'achievement' => $achievement,
			'affiliate'	=> $affiliate,
			'title'  => $title,
			'meta'   => meta_create($title)
		);

		$this->load->view('template/header', $asset);
		$this->load->view('indonesia_view');
		$this->load->view('template/footer');
	}

	public function abnr_foundation()
	{
		$title = ucwords($this->lang->line('abnr_foundation'));
		$achievement = $this->model_front->get_featured_achievement();
		$affiliate = $this->model_front->get_affiliate();
		$page  = $this->model_front->get_page(5, TRUE); // section about us


		$asset = array(
			'js'	 => array(),
			'css'	 => array(),
			'active' => 'abnr',
			'achievement' => $achievement,
			'affiliate'	=> $affiliate,
			'page'   => $page,
			'title'  => $title,
			'meta'   => meta_create($title)
		);

		$this->load->view('template/header', $asset);
		$this->load->view('abnr_foundation_view');
		$this->load->view('template/footer');
	}
}
