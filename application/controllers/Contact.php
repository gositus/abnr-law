<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Contact extends CI_Controller
{

	var $model = 'model_message';

	public function __construct()
	{
		parent::__construct();
		$this->load->model($this->model);
		$this->load->model('model_front');
	}

	public function index()
	{
		$title = ucwords($this->lang->line('contact'));
		$lang = current_language();
		$achievement = $this->model_front->get_featured_achievement();
		$affiliate = $this->model_front->get_affiliate();
		$page  = $this->model_front->get_page(12, true); // section contact


		$asset = array(
			'js'     => array(),
			'css'    => array(),
			'active' => 'contact',
			'achievement' => $achievement,
			'affiliate'	=> $affiliate,
			'page'	 => $page,
			'meta'   => meta_create($title)
		);

		$this->load->view('template/header', $asset);
		$this->load->view('contact_us_view');
		$this->load->view('template/footer');
	}

}
