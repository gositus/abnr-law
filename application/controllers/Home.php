<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller
{

	var $model         = 'model_front';

	public function __construct()
	{
		parent::__construct();
		$this->load->model($this->model);
	}

	public function index()
	{
		$banner = $this->model_front->get_banner(1, FALSE);
        $news = $this->model_front->get_news_homepage();
		$achievement = $this->model_front->get_featured_achievement();
		$affiliate = $this->model_front->get_affiliate();

		total_visitor(); // count leads from dashboard
		$asset = array(
			'js'     => array('fancybox/jquery.fancybox.min','flexslider/flexSlider.min'),
			'css'    => array('fancybox/jquery.fancybox.min','flexslider/flexslider.min'),
			'active' => '',
			'banner' => $banner,
			'achievement' => $achievement,
			'affiliate'	=> $affiliate,
            'news'  =>$news
		);

		$this->load->view('template/header', $asset);
		$this->load->view('home_view');
		$this->load->view('template/footer');
	}
	public function cari_captcha()
	{
		$cap = go_captcha();

		$this->session->set_userdata('captchaWord', $cap['word']);

		if (isset($cap['image'])) echo $cap['image'];
		elseif (isset($this->session->userdata['anti_spam'])) echo 'You have failed too many times, Please try again in an hour.';
		else echo 'Clicking too fast.';
	}

	public function cek_ses($key = "")
	{
		$ses_cap =  $this->session->userdata['captchaWord'];

		if ($key == $ses_cap || strtolower($ses_cap) == $key) echo 'true';
		else echo 'false';
	}

	public function new_token()
	{
		echo $this->security->get_csrf_hash();
	}

	public function test_email()
	{
		$email_data = array(
			'to' 		=> 'ratih@gositus.com',
			'title' 	=> 'Testing Email',
			'subject'	=> 'Testing Email',
			'to_name'	=> 'ratih',
			'from'		=> setting_value('email'), // diisi sesuai dengan company "reply to email"
			'name'		=> setting_value('company_name'),
			'reply_to'	=> setting_value('email'),
			'reply_to_name'	=> setting_value('web_title'),
			'message'	=> 'hi'
		);

		//sendemail($email_data);
	}

	public function count($table = '', $id = '')
	{
		$row = $this->db->get_where($table, array('id' => $id))->row_array();

		if (!empty($row)) count_it($table, $id, $row);

		if (!empty($row['seo_url'])) redirect(base_url($table . '/' . $row['seo_url']));
		else redirect($row['url']);
	}

	public function favicon()
	{
		$this->load->view("template/favicon_manifest");
	}

	public function secure_download()
	{

		download_file('files/default-image.png', 'secured_file.png');
	}
}
