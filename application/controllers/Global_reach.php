<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Global_reach extends CI_Controller
{

	var $model         = 'model_front';

	public function __construct()
	{
		parent::__construct();
		$this->load->model($this->model);
	}

	public function index()
	{
		$title = ucwords($this->lang->line('global'));
		$achievement = $this->model_front->get_featured_achievement();
		$affiliate = $this->model_front->get_affiliate();
		$page  = $this->model_front->get_page(3, TRUE); // section about us


		$asset = array(
			'js'     => array(),
			'css'    => array(),
			'active' => 'global',
			'achievement' => $achievement,
			'affiliate'	=> $affiliate,
			'page'   => $page,
			'meta'   => meta_create($title)
		);

		$this->load->view('template/header', $asset);
		$this->load->view('global_view');
		$this->load->view('template/footer');
	}
}
