<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class News extends CI_Controller
{

	var $model         = 'model_front';

	public function __construct()
	{
		parent::__construct();
		$this->load->model($this->model);
	}

	public function index($year = "", $month = "")
	{
		$title = ucfirst($this->lang->line('news'));

		$page  = $this->model_front->get_page(11, TRUE); // section about us
		$achievement = $this->model_front->get_featured_achievement();
		$affiliate = $this->model_front->get_affiliate();


		$asset = array(
			'js'     => array(),
			'css'    => array(),
			'active' => 'news',
			'achievement' => $achievement,
			'affiliate'	=> $affiliate,
			'page'	 => $page,
			'meta'   => meta_create($title),
		);
		$asset['news_year'] = $this->model_front->get_news_year();
		if (!empty($asset['news_year'])) {
			if (empty($year)) {
				$year = $asset['news_year'][0];
			}
		}
		$asset['news_month'] = $this->model_front->get_news_month($year);

		// if (empty($month)) {
		// 	$month = $asset['news_month'][0];
		// }
		$asset['year'] = $year;
		$asset['month'] = $month;

		$asset['news'] = $this->model_front->get_news('', '','','',12);

		$this->load->view('template/header', $asset);
		$this->load->view('news_view');
		$this->load->view('template/footer');
	}

	public function detail($seo_url = "")
	{
		$news 		   = $this->model_front->get_news($seo_url, '', '', TRUE);

        if (empty($news)) redirect(base_url('news'));


		$achievement = $this->model_front->get_featured_achievement();
		$affiliate = $this->model_front->get_affiliate();

		$page  = $this->model_front->get_page(11, TRUE);
		$title         = $news['name'];
		$image 		   = (!empty($news['image'])) ?  base_url('lib/images/news/' . $news['image']) :  base_url('lib/images/page/' . $page['image']);

		count_it('news', $news['id'], $news); // add count di detail news
		total_visitor('news', $news['id']); // to dashboard report

		// disesuaikan dengan url yang akan di buat untuk header json dan xml
		switch ($news['type']) {
			case 1:
				$type = 'news';
				break;
			case 2:
				$type = 'event';
				break;
			case 3:
				$type = 'announcement';
				break;
			case 4:
				$type = 'blog';
				break;
		}

		$asset = array(
			'js'	=> array(),
			'css'	=> array(),
			'active' => 'news',
			'type'  => $type,
			'page'	=> $page,
			'achievement' => $achievement,
			'affiliate'	=> $affiliate,
			'news'	=> $news,
			'meta'	=> array(
				'title'       => !empty($news['meta_title']) ? $news['meta_title'] : meta_create($title, 'web_title'),
				'keyword'     => !empty($news['meta_keyword']) ? $news['meta_keyword'] : meta_create($title, 'meta_keyword'),
				'description' => !empty($news['meta_description']) ? $news['meta_description'] : trim(preg_replace('/\s\s+/', ' ', (preg_replace("/&#?[a-z0-9]{2,8};/i","",substr(strip_tags(($news['content'])),0, 300))))),
				'image'         => $image
			),
		);

        $asset['related_news']= $this->model_front->get_news('', '','','',6,$news['id']);

		$this->load->view('template/header', $asset);
		$this->load->view('news_detail_view');
		$this->load->view('template/footer');
	}



	//Untuk SEO xml khusus News detail dan Event detail > Tolong sesuaikan > link url ada di header
	public function xml($seo_url = "")
	{
		$news = $this->model_front->get_news($seo_url, '', TRUE);

		if (empty($news)) {
			$seo_url = str_replace("-", "_", $seo_url);
			$news = $this->model_front->get_news($seo_url, '', TRUE);
		}
		if (empty($news)) {
			$seo_url = str_replace("_", "-", $seo_url);
			$news = $this->model_front->get_news($seo_url, '', TRUE);
		}

		if (count($news) < 1) redirect(base_url('blog'));
		$image = (!empty($news['image'])) ?  base_url('lib/images/news/' . $news['image']) : base_url('lib/images/gositus/default-image.png');

		$asset = array(
			'news'	=> $news,
			'image'	=> $image
		);

		$this->load->view('seo/news_detail_xml_view', $asset);
	}

	//Untuk SEO json khusus News detail dan Event detail > Tolong sesuaikan > link url ada di header
	public function json($seo_url = "")
	{
		$news = $this->model_front->get_news($seo_url, '', TRUE);
		if (empty($news)) {
			$seo_url = str_replace("-", "_", $seo_url);
			$news = $this->model_front->get_news($seo_url, '', TRUE);
		}
		if (empty($news)) {
			$seo_url = str_replace("_", "-", $seo_url);
			$news = $this->model_front->get_news($seo_url, '', TRUE);
		}

		if (count($news) < 1) redirect(base_url('blog'));
		$image = (!empty($news['image'])) ?  base_url('lib/images/news/' . $news['image']) : base_url('lib/images/gositus/default-image.png');


		$asset = array(
			'title'	=> $news['name'],
			'news'	=> $news,
			'image'	=> $image
		);

		$this->load->view('seo/news_detail_json_view', $asset);
	}
}
