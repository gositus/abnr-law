<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Practice extends CI_Controller
{

	var $model         = 'model_front';

	public function __construct()
	{
		parent::__construct();
		$this->load->model($this->model);
	}

	public function index()
	{
		$title = ucwords($this->lang->line('practice_area'));
		$page  = $this->model_front->get_page(10, true);
		$achievement = $this->model_front->get_featured_achievement();
		$affiliate = $this->model_front->get_affiliate();

		$asset = array(
			'js'     => array(),
			'css'    => array(),
			'active' => 'practice',
			'achievement' => $achievement,
			'affiliate'	=> $affiliate,
			'page'   => $page,
			'meta'   => meta_create($title)
		);

		$asset['practice'] = $this->model_front->get_practice();

		$this->load->view('template/header', $asset);
		$this->load->view('practice_view');
		$this->load->view('template/footer');
	}

	public function detail($seo_url = "")
	{

		$gov_link  = $this->model_front->get_page(15, true);
		$achievement = $this->model_front->get_featured_achievement();
		$affiliate = $this->model_front->get_affiliate();

		$data = $this->model_front->get_practice($seo_url);
		if (empty($data)) {
			redirect('practice');
		}

		$asset = array(
			'js'     => array(),
			'css'    => array(),
			'active' => 'practice',
			'achievement' => $achievement,
			'affiliate'	=> $affiliate,
			'row'	 => $data,
			'gov_link' => $gov_link,
            'practice_achievement'=>$this->model_front->get_achievement_by_practice($data['id']),
			'meta'   => array(
                'title'       => meta_create($data['name'], 'web_title'),
                'keyword'     => meta_create($data['name'], 'meta_keyword'),
                'description' => trim(substr(strip_tags(html_entity_decode($data['content'])),0, 300)),
            )
		);

		$asset['practice'] = $this->model_front->get_practice();

		$this->load->view('template/header', $asset);
		$this->load->view('practice_detail_view');
		$this->load->view('template/footer');
	}
}
