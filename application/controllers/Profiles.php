<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

use Mpdf\Mpdf;
use Mpdf\Config\ConfigVariables;
use Mpdf\Config\FontVariables;

class Profiles extends CI_Controller
{

    var $model = 'model_front';
    var $limit = 15;

    public function __construct()
    {
        parent::__construct();
        $this->load->model($this->model);
    }

    public function index($type = "", $page_num = 1)
    {
        $title = ucfirst($this->lang->line('profiles'));
        $page = $this->model_front->get_page(7, TRUE); // section about us
        $achievement = $this->model_front->get_featured_achievement();
        $affiliate = $this->model_front->get_affiliate();

        $asset = array(
            'js' => array(),
            'css' => array(),
            'active' => 'profile',
            'page' => $page,
            'achievement' => $achievement,
            'affiliate' => $affiliate,
            'type' => $type,
            'meta' => meta_create($title)
        );
        $asset['profile_list'] = $this->model_front->get_profile($type, $this->limit, ($this->limit * ($page_num - 1)));

        $total_data = count($this->model_front->get_profile($type));


        $this->load->library('pagination');
        if (!empty($type) && $type != 'all') {
            $uri=$this->uri->segment_array();
            $burl = site_url('profiles/'.$uri[2]);
        } else {
            $burl = site_url('profiles/all');
        }
        $config['base_url'] = $burl;
        $config['total_rows'] = $total_data;
        $config['per_page'] = $this->limit;
        $config['uri_segment'] = 3;
        $config['num_links'] = 2;

        $config['full_tag_open'] = ' <ul class="pagination justify-content-center">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li class="page-item" aria-current="page">';
        $config['num_tag_close'] = '</li>';

        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';

        $config['prev_link'] = ' Previous';
        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="javascript:;">';
        $config['cur_tag_close'] = '</a></li>';

        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li class="page-item d-none d-sm-block">';
        $config['first_tag_close'] = '</li>';

        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li class="page-item d-none d-sm-block">';
        $config['last_tag_close'] = '</li>';

        $config['use_page_numbers'] = TRUE;
        $config['attributes'] = array('class' => 'page-link');


        $this->pagination->initialize($config);
        $asset['pagination'] = $this->pagination->create_links();


        $this->load->view('template/header', $asset);
        $this->load->view('profile_view');
        $this->load->view('template/footer', $asset);
    }

    public function detail($seo_url)
    {

        $data = $this->model_front->get_profile_by_seo_url($seo_url);
        $page = $this->model_front->get_page(7, TRUE);
        $achievement = $this->model_front->get_featured_achievement();
        $affiliate = $this->model_front->get_affiliate();

        $asset = array(
            'js' => array('flexscroll/flexscroll', 'flexscroll/init_flexscroll'),
            'css' => array('flexscroll/flexscroll'),
            'active' => 'profile',
            'profile' => $data,
            'page' => $page,
            'achievement' => $achievement,
            'affiliate' => $affiliate,
            'profile_achievement'=>$this->model_front->get_achievement_by_profile($data['id']),
            'meta' => array(
                'title' => meta_create($data['name'], 'web_title'),
                'keyword' => meta_create($data['name'], 'meta_keyword'),
                'description' => trim(substr(strip_tags(html_entity_decode($data['content'])), 0, 300)),
            ),
        );

        if (empty($data)) {
            redirect('profiles');
        }
//		else if($data['type'] == 2){
//			$asset['related']     = $this->model_front->get_profile(7);
//			$asset['related_sub'] = $this->model_front->get_profile($data['type']);
//			$asset['main_type']   = $data['type'];
//		}
//		else if($data['type'] == 7){
//			$asset['related']     = $this->model_front->get_profile($data['type']);
//			$asset['main_type']   = $data['type'];
//			$asset['related_sub'] = $this->model_front->get_profile(2);
//		}else{
        $asset['related'] = $this->model_front->get_profile($data['type']);
        $asset['related_sub'] = FALSE;
        $asset['main_type'] = $data['type'];
//		}

        $asset['award'] = $this->model_front->get_award_profile($data['id']);
        // pre($asset);

        $this->load->view('template/header', $asset);
        $this->load->view('profile_detail_view');
        $this->load->view('template/footer', $asset);
    }

    public function type($type = "")
    {

        $data = $this->model_front->get_profile($type);
        $page = $this->model_front->get_page(7, TRUE);
        $achievement = $this->model_front->get_featured_achievement();
        $affiliate = $this->model_front->get_affiliate();

        $asset = array(
            'js' => array('flexscroll/flexscroll', 'flexscroll/init_flexscroll'),
            'css' => array('flexscroll/flexscroll'),
            'active' => 'profile',
            'profile' => $data[0],
            'page' => $page,
            'achievement' => $achievement,
            'affiliate' => $affiliate,
        );

        // pre($data);

        if (empty($data)) {
            redirect('profiles');
        }
//		else if($data[0]['type'] == 2){
//			$asset['related']     = $data;
//			$asset['related_sub'] = $this->model_front->get_profile(7);
//			$asset['main_type']   = $data[0]['type'];
//		}
//		else if($data[0]['type'] == 7){
//			$asset['related']     = $data;
//			$asset['related_sub'] = $this->model_front->get_profile(2);
//			$asset['main_type']   = $data[0]['type'];
//		}else{
        $asset['related'] = $this->model_front->get_profile($data[0]['type']);
        $asset['related_sub'] = FALSE;
        $asset['main_type'] = $data[0]['type'];
//		}

        if ($type != 4) {
            $title = $data[0]['name'];
        } else {
            $title = ucwords($this->lang->line('assistant_law'));
        }
        $asset['meta'] = meta_create($title);

        // $asset['related'] = $data;
        $asset['award'] = $this->model_front->get_award_profile($data[0]['id']);

        $this->load->view('template/header', $asset);
        if ($type != 4) {
            if ($type == 2) {
                $top_data = $this->model_front->get_profile(7);
                $asset['top_related'] = $top_data;
            }
            $this->load->view('profile_detail_view');
        } else {
            $this->load->view('profile_assistant_law_detail_view');
        }
        $this->load->view('template/footer', $asset);
    }

    public function getData()
    {
        $data = $this->input->post();
        $start = ($this->limit * ($data['page'] - 1));
        $result = [];
        $result['profile_list'] = $this->model_front->get_profile('', $this->limit, $start);
        $result['profile_total'] = count($this->model_front->get_profile());
        $result['page_number'] = ceil($result['profile_total'] / $this->limit);
        $html = '';
        foreach ($result['profile_list'] as $value) {
            $html .= '<div class="col-list" data-type="partners">
                            <div class="isotope-item">
                                <figure>
                                    <a href="' . base_url('profiles/' . $value['seo_url']) . '">
                                        <img src="' . base_url('lib/images/page/profiles_placeholder.png') . '" alt="' . $value['name'] . '">
                                    </a>
                                </figure>
                                <figcaption>
                                    <div class="figbody">
                                        <h5>' . strtoupper($this->lang->line('partners')) . '</h5>
                                        <a href="' . base_url('profiles/' . $value['seo_url']) . '">' . strtoupper($value['name']) . '</a>
                                    </div>
                                    <div class="figfooter">
                                        <a href="mailto:' . $value['email'] . '">' . $value['email'] . '</a>
                                        <a href="telp:' . $value['email'] . '">' . $value['phone'] . '</a>
                                    </div>
                                </figcaption>
                            </div>
                        </div>';
        }

        echo $html;
    }

    public function getPDF($seo_url=""){
        $data = $this->model_front->get_profile_by_seo_url($seo_url);
        $asset = array(
            'profile'=>$data
        );

        $fontDir = [FCPATH . '/lib/css/webfonts/fonts/'];

        // Font data (font key and file mapping)
        $fontData = [
            'frutiger' => [
                'B' => 'VonnesTTBook.ttf',
                'R' => 'VonnesTTLight.ttf',
            ]
        ];

        // Initialize mPDF
        $mpdf = new Mpdf([
            'mode' => '+aCJK', 
            'fontDir' => $fontDir,
            'fontdata' => $fontData,
            'useAdobeCJK' => true,
            'allow_charset_conversion' => true,
            'margin_top' => 35,
            "autoScriptToLang" => true,
            "autoLangToFont" => true,
        ]);
        $mpdf->curlAllowUnsafeSslRequests = true;
        $html='<img width="140" src="'.base_url('lib/images/logo/' . setting_value('logo')).'" alt="'.setting_value('company_name').'">';
        $mpdf->SetHTMLHeader($html);


        // Your HTML content to convert to PDF
        $this->load->view('pdf/profile',$asset,'');
        $html = $this->output->get_output();
        $mpdf->WriteHTML($html);
        $mpdf->Output($data['name'].'.pdf', 'I');
    }
}
