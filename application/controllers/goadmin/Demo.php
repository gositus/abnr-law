<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Demo extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		check_login();
	}
	
	public function index()
	{
		check_access('demo', 'menu', TRUE);
		
		$asset = array(
					'title'	=> 'Demo',
					'url'	=> '',
					'js'	=> array(),
					'css'	=> array(),
				);

		$this->load->view('admin/template/header', $asset);
		$this->load->view('admin/template/menu');
		$this->load->view('admin/demo/form_view');
		$this->load->view('admin/template/footer');
	}
}