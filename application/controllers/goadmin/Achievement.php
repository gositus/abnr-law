<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Achievement extends CI_Controller
{

	// Also for table name
	var $url           = 'achievement'; //  nama table disarankan sama dengan  nama url
	var $model         = 'model_achievement';
	var $title         = 'Achievement';
	var $dt_serverside = FALSE;
	var $image_width         = 160;
	var $image_height        = 160;

	var $image_width_affiliations       = 300;
	var $image_height_affiliations        = 150;

	public function __construct()
	{
		parent::__construct();
		check_login();

		$this->load->model($this->model);
        $this->load->model('model_practice');
        $this->load->model('model_profile');
	}

	public function index()
	{
		check_access($this->url, 'menu', TRUE);

		$asset = array(
			'title'	=> $this->title,
			'url'	=> $this->url,
			'js'	=> array(),
			'css'	=> array()
		);

		$this->load->view('admin/template/header', $asset);
		$this->load->view('admin/template/menu');
		$this->load->view('admin/' . $this->url . '/list');
		$this->load->view('admin/template/footer');
	}

	public function list_data()
	{
		// SEND DATA TO DATATABLE
		$model_name = $this->model;
		check_access($this->url, 'menu', TRUE);

		$where = array('a.flag !=' => 3);
		if ($this->dt_serverside) {
			$asset['data'] = select_all_row($this->url);
			$output        = array(
				'draw'            => $this->input->post('draw'),
				"recordsTotal"    => dt_countTotal($this->url, $where, $join),
				"recordsFiltered" => dt_countFiltered($this->url, $where, $join),
				"data"            => $asset['data'],
			);
		} else {
			$asset = array(
				'data' => $this->$model_name->get_list()
			);
			$output = array('data' => $asset['data']);
		}
		echo json_encode($output);
	}

	public function add()
	{
		check_access($this->url, 'add', TRUE);

		$model_name = $this->model;
		$asset = array(
			'title'	=> "Add " . $this->title,
			'url'	=> $this->url,
			'js'	=> array('form', 'fileFinder'),
			'css'	=> array(),
            'practice'=>$this->model_practice->get_practice(),
            'publication'=>select_all_row('achievement_publication',array('flag !='=>3)),
            'profile'=>$this->model_profile->get_profile()
		);

		// Get all parent pages
		$where = array('flag !=' => 3);
		$this->form_validation->set_rules('flag', 'Status', 'trim|required');

		$this->form_validation->set_error_delimiters('<li>', '</li>');

		if ($this->form_validation->run() == FALSE) {
			$this->load->view('admin/template/header', $asset);
			$this->load->view('admin/template/menu');
			$this->load->view('admin/' . $this->url . '/add');
			$this->load->view('admin/template/footer');
		} else {

			$this->$model_name->insert();
			$this->session->set_flashdata('success', 'Data succesfully saved!');
			redirect(base_url() . 'goadmin/' . $this->url);
		}
	}

	public function view($item_id)
	{

		check_access($this->url, 'read', TRUE);

		$check = select_all_row($this->url, array('id' => $item_id, 'flag !=' => 3), TRUE);
        $practice_list=select_all_row('achievement_to_practice',array('achievement_id'=>$item_id),'','asc','practice_id');
        $practice_ids = array_column($practice_list, 'practice_id');

        $profile_list=select_all_row('achievement_to_profile',array('achievement_id'=>$item_id),'','asc','profile_id');
        $profile_ids = array_column($profile_list, 'profile_id');

		$model_name = $this->model;
		if ($check) {
			$asset = array(
				'title'	=> $this->title,
				'url'	=> $this->url,
				'js'	=> array('form', 'list', 'log', 'fileFinder'),
				'css'	=> array(),
                'practice'=>$this->model_practice->get_practice(),
                'publication'=>select_all_row('achievement_publication',array('flag !='=>3)),
                'practice_selected'=>$practice_ids,
                'profile'=>$this->model_profile->get_profile(),
                'profile_selected'=>$profile_ids,
			);

			$asset['row']     = $this->$model_name->get_detail($item_id);
			$asset['default'] = setting_value('default_language');
			$asset['title']   =  $this->title . ' - ' . $asset['row'][$asset['default']]['name'];

			$where = array('flag !=' => 3);

			$this->form_validation->set_rules('flag', 'Status', 'trim|required');

			$this->form_validation->set_error_delimiters('<li>', '</li>');

			if ($this->form_validation->run() == FALSE) {
				$this->load->view('admin/template/header', $asset);
				$this->load->view('admin/template/menu');
				$this->load->view('admin/' . $this->url . '/view');
				$this->load->view('admin/template/footer');
			} else {
                check_access($this->url, 'edit', TRUE);
				$this->$model_name->update();
				$this->session->set_flashdata('success', 'Data has been changed!');
				redirect(base_url() . 'goadmin/' . $this->url);
			}
		} else redirect(base_url() . 'goadmin/' . $this->url);
	}
}
