<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		check_login();
		$this->load->helper('url');
	}

	public function flag()
	{
		if ($this->input->post()) {
            if(check_access($this->input->post('table'), 'edit')) {
                $this->model_process->flag();
            }else{
				redirect(base_url('goadmin'));
			}
			echo $this->security->get_csrf_hash();
		} else {
			redirect(base_url('goadmin'));
		}
	}

	public function delete()
	{
		if ($this->input->post()) {
            if(check_access($this->input->post('table'), 'delete')) {
                $this->model_process->delete();
            }else{
				redirect(base_url('goadmin'));
			}
			echo $this->security->get_csrf_hash();
		} else {
			redirect(base_url('goadmin'));
		}
	}

	public function delete_image()
	{
		if ($this->input->post()) {
			if(check_access($this->input->post('table'), 'edit')) {
				$this->model_process->delete_image();
            }else{
				http_response_code(403);
			}
			echo $this->security->get_csrf_hash();
		} else {
			redirect(base_url('goadmin'));
		}
	}

	public function delete_file()
	{
		if ($this->input->post()) {
			if(check_access($this->input->post('table'), 'edit')) {
				$this->model_process->delete_file();
            }else{
				http_response_code(403);
			}
			echo $this->security->get_csrf_hash();
		} else {
			redirect(base_url('goadmin'));
		}
	}

	public function upload_image(){
		$respon = [
			'uploaded' => false,
			'error' => [
				'messages' => 'Cannot upload file'
			]
		];


		if(!empty($_FILES['upload'])){
			$image =  file_upload_name('upload', 'lib/images/upload');
			if(!empty($image)){
				$filename = $image['file_name'];

				$respon = [
                    'uploaded' => true,
                    'url' => base_url('/lib/images/upload/' . $filename)
                ];
			}
		}

		echo json_encode($respon);
		
	}
}
