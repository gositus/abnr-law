<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends CI_Controller
{

    // Also for table name
    var $url           = 'profile'; //  nama table disarankan sama dengan  nama url
    var $model         = 'model_profile';
    var $title         = 'Profile';
    var $dt_serverside = FALSE;
    var $image_width         = 800;
    var $image_height        = 275;
    var $image_width_right         = 453;
    var $image_height_right        = 275;
    var $image_profile_width  =160;
    var $image_profile_height =200;

    public function __construct()
    {
        parent::__construct();
        check_login();

        $this->load->model($this->model);
    }

    public function index($id="1")
    {
        check_access($this->url, 'menu', TRUE);

        $asset = array(
            'title'    => $this->title,
            'url'    => $this->url,
            'js'    => array(),
            'css'    => array(),
            'data_profile' =>$id
        );

        $this->load->view('admin/template/header', $asset);
        $this->load->view('admin/template/menu');
        $this->load->view('admin/' . $this->url . '/list');
        $this->load->view('admin/template/footer');
    }

    public function list_data($data=1)
    {
        // SEND DATA TO DATATABLE
        $model_name = $this->model;
        check_access($this->url, 'menu', TRUE);

        $where = array('a.flag !=' => 3);
        if ($this->dt_serverside) {
            $asset['data'] = select_all_row($this->url);
            $output        = array(
                'draw'            => $this->input->post('draw'),
                "recordsTotal"    => dt_countTotal($this->url, $where, $join),
                "recordsFiltered" => dt_countFiltered($this->url, $where, $join),
                "data"            => $asset['data'],
            );
        } else {
            $asset = array(
                'data' => $this->$model_name->get_list($data)
            );
            $output = array('data' => $asset['data']);
        }
        echo json_encode($output);
    }

    public function add($data=1)
    {
        check_access($this->url, 'add', TRUE);

        $model_name = $this->model;
        $asset = array(
            'title'    => "Add " . $this->title,
            'url'    => $this->url,
            'js'    => array('form', 'fileFinder'),
            'css'    => array('plugins/jqueryUI/jquery-ui.min'),
            'data_profile' =>$data
        );

        // Get all parent banners
        $where = array('flag !=' => 3);
        $asset['section'] = select_all_row('section', array('type' => 2, 'flag' => 1));

        $this->form_validation->set_rules('flag', 'Status', 'trim|required');

        $this->form_validation->set_error_delimiters('<li>', '</li>');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('admin/template/header', $asset);
            $this->load->view('admin/template/menu');
            $this->load->view('admin/' . $this->url . '/add');
            $this->load->view('admin/template/footer');
        } else {

            $this->$model_name->insert();
            $this->session->set_flashdata('success', 'Data succesfully saved!');
            redirect(base_url() . 'goadmin/' . $this->url.'/'.$this->input->post('type'));
        }
    }

    public function view($item_id)
    {

        check_access($this->url, 'read', TRUE);

        $check = select_all_row($this->url, array('id' => $item_id, 'flag !=' => 3), TRUE);
        $model_name = $this->model;
        if ($check) {
            $detail = $this->$model_name->get_detail($item_id);
            $asset = array(
                'title'    => $this->title  . ' - ' . $detail[setting_value('default_language')]['name'],
                'url'    => $this->url,
                'js'    => array('form', 'list', 'log', 'fileFinder'),
                'css'    => array('plugins/jqueryUI/jquery-ui.min')
            );
            $asset['row'] = $detail;
            $asset['default'] = setting_value('default_language');

            $asset['award_data'] = $this->$model_name->get_award($item_id);

            $this->form_validation->set_rules('flag', 'Status', 'trim|required');


            $this->form_validation->set_error_delimiters('<li>', '</li>');

            if ($this->form_validation->run() == FALSE) {
                $this->load->view('admin/template/header', $asset);
                $this->load->view('admin/template/menu');
                $this->load->view('admin/' . $this->url . '/view');
                $this->load->view('admin/template/footer');
            } else {
                check_access($this->url, 'edit', TRUE);
                $this->$model_name->update();
                $this->session->set_flashdata('success', 'Data has been changed!');
                redirect(base_url() . 'goadmin/' . $this->url.'/'.$this->input->post('type'));
            }
        } else redirect(base_url() . 'goadmin/' . $this->url);
    }

    public function delete_award($id = "")
    {
        $this->db->delete('profile_award', array('id' => $id));
    }

    public function order_up($id = 0)
    {
        $model_name = $this->model;
        $this->$model_name->order_up($id);
    }
    public function order_down($id = 0)
    {
        $model_name = $this->model;
        $this->$model_name->order_down($id);
    }

    public function get_sort($type_id=""){
        $data=$this->db->order_by('sort','desc')->select('*')->get_where($this->url,array())->row_array();
        echo json_encode($data);
    }
}
