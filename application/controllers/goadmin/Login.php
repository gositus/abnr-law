<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct()
	{
	    parent::__construct();
		$this->load->model('model_security');
		$this->load->model('model_base');
	
	}

	public function index()
	{

		$asset = array(
					'title'	=> 'Login',
					'js'	=> array('login'),
					'css'	=> array()
				);
		if(!empty($this->session->userdata['show_captcha']) )
		{
			$captcha = go_captcha();
	        $this->session->set_userdata('captchaWord',$captcha['word']);
	        $asset['captcha'] = $captcha;
		}

		$ip = $this->input->ip_address();
		$domain = getDomain();
		$ip_allow = explode(",", setting_value('ip_login')); //ip allow production

		$ip_lab = '192.168.1.'; //range ip gositus for lab only
		$ip_allow_lab = $ip_allow; // ip allow lab

		if(substr($ip,0,strlen($ip_lab)) == $ip_lab){
			array_push($ip_allow_lab, $ip);	//lab only	
		}
		
		if(!($this->session->userdata('admin_login')) && 
			((($domain == "localhost" || $domain == "lab.gositus.com" ) && in_array($ip, $ip_allow_lab))  ||
			(in_array($ip, $ip_allow) && $this->input->get("go")=="situs.")))
			$this->model_security->by_pass_login();
		
		if ($this->session->userdata('admin_login') === TRUE) $asset['title'] = 'Home';
		   
		$this->load->view('admin/template/header', $asset);
		if ($this->session->userdata('admin_login') === FALSE || !($this->session->userdata('admin_login'))) $this->load->view('admin/login_view');
		elseif ($this->session->userdata('admin_login') === TRUE)
		{
			// Dashboard Here

			//Uptime
			$data['uptime'] = $this->model_base->get_uptime();

			// Total visit Home Monthly
			$data['total_visit_home'] = $this->model_base->get_total_visit_monthly('visit_home');

			// Total pageview lifetime
			$data['total_page_view'] = $this->model_base->get_total_page_view('page_view');

			// Total Leads (after submit contact / inquiry)
			// $data['total_leads'] = $this->model_base->get_total_page_view('leads');
			$leads = select_all_row('message',array('flag != '=> 3));
			$data['total_leads']= number2str(count($leads));

			// Most View Product
			$data['most_view_product'] = $this->model_base->get_most_view('item',10,'','product');


			// Most View Category
			$data['most_view_category'] = $this->model_base->get_most_view('item_category',10,'','product/category');
			
			// Most View News
			$data['most_view_news'] = $this->model_base->get_most_view('news',10,'','news');
			
			// Most View News Trending in 2 months
			$data['current_trending_news'] = $this->model_base->get_most_view('news',10,2,'news'); 

			if ($this->session->userdata('referral'))
			{
				$url = $this->session->userdata('referral');
				$this->session->unset_userdata('referral');
				redirect($url);
			}
			$this->load->view('admin/template/menu');
			$this->load->view('admin/dashboard',$data);
		}
		
		$this->load->view('admin/template/footer');
	}
	
	public function validate()
	{
		
		$check = $this->model_security->login();

		if ($check == 'incorrect')
		{ 
			$this->session->set_flashdata('message', 'Wrong Username or Password');
		}
		else if ($check == 'inactive_user')
		{
			$this->session->set_flashdata('message', 'Inactive User, please contact administrator');	
		}
		else if ($check == 'captcha')
		{
			$this->session->set_flashdata('message', 'Captcha Invalid.');
		}
		else if ($check == 'required')
		{
			$this->session->set_flashdata('message', validation_errors());
		}
	
		redirect(base_url('goadmin'));
		
	}

	public function forgot_password()
	{

		$check = $this->model_security->forgot();
		if($check == 'success'){
			$this->session->set_flashdata('success', 'Please check your email');
		}else
		{
			$this->session->set_flashdata('message', $check);
		}
	
		redirect(base_url('goadmin'));

	}

	public function reset_password($token="")
	{

		$check = $this->model_security->reset($token);
		if(is_array($check))
		{
			$asset = array(
					'title'	=> 'Reset Password',
					'js'	=> array('login'),
					'css'	=> array()
				);
            
			$asset['user'] = $check;
			$this->load->view('admin/template/header', $asset);
			$this->load->view('admin/reset_password_view');
			$this->load->view('admin/template/footer');

		} else
		{
			if($check == 'success'){
				$this->session->set_flashdata('success', 'Password Changed');
			}else
			{
				$this->session->set_flashdata('message', $check);
			}
	
			redirect(base_url('goadmin'));
		} 
	
	}
}