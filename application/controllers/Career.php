<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Career extends CI_Controller
{

	var $model         = 'model_front';

	public function __construct()
	{
		parent::__construct();
		$this->load->model($this->model);
	}

	public function index()
	{
		$title = ucfirst($this->lang->line('join_us'));
		$achievement = $this->model_front->get_featured_achievement();
		$affiliate = $this->model_front->get_affiliate();
		$page  = $this->model_front->get_page(13, TRUE); // section about us
        $banner  = $this->model_front->get_banner(18, FALSE);


		$asset = array(
			'js'	 => array('fancybox/jquery.fancybox.min'),
			'css'	 => array('fancybox/jquery.fancybox.min'),
			'active' => 'career',
			'page'   => $page,
            'banner'	=> $banner,
			'achievement' => $achievement,
			'affiliate'	=> $affiliate,
			'meta'   => meta_create($title),
			'career' => $this->model_front->get_career()
		);

		$this->load->view('template/header', $asset);
		$this->load->view('career_view');
		$this->load->view('template/footer');
	}

	public function details($seo_url = '')
	{
		$career = $this->model_front->get_career($seo_url, TRUE);
		if($career){
			$title = ucwords($career['name']);
			$page  = $this->model_front->get_page(13, TRUE); 

			$asset = array(
				'js'	 => array(),
				'css'	 => array(),
				'active' => 'career',
				'page'   => $page,
				'meta'   => meta_create($title),
				'career' => $career,
				'list'   => $this->model_front->get_career()
			);

			$this->load->view('template/header', $asset);
			$this->load->view('career_detail_view');
			$this->load->view('template/footer');
		}else{
			show_404();
	}
	}
}
