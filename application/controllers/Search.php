<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Search extends CI_Controller
{

    var $model         = 'model_front';

    public function __construct()
    {
        parent::__construct();
        $this->load->model($this->model);
    }

    public function index($text = "")
    {
        if (!empty($this->input->post('text'))) {
            $text = $this->input->post('text');
            redirect('search/' . $text);
        }
        if (empty($text)) {
            redirect(base_url());
        }
        $text=urldecode(input_clean($text));
        $title = ucfirst($this->lang->line('search'));
        $page  = $this->model_front->get_page(14, TRUE);

        $achievement = $this->model_front->get_featured_achievement();
        $affiliate = $this->model_front->get_affiliate();

        $asset = array(
            'js'     => array(),
            'css'    => array(),
            'active' => 'search',
            'page'   => $page,
            'achievement' => $achievement,
            'affiliate'	=> $affiliate,
            'meta'   => meta_create($title),
            'search_text'   => $text
        );

        $asset['news'] = $this->model_front->search($text, 'news',5);
        $asset['profile'] = $this->model_front->search($text, 'profile',5);
        $asset['achievements'] = $this->model_front->search($text, 'achievement',5);
        $asset['document'] = $this->model_front->search($text, 'document',5);
        $asset['practice'] = $this->model_front->search($text, 'practice',5);

        $asset['total_news'] = count($this->model_front->search($text, 'news'));
        $asset['total_profile'] = count($this->model_front->search($text, 'profile'));
        $asset['total_achievements'] = count($this->model_front->search($text, 'achievement'));
        $asset['total_document'] = count($this->model_front->search($text, 'document'));
        $asset['total_practice'] = count($this->model_front->search($text, 'practice'));


        $this->load->view('template/header', $asset);
        $this->load->view('search_view');
        $this->load->view('template/footer');
    }

    public function get_data(){
        $result=$this->model_front->search(urldecode(input_clean($this->input->post('serach_text'))),urldecode(input_clean($this->input->post('page'))),5,input_clean($this->input->post('page_number'))*5);

        foreach ($result as $key=> $value){
            $page=$this->input->post('page');
            if(!empty($value['seo_url'])) {
                if($page=='profile'){
                    $page='profiles';
                    if(!empty($value['profile_image'])){
                        $result[$key]['profile_image']=base_url('lib/images/'.input_clean($this->input->post('page')).'/' . $value['profile_image']);
                    }else{
                        $result[$key]['profile_image']=base_url('lib/images/page/profiles_placeholder.png');
                    }
                }
                $result[$key]['seo_url'] = base_url(input_clean($page) . '/' . $value['seo_url']);
            }else{
                $result[$key]['seo_url']='javascript:;';
            }

            if(!empty($value['start'])){
               $result[$key]['start']=format_date($value['start']);
            }

            if($page=='document'){
                $result[$key]['file']=base_url('files/document/' . $value['file']);
                $result[$key]['publish_date']=format_date($value["publish_date"]);
                $result[$key]['publisher']=select_all_row('publisher', array('id' => $value['publisher']), true)['name'];
            }

            if(!empty($value['image'])){
                $result[$key]['image']=base_url('lib/images/'.input_clean($this->input->post('page')).'/' . $value['image']);
            }else{
                $result[$key]['image']=base_url('lib/images/'.input_clean($this->input->post('page')).'/' . $value['image']);
            }

            $result[$key]['content']=character_limiter(strip_tags($value['content']), 400);
        }

        echo json_encode($result);
    }
}
