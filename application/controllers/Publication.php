<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Publication extends CI_Controller
{

	var $model         = 'model_front';
	var $limit         = 12;

	public function __construct()
	{
		parent::__construct();
		$this->load->model($this->model);
	}

	public function index($page = 1)
	{
		$title = ucwords($this->lang->line('publication'));
		$achievement = $this->model_front->get_featured_achievement();
		$affiliate = $this->model_front->get_affiliate();

		$asset = array(
			'js'     => array(),
			'css'    => array(),
			'active' => 'publication',
			'achievement' => $achievement,
			'affiliate'	=> $affiliate,
			'page'   => $this->model_front->get_page(9, TRUE),
			'meta'   => meta_create($title)
		);

		$data_filter = array(
			'start' => $this->limit * ($page - 1),
			'limit' => $this->limit
		);

		$asset['publications'] = $this->model_front->get_publication($data_filter);

		$total_news = count($this->model_front->get_publication());

		$this->load->library('pagination');

		$config['base_url'] = site_url('publication');
		$config['total_rows'] = $total_news;
		$config['per_page'] = $this->limit;
		$config['uri_segment'] = 2;
		$config['num_links'] = 2;

		$config['full_tag_open'] = ' <ul class="pagination justify-content-center">';
		$config['full_tag_close'] = '</ul>';

		$config['num_tag_open'] = '<li class="page-item" aria-current="page">';
		$config['num_tag_close'] = '</li>';

		$config['next_link'] = 'Next';
		$config['next_tag_open'] = '<li class="page-item">';
		$config['next_tag_close'] = '</li>';

		$config['prev_link'] = ' Previous';
		$config['prev_tag_open'] = '<li class="page-item">';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="javascript:;">';
		$config['cur_tag_close'] = '</a></li>';

		$config['first_link'] = 'First';
		$config['first_tag_open'] = '<li class="page-item d-none d-sm-block">';
		$config['first_tag_close'] = '</li>';

		$config['last_link'] = 'Last';
		$config['last_tag_open'] = '<li class="page-item d-none d-sm-block">';
		$config['last_tag_close'] = '</li>';

		$config['use_page_numbers'] = TRUE;
		$config['attributes'] = array('class' => 'page-link');


		$this->pagination->initialize($config);
		$asset['pagination'] = $this->pagination->create_links();

		$this->load->view('template/header', $asset);
		$this->load->view('publication_view');
		$this->load->view('template/footer');
	}
}
