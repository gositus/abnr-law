<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Newsletter extends CI_Controller {

	var $model         = 'model_front';

	public function __construct()
	{
		parent::__construct();
		$this->load->model($this->model);
	}

	public function add()
	{
		if(!$this->input->post()) redirect(base_url());

       	$this->form_validation->set_rules('email_newsletter', 'Email', 'trim|valid_email|required');
		
       	if ($this->form_validation->run() == TRUE){
			$sub = $this->model_front->add_subscriber();
			$this->session->set_flashdata("newsletter", "Success subcribe newsletter");
			redirect(base_url());
		} else{
			$this->session->set_flashdata("newsletter", validation_errors());
			redirect(base_url());
		}
	}
}