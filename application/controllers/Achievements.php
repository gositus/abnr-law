<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Achievements extends CI_Controller
{

    var $model         = 'model_front';

    public function __construct()
    {
        parent::__construct();
        $this->load->model($this->model);
    }

    public function index($year = "")
    {
        $title = ucfirst($this->lang->line('achievements'));
        $page  = $this->model_front->get_page(8, TRUE); // section about us
        $achievement = $this->model_front->get_featured_achievement();
        $affiliate = $this->model_front->get_affiliate();

        $asset = array(
            'js'	=> array(),
            'css'	=> array(),
            'active' => 'achievements',
            'page'  => $page,
            'achievement' => $achievement,
            'affiliate'	=> $affiliate,
            'meta'  => meta_create($title)
        );

        $asset['year'] = $this->model_front->get_achievement_year();
        $achievement=[];
        foreach ($asset['year'] as $key=>$value){
            $achievement[$key]['year']=$value;
            $achievement[$key]['achievement']=$this->model_front->get_achievement($value);
        }

        $asset['achievements']=$achievement;

        $this->load->view('template/header', $asset);
        $this->load->view('achievements_view');
        $this->load->view('template/footer');
    }
}
