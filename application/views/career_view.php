<div class="banner">
	<div class="container">
        <div class="row align-items-end no-gutters caption-banner archive">
            <div class="col-12 col-sm-12 col-md-12 col-lg-8">
                <img class="w-100 banner-left profile-index" src="<?php echo base_url('lib/images/page/' . $page['image']) ?>" alt="<?php echo $page['name'] ?>">
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-4">
                <!-- <img class="w-100 banner-right" src="<?php echo base_url('lib/images/page/' . $page['image_right']) ?>" alt="<?php echo $page['name'] ?>"> -->
                <div class="banner-right">
                    <h1><?php echo $page['heading'] ?></h1>
                </div>
            </div>
        </div>
	</div>
</div>
<div class="mid-content py-4">
	<div class="container">
		<div class="row justify-content-center">

			<div class="col-md-8 col-lg-6">
				<div class="description">
					<?php echo $page['content'] ?>
				    <?php foreach ($banner as $value){ ?>
					<a href="<?php echo base_url('lib/images/banner/'.$value['image']) ?>" data-fancybox="gallery">
					    <img src="<?php echo base_url('lib/images/banner/'.$value['image']) ?>" class="w-100 mt-4" alt="<?php echo $value['name'] ?>">
					</a>
                    <?php } ?>
				

					<?php if (count($career) > 0) { ?>
						<ul class="mt-2">
							<?php foreach ($career as $key => $value) { ?>
								<li>
									<a href="<?php echo base_url('career/' . $value['seo_url']) ?>"><?php echo strtoupper($value['name']) ?></a>
								</li>
							<?php } ?>
						</ul>
					<?php } ?>
				</div>
			</div>

		</div>


	</div>
</div>
<script type="text/javascript">
	$(function() {
		$('[data-fancybox]').fancybox();
	
			$('.fancybox').fancybox({
			
			}); // fancybox
	});
</script>