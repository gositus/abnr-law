<div class="banner">
	<div class="container">
		<div class="row align-items-end no-gutters publication caption-banner">
			<div class="col-12 col-sm-12 col-md-12 col-lg-8">
				<img class="w-100 banner-left publication" src="<?php echo base_url('lib/images/page/' . $page['image']) ?>" alt="<?php echo $page['name'] ?>">
			</div>
			<div class="col-12 col-sm-12 col-md-12 col-lg-4">
				<!-- <img class="w-100 banner-right" src="<?php echo base_url('lib/images/page/' . $page['image_right']) ?>" alt="<?php echo $page['name'] ?>"> -->
				<div class="banner-right">
					<h1>Publications</h1>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="publication">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="publication-lists">
					<?php foreach ($publications as $key => $value) { ?>
						<div class="col-list">
							<div class="media">
								<?php if (!empty($value['image'])) { ?>
									<a target="_blank" href="<?php echo base_url('files/document/' . $value['file']) ?>">
										<img src="<?php echo base_url('lib/images/document/' . $value['image']) ?>" alt="<?php echo $value['name'] ?>">
									</a>
								<?php } else { ?>
									<a target="_blank" href="<?php echo base_url('files/document/' . $value['file']) ?>">
										<img src="<?php echo base_url('lib/images/document/default.jpg') ?>" alt="<?php echo $value['name'] ?>">
									</a>
								<?php } ?>

								<div class="media-body">
									<span class="date"><?php echo format_date($value["publish_date"]) ?></span>
									<a target="_blank" href="<?php echo base_url('files/document/' . $value['file']) ?>">
										<h5><?php echo $value['name'] ?></h5>
										<span>[<?php echo filesize_formatted(FCPATH . 'files/document/' . $value['file']) ?>]</span>
									</a> 
									
									<div class="media-footer">
										<span>Author: <?php echo $value['author'] ?></span>
										<span>Publisher: <?php echo $value['publisher'] ?></span>
									</div>
								</div>
							</div>
						</div>
					<?php } ?>
				</div>
				<nav class="pagination pagination-cs pagination-sm justify-content-center">
					<?php echo $pagination ?>
				</nav>
			</div>
		</div>
	</div>
</div>

<div class="mid-content py-4 d-none">
	<div class="container">
		<div class="row justify-content-center">

			<div class="col-md-8 col-lg-8">
				<div class="description publication">
					<div class="side-right">
						<?php foreach ($publications as $key => $value) { ?>
							<div class="media">
								<?php if (!empty($value['image'])) { ?>
									<a target="_blank" href="<?php echo base_url('files/document/' . $value['file']) ?>">
										<img src="<?php echo base_url('lib/images/document/' . $value['image']) ?>" alt="<?php echo $value['name'] ?>">
									</a>

								<?php } else { ?>
									<a target="_blank" href="<?php echo base_url('files/document/' . $value['file']) ?>">
										<img src="<?php echo base_url('lib/images/document/default.jpg') ?>" alt="<?php echo $value['name'] ?>">
									</a>
								<?php } ?>

								<div class="media-body">
									<a target="_blank" href="<?php echo base_url('files/document/' . $value['file']) ?>">
										<h5><?php echo $value['name'] ?></h5>
									</a> 
									[<?php echo filesize_formatted(FCPATH . 'files/document/' . $value['file']) ?>]
									<?php echo format_date($value["publish_date"]) ?><br>
									Author: <?php echo $value['author'] ?><br>
									Publisher: <?php echo $value['publisher'] ?>
								</div>
							</div>
						<?php } ?>
					</div>

					<nav>
						<?php echo $pagination ?>
					</nav>
				</div>
			</div>

		</div>
	</div>
</div>