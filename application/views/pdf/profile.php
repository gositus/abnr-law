<html>
<head>
    <style>
        .content {
            margin-top: 1rem;
            font-size: 8pt;
            line-height: 1.6;
            color: #231f20;
        }

        .content p {
            margin-top: 0;
        }

        .left {
            font-size: 8pt;
            float: left;
            width: 28%;
            text-align: right;
        }

        .right {
            font-size: 8pt;
            float: right;
            width: 69%;
        }

        .content em {
            font-size: 8pt;
        }
    </style>
</head>
<body>


<div style="text-align: right">
    <img style="display:block;width: 70%;" src="<?php echo base_url('lib/images/profile/') . $profile['image'] ?>" alt="<?php echo $profile['name'] ?>">
    <div class="profile-name" style="display: block;width: 100%;text-align: left">
        <div style="padding-left:31%;margin-top: 3px ">
            <h3 style="margin: 0;color: #a78563;font-size: 10pt;margin-bottom: 5px"><?php echo $profile['name'] ?></h3>
            <?php if ($profile['id'] != 58) {
                $position=$this->lang->line(type_profile($profile['type']));
                 } else {
                $position='Assistant lawyer';
            } ?>
            <span style="color: #231f20;font-size: 8pt;text-transform: capitalize "><?php echo $position ?></span>
        </div>
        <div style="margin-top: 4rem; gap: 15px">
            <div style="margin-bottom: 2rem;">
                <div style="width: 28%; float: left;">
                    <table style="margin-left: auto;border-collapse: collapse; position: relative;">
                        <tr>
                            <td style="width: 23px;height: 22px;border-top:0px solid white;border-bottom:1px solid white; border-right:3px solid white;background-color: #6c889f; padding-right: 5px"></td>
                            <td style="width: 23px;height: 22px;border-top:0px solid white;border-bottom:1px solid white; border-right:3px solid white;background-color: #5d6871; padding-right: 5px"></td>
                            <td style="width: 23px;height: 22px;border-top:0px solid white;border-bottom:1px solid white; border-right:3px solid white;background-color: #a78a61; padding-right: 5px"></td>
                            <td style="width: 23px;height: 22px;border-top:0px solid white;border-bottom:1px solid white;background-color: #a2a574; padding-right: 5px"></td>
                        </tr>
                    </table>
                </div>

                <div style="display: block; float: right; width: 69%;height: 20px;background-color: #6c889f;"></div>
            </div>
            <div class="content">
                <div class="left">
                    <h6 style="color: #a78563;font-weight: bold;margin:0;font-size: 8pt">Background</h6>
                </div>
                <div class="right">
                    <?php echo $profile['content'] ?>
                </div>
            </div>
            <?php if(strlen(strip_tags($profile['content']))<2500){ ?>
            <pagebreak/>
            <?php } ?>
            <?php if (!empty($profile['professional_membership'])) { ?>
                <div class="content">
                    <div class="left">
                        <h6 style="color: #a78563;font-weight: bold;margin:0;font-size: 8pt">Professional Membership</h6>
                    </div>
                    <div class="right">
                        <?php echo nl2br($profile['professional_membership']) ?>
                    </div>
                </div>
            <?php } ?>
            <?php if (!empty($profile['citizenship'])) { ?>
                <div class="content">
                    <div class="left">
                        <h6 style="color: #a78563;font-weight: bold;margin:0;font-size: 8pt">Citizenship</h6>
                    </div>
                    <div class="right">
                        <?php echo nl2br($profile['citizenship']) ?>
                    </div>
                </div>
            <?php } ?>
            <?php if (!empty($profile['language'])) { ?>
                <div class="content">
                    <div class="left">
                        <h6 style="color: #a78563;font-weight: bold;margin:0;font-size: 8pt">Language</h6>
                    </div>
                    <div class="right">
                        <?php echo nl2br($profile['language']) ?>
                    </div>
                </div>
            <?php } ?>
            <?php if (!empty($profile['email'])) { ?>
                <div class="content">
                    <div class="left">
                        <h6 style="color: #a78563;font-weight: bold;margin:0;font-size: 8pt">Email</h6>
                    </div>
                    <div class="right">
                        <a href="mailto:<?php echo $profile['email'] ?>"><?php echo nl2br($profile['email']) ?></a>
                    </div>
                </div>

            <?php } ?>
            <?php if (!empty($profile['phone'])) { ?>
                <div class="content">
                    <div class="left">
                        <h6 style="color: #a78563;font-weight: bold;margin:0;font-size: 8pt">Phone</h6>
                    </div>
                    <div class="right">
                        <?php echo nl2br($profile['phone']) ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>

</body>
</html>
