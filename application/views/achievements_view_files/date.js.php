/*
 
  function diffDateInDay(date1, date2);
  // return date1 - date2 in day
  
  function dateToStr(date1, fmt);
  // convert date to string according to format
  // date format: 
  // - Y for year
  // - m for month, leading by zero
  // - n for month
  // - d for date, leading by zero
  // - j for date
  // - h for hour, leading by zero
  // - i for minute, leading by zero
  // - s for minute, leading by zero

  function isDate(str, separator);
  // return true if str in date format, otherwise false. 
  // Date Format: DD-MM-YYYY HH:MM:SS

  function isDateLet(str1, str2, separator);
  // return true if str1 older or equals than str2, otherwise false
  // Date Format: DD-MM-YYYY HH:MM:SS

  function isDateLt(str1, str2, separator);
  // return true if str1 older than str2, otherwise false
  // Date Format: DD-MM-YYYY HH:MM:SS

  function isDateGet(str1, str2, separator);
  // return true if str1 younger or equals than str2, otherwise false
  // Date Format: DD-MM-YYYY HH:MM:SS
      
  function isDateGt(str1, str2, separator);
  // return true if str1 younger than str2, otherwise false
  // Date Format: DD-MM-YYYY HH:MM:SS
  
  function isDateInOneWeek(date1, date2);
  // return true if date1 and date2 in one week
  
  function isFuture(str);
  // return true if str is date younger than now, otherwise false
  // Date Format: DD-MM-YYYY HH:MM:SS

  function strToDate(str); 
  // convert string to date, Nan if str no data format.
  // Date Format: DD/MM/YYYY HH:MM:SS
    
*/

	function diffDateInDay(date1, date2) {
			var diff_day = (date1.getTime() - date2.getTime())/(1000*3600*24);
			return diff_day;		
	}

  function dateToStr(date1, fmt) {
  	  var str = '';
      for (var i=0; i < fmt.length; i++) {
          var char1 = fmt.substring(i, i+1);
          
          if (char1 == 'Y') {
              str += date1.getYear();
          }
          else
          if (char1 == 'm') {
              str += leadingZero(date1.getMonth()+1);
          }
          else
          if (char1 == 'n') {
              str += (date1.getMonth()+1);
          }
          else          
          if (char1 == 'd') {
              str += leadingZero(date1.getDate());
          }
          else
          if (char1 == 'j') {
              str += date1.getDate();
          }
          else          
          if (char1 == 'h') {
              str += leadingZero(date1.getHours());
          }
          else
          if (char1 == 'i') {
              str += leadingZero(date1.getMinutes());
          }          
          else
          if (char1 == 's') {
              str += leadingZero(date1.getSeconds());
          }          
          else {
              str += char1;
          }
      }
      
      return str;
  }

  function isDate(str, separator) {
      return (! isNaN(strToDate(str, separator)));
  }

  function isDateLet(str1, str2, separator) {
      var date1 = strToDate(str1, separator);
      if (isNaN(date1)) return false;
      
      var date2 = strToDate(str2, separator);
      if (isNaN(date2)) return false;
      
      return date1 <= date2;
  }

  function isDateLt(str1, str2, separator) {
      var date1 = strToDate(str1, separator);
      if (isNaN(date1)) return false;
      
      var date2 = strToDate(str2, separator);
      if (isNaN(date2)) return false;
      
      return date1 < date2;
  }

  function isDateGet(str1, str2, separator) {
      var date1 = strToDate(str1, separator);
      if (isNaN(date1)) return false;
      
      var date2 = strToDate(str2, separator);
      if (isNaN(date2)) return false;
      
      return date1 >= date2;
  }
    
  function isDateGt(str1, str2, separator) {
      var date1 = strToDate(str1, separator);
      if (isNaN(date1)) return false;
      
      var date2 = strToDate(str2, separator);
      if (isNaN(date2)) return false;
      
      return date1 > date2;
  }

	function isDateInOneWeek(date1, date2) {
			var day = date1.getDay(date1);
			var diff = 0;
			
			if (day > 0) 
				diff = day - 1;
			else
				diff = 6;
					
					
			// get date of sunday in one week with date1
			var myDate = date1.getDate();
			var myMonth = date1.getMonth();
			var myYear = date1.getYear();
					
			var start = new Date(myYear, myMonth, myDate-diff, 0, 0, 0, 0);			
			for (var i=0; i < 7; i++) {
					myDate = start.getDate();
					myMonth = start.getMonth();
					myYear = start.getYear();
					
					if ((myDate == date2.getDate()) && (myMonth == date2.getMonth()) & (myYear == date2.getYear())) {
							return true;
					}
					
					start = new Date(myYear, myMonth, myDate+1, 0, 0, 0, 0);
			}
			
			return false;
	}

  function isFuture(str, separator) {
      var date1 = strToDate(str, separator);
      if (isNaN(date1)) return false;
      
      var date2 = new Date();
      return (date1 >= date2);
  }
  
  function strToDate(str, separator) {
  		re = /[^0-9]/
  	
  	  if (str == '') return NaN;
  	  if (! separator) separator = '/';
  	
      var dates = str.split(' ', 2); // get date and time
      if (dates.length < 1) return NaN;

  	  var myDate;
  	  if (dates.length >= 1) {
  	      myDate = dates[0].split(separator);
  	      if (myDate.length < 3) return NaN;  	      
  	      if (myDate.length > 3) return NaN;
  	      if (dates.length == 1) {
  	      			if (re.test(myDate[0])) return NaN;
  	      			if (re.test(myDate[1])) return NaN;
  	      			if (re.test(myDate[2])) return NaN;
  	      	
  	      	    var retDate = new Date(myDate[2], myDate[1]-1, myDate[0]);  	        	      	      	      	    
  	      	    
  	      	    re = /^0/
  	      	    var day = myDate[0].replace(re, '');
  	      	    var month = myDate[1].replace(re, '');
  	      	    var year = myDate[2].replace(re, '');
  	      	    
  	      	    if (parseInt(retDate.getDate()) != parseInt(day)) return NaN; // date bigger than current  	      	    
								if (parseInt(retDate.getMonth()+1) != parseInt(month)) return NaN; // date bigger than current  	      	    
								if (parseInt(retDate.getFullYear()) != parseInt(year)) return NaN; // date bigger than current  	      	    
				
  	      	    return retDate;
  	      }
  	       	      
  	      myTime = dates[1].split(':');  	           
  	      if (myTime.length < 4) return NaN;
  	      if (myTime.length > 4) return NaN;
  	      if (myTime.length == 4) {
  	      			if (re.test(myDate[0])) return NaN;
  	      			if (re.test(myDate[1])) return NaN;
  	      			if (re.test(myDate[2])) return NaN;
  	      			if (re.test(myTime[0])) return NaN;
  	      			if (re.test(myTime[1])) return NaN;
  	      			if (re.test(myTime[2])) return NaN;
  	      	
  	      	  	var retDate = new Date(myDate[2], myDate[1]-1, myDate[0], myTime[0], myTime[1], myTime[2]);  	      	
  	      	
  	      	  	re = /^0/
  	      	  	var day = myDate[0].replace(re, '');
			  				var month = myDate[1].replace(re, '');
		 	  				var year = myDate[2].replace(re, '');
		 	  				var hour = (myTime[0].length > 1)?myTime[0].replace(re, ''):myTime[0]; 	  
		 	  				var mins = (myTime[1].length > 1)?myTime[1].replace(re, ''):myTime[1];
		 	  				var secs = (myTime[2].length > 1)?myTime[2].replace(re, ''):myTime[2];
								
			  				if (parseInt(retDate.getDate()) != parseInt(day)) return NaN; // date bigger than current  	      	    			  
			  				if (parseInt(retDate.getMonth()+1) != parseInt(month)) return NaN; // date bigger than current  	      	    			  						
			  				if (parseInt(retDate.getFullYear()) != parseInt(year)) return NaN; // date bigger than current  	      	    
			  				if (parseInt(retDate.getHours()) != parseInt(hour)) return NaN; // date bigger than current  	      	    
			  				if (parseInt(retDate.getMinutes()) != parseInt(mins)) return NaN; // date bigger than current  	      	    
			  				if (parseInt(retDate.getSeconds()) != parseInt(secs)) return NaN; // date bigger than current  	      	    
	  	  
  	          	return retDate;
  	      	}
  	          
  	      	return new Date(myDate[2], myDate[1]-1, myDate[0], myTime[0], myTime[1], myTime[2], myTime[3]);
  	      
  	  	}
  	  
  	  	return NaN;
  }
  