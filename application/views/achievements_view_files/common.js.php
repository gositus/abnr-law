function number_format(str) {
	var result = "";		
	var thousand = 0;
	while (str.length > 0) {
		result = str.substr(str.length-1, 1) + result;
		str = str.substr(0, str.length-1);
		thousand++;
		if ((thousand == 3) && (str.length > 0)) {
			result = '.' + result;
			thousand = 0;
		}
	}
	
	return result;
}	

function trim(str) {
  re = /(^\s*)|(\s*$)/gi
  return str.replace(re, '');

}



function isAlphaNumeric(str) {
		if (! str) return '';
    re = /[^a-zA-Z0-9_\-]/g
    return ! re.test(str);
}

function isNumeric(str) {
    re = /[^0-9\.\,]/g
    return ! re.test(str);

}


function isEmail(str) {
	if (! str) return '';
	re = /(^[\w\d]+(\.[\d\w\-\_]+)*@[\w\d]+(\.[\d\w]+)+$)/
	return re.test(str);
}
	
function formrule(field) {
	if (field.defaultValue == field.value) field.value = "";
}

function formrule2(field) {
	if (field.value == "") {
		field.value = field.defaultValue;
	}
}

function search_onkeypress(e) {
	var keynum;
	var keychar;
	var numcheck;

	if(window.event) // IE
	{
  		keynum = e.keyCode;
  	}
	else 
	if(e.which) // Netscape/Firefox/Opera
  	{
  		keynum = e.which;
  	}
	
	if (keynum == 13) {
		search();
	}
}		

function search() {			
	if (document.frm_search.input_search.value.length > 0) {
		document.frm_search.submit();
	}
}