<div class="banner">
    <div class="container">
        <div class="row align-items-end no-gutters achievements-index caption-banner">
            <div class="col-12 col-sm-12 col-md-12 col-lg-8">
                <img class="w-100 banner-left" src="<?php echo base_url('lib/images/page/' . $page['image']) ?>" alt="<?php echo $page['name'] ?>">
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-4">
                <!-- <img class="w-100 banner-right" src="<?php echo base_url('lib/images/page/') . $page['image_right'] ?>" alt="<?php echo $page['name'] ?>"> -->
                <div class="banner-right">
                    <h1>Our</br> Achievements</h1>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="achievements-index">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="achievements-lists">
                    <?php foreach ($achievements as $value) { ?>
                        <?php if (!empty($value['achievement'])) { ?>
                            <span class="date"><?php echo $value['year'] ?></span>
                            <ul>
                                <?php foreach ($value['achievement'] as $val) { ?>
                                    <li>
                                        <a href="javascript:void(0)" class="open-modal-achievement" data-img="<?php echo base_url('lib/images/achievement/' . $val['image']) ?>" data-title="<?php echo $val['name'] ?>" data-year="<?php echo $value['year'] ?>" data-content="<?php echo htmlentities($val['content']) ?>">
                                            <img src="<?php echo base_url('lib/images/achievement/' . $val['image']) ?>" alt="<?php echo $val['name'] ?>">
                                        </a>
                                    </li>
                                <?php } ?>
                            </ul>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="achievements" tabindex="-1" aria-labelledby="achievementsLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="fal fa-times fa-fw"></i></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="d-flex h-100">
                            <img class="modal-img" src="">
                        </div>
                    </div>
                    <div class="col-md-8">
                        <h1 class="text-uppercase modal-title"></h1>
                        <strong class="date"></strong>
                        <div class="modal-desc">

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('.open-modal-achievement').on('click', function () {
            var title = $(this).data('title');
            var year = $(this).data('year');
            var img = $(this).data('img');
            var content = $(this).data('content');

            $('#achievements .modal-title').html(title);
            $('#achievements .date').html(year);
            $('#achievements .modal-img').attr('src', img);
            $('#achievements .modal-desc').html(content);

            $('#achievements').modal({
                show: true
            });
        })
    });
</script>