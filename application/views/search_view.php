<div class="banner">
    <div class="container">
        <div class="row align-items-end no-gutters caption-banner search">
            <div class="col-12 col-sm-12 col-md-12 col-lg-8">
                <img class="w-100 banner-left profile-index" src="<?php echo base_url('lib/images/page/' . $page['image']) ?>" alt="<?php echo $page['name'] ?>">
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-4">
                <!-- <img class="w-100 banner-right" src="<?php echo base_url('lib/images/page/' . $page['image_right']) ?>" alt="<?php echo $page['name'] ?>"> -->
                <div class="banner-right">
                    <h1><?php echo $page['heading'] ?></h1>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="mid-content py-4">
    <div class="container-lg">
        <div class="row">

            <div class="col-12 col-sm-3 col-md-4 col-lg-4 text-sm-right pr-sm-0">
                <div class="description pr-0 pr-sm-3">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <h1 class="page-title"><?php echo ucwords($this->lang->line('search_result')) ?></h1>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-12 col-sm-9 col-md-8">
                <?php if($total_news+$total_profile+$total_achievements+$total_document+$total_practice==0){ ?>
                        <div class="side-right text-center m-2">
                            <span>Keyword <?php echo $search_text ?> Not Found</span>

                        </div>
                <?php }else{ ?>
                <?php if (!empty($practice)) { ?>
                    <div class="middle-content">
                        <h3><?php echo ucfirst($this->lang->line('practice_area')) ?></h3>
                    </div>
                    <div class="description mt-3 mb-5">
                        <div class="side-right">
                            <?php foreach ($practice as $value) { ?>
                                <div class="news-item">
                                    <a href="<?php echo base_url('practice/' . $value['seo_url']) ?>">
                                        <h4><b><?php echo $value['name'] ?></b></h4>
                                    </a>

                                    <?php echo character_limiter(strip_tags($value['content']), 400) ?>
                                </div>
                            <?php } ?>
                            <div class="content-append">

                            </div>
                            <div class="loading-search" style="display: none">
                                <i class="ml-1 fal fa-spinner fa-spin"></i>
                            </div>
                        </div>
                        <?php if ($total_practice > count($practice)) { ?>
                            <a class="mb-3 see-more-search" data-page="practice" href="javascript:;"><b>SEE MORE >></b></a>
                        <?php } ?>
                    </div>

                <?php } ?>
                <?php if (!empty($profile)) { ?>
                    <div class="middle-content">
                        <h3><?php echo ucfirst($this->lang->line('profiles')) ?></h3>
                    </div>
                    <div class="description mt-3 mb-5">
                        <div class="side-right">
                            <?php foreach ($profile as $value) { ?>
                                <div class="news-item">
                                    <div class="with-img">
                                        <a href="<?php echo base_url('profiles/' . $value['seo_url']) ?>">
                                            <img class="w-100" src="<?php echo (!empty($value['profile_image'])) ? base_url('lib/images/profile/' . $value['profile_image']) : base_url('lib/images/page/profiles_placeholder.png') ?>" alt="<?php echo($value['name']) ?>">
                                        </a>
                                        <div class="content">
                                            <a href="<?php echo base_url('profiles/' . $value['seo_url']) ?>">
                                                <h4><b><?php echo $value['name'] ?></b></h4>
                                            </a>
                                            <?php echo character_limiter(strip_tags($value['content']), 400) ?>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="content-append">

                            </div>
                            <div class="loading-search" style="display: none">
                                <i class="ml-1 fal fa-spinner fa-spin"></i>
                            </div>
                        </div>
                        <?php if ($total_profile > count($profile)) { ?>
                            <a class="mb-3 see-more-search" data-page="profile" href="javascript:;"><b>SEE MORE >></b></a>
                        <?php } ?>
                    </div>

                <?php } ?>
                <?php if (!empty($achievements)) { ?>
                    <div class="middle-content">
                        <h3><?php echo ucfirst($this->lang->line('achievements')) ?></h3>
                    </div>
                    <div class="description mt-3 mb-5">
                        <div class="side-right">
                            <?php foreach ($achievements as $value) { ?>
                                <div class="news-item">
                                    <a href="<?php echo (!empty($value['url'])) ? $value['url'] : 'javascript:;' ?>">
                                        <h4><b><?php echo $value['name'] ?></b></h4>
                                    </a>
                                    <?php echo character_limiter(strip_tags($value['content']), 400) ?>
                                </div>
                            <?php } ?>
                            <div class="content-append">

                            </div>
                            <div class="loading-search" style="display: none">
                                <i class="ml-1 fal fa-spinner fa-spin"></i>
                            </div>
                        </div>
                        <?php if ($total_achievements > count($achievements)) { ?>
                            <a class="mb-3 see-more-search" data-page="achievement" href="javascript:;"><b>SEE MORE >></b></a>
                        <?php } ?>
                    </div>

                <?php } ?>

                <?php if (!empty($news)) { ?>
                    <div class="middle-content">
                        <h3><?php echo ucfirst($this->lang->line('news')) ?></h3>
                    </div>
                    <div class="description mt-3 mb-5">
                        <div class="side-right">
                            <?php foreach ($news as $value) { ?>
                                <div class="news-item">
                                    <a href="<?php echo base_url('news/' . $value['seo_url']) ?>">
                                        <h4><b><?php echo $value['name'] ?></b></h4>
                                    </a>
                                    <span><?php echo format_date($value['start']) ?></span>
                                    <?php echo character_limiter(strip_tags($value['content']), 400) ?>
                                </div>
                            <?php } ?>
                            <div class="content-append">

                            </div>
                            <div class="loading-search" style="display: none">
                                <i class="ml-1 fal fa-spinner fa-spin"></i>
                            </div>
                        </div>
                        <?php if ($total_news > count($news)) { ?>
                            <a class="mb-3 see-more-search" data-page="news" href="javascript:;"><b>SEE MORE >></b></a>
                        <?php } ?>
                    </div>

                <?php } ?>
                <?php if (!empty($document)) { ?>
                <div class="middle-content">
                    <h3><?php echo ucfirst($this->lang->line('publication')) ?></h3>
                </div>
                <div class="description mt-3 mb-5">
                    <div class="side-right">
                        <?php foreach ($document as $value) { ?>
                        <div class="news-item">
                            <div class="news-item">
                                <div class="with-img">
                                    <?php if (!empty($value['image'])) { ?>
                                        <a target="_blank" href="<?php echo base_url('files/document/' . $value['file']) ?>">
                                            <img src="<?php echo base_url('lib/images/document/' . $value['image']) ?>" alt="<?php echo $value['name'] ?>">
                                        </a>

                                    <?php } else { ?>
                                        <a target="_blank" href="<?php echo base_url('files/document/' . $value['file']) ?>">
                                            <img src="<?php echo base_url('lib/images/document/default.jpg') ?>" alt="<?php echo $value['name'] ?>">
                                        </a>
                                    <?php } ?>
                                    <div class="content">
                                        <a href="<?php echo base_url('files/document/' . $value['file']) ?>">
                                            <h4><b><?php echo $value['name'] ?></b></h4>
                                        </a>
                                        <?php echo format_date($value["publish_date"]) ?><br>
                                        <div class="end-content">
                                        Author: <?php echo $value['author'] ?><br>
                                        Publisher: <?php echo select_all_row('publisher', array('id' => $value['publisher']), true)['name'] ?>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <?php } ?>
                            <div class="content-append">

                            </div>
                            <div class="loading-search" style="display: none">
                                <i class="ml-1 fal fa-spinner fa-spin"></i>
                            </div>
                        </div>
                    </div>
                    <?php if ($total_document > count($document)) { ?>
                        <a class="mb-3 see-more-search" data-page="document" href="javascript:;"><b>SEE MORE >></b></a>
                    <?php } ?>
                    <?php } ?>
                    <?php if (empty($news) && empty($profile) && empty($achievement) && empty($document) && empty($practice)) { ?>
                        <div class="middle-content">
                            <div class="side-right text-center m-2">
                                <span><?php echo sprintf($this->lang->line('search_not_found'), $search_text); ?></span>
                            </div>
                        </div>
                    <?php } ?>
                <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        var page=[];
        page['news']=0;
        page['profile']=0;
        page['achievement']=0;
        page['document']=0;
        page['practice']=0;

        var page_count=[];
        page_count['news']="<?php echo ceil($total_news/5) ?>";
        page_count['profile']="<?php echo ceil($total_profile/5) ?>";
        page_count['achievement']="<?php echo ceil($total_achievements/5) ?>";
        page_count['document']="<?php echo ceil($total_document/5) ?>";
        page_count['practice']="<?php echo ceil($total_practice/5) ?>";


        $('.see-more-search').on('click',function () {
            var thisPage=$(this);
            var loading=$(this).parent().find('.loading-search');
            var content=$(this).parent().find('.content-append');

            var pageName = $(this).data('page');
            page[pageName]++;
            var html='';
            $.ajax({
                type: "POST",
                url: base_url + "search/get_data",
                data: {
                    page: pageName,
                    page_number:page[pageName],
                    serach_text:'<?php echo $search_text ?>',
                },
                dataType: "json",
                beforeSend: function () {
                    loading.show();

                },
                success: function (data) {
                    data.forEach(function(item) {
                        if(pageName=='profile') {
                            html = html + '<div class="news-item"><div class="with-img"><a href="' + item.seo_url + '"><img class="w-100" src="'+item.profile_image+'" alt="' + item.name + '"> </a><div class="content"><a href="' + item.seo_url + '"><h4><b>' + item.name + '</b></h4></a>' + item.content + '</div></div></div>'
                        }else if(pageName=='news'){
                            html = html + '<div class="news-item"><a href="' + item.seo_url + '"> <h4><b>' + item.name + '</b></h4></a><span>'+ item.start +'</span>' + item.content + '</div>';
                        }else if(pageName=='document'){
                            html = html + '<div class="news-item"><div class="with-img"><a href="' + item.seo_url + '"><img class="w-100" src="'+item.image+'" alt="' + item.name + '"> </a><div class="content"><a href="' + item.seo_url + '"> <h4><b>' + item.name + '</b></h4></a><span>'+ item.publish_date +'</span><div class="end-content">Author: '+item.author+'<br>Publisher: '+item.publisher+'</div></div></div>';
                        }else{
                            html = html + '<div class="news-item"><a href="' + item.seo_url + '"> <h4><b>' + item.name + '</b></h4></a>' + item.content + '</div>';
                        }
                    });
                    content.append(html);
                    loading.hide();
                    if(page_count[pageName]<(page[pageName]+2)){
                        thisPage.hide();
                    }
                }
            });
            return false;
        })
    });
</script>