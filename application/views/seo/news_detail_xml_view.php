<?php 
header('Content-Type: text/xml');
echo '<?xml version="1.0"?>';
?>
<oembed>
  <version>1.0</version>
  <provider_name><?php echo setting_value("site_name"); ?></provider_name>
  <provider_url><?php echo base_url(); ?></provider_url>
  <author_name>Gositus</author_name>
  <title><?php echo $news['name'];?></title>
  <type>rich</type>
  <width>800</width>
  <height>600</height>
  <html><?php echo htmlentities($news['content']);?></html>
  <thumbnail_url><?php echo base_url('lib/images/news/').$news['image'];?></thumbnail_url>
  <thumbnail_width>600</thumbnail_width>
  <thumbnail_height>400</thumbnail_height>
</oembed>