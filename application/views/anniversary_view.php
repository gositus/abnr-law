<div class="banner">
	<div class="container-lg">
		<div class="row align-items-end no-gutters caption-banner">
			<div class="col-md-8">
				<img class="w-100" src="<?php echo base_url('lib/images/banner/50th_left_banner_17.jpg') ?>" alt="">
				
			</div>
			<div class="col-md-4">
				<div class="pl-4 pr-5 py-4">
					<p class="mb-0 mainpage-title">
					50th Anniversary
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="mid-content py-4">
	<div class="container-lg">
		<div class="row" >
			<div class="col-3 col-sm-3 col-md-4 col-md-4 text-right">
				<div class="description pr-0 pr-sm-3">
					<ul class="list-columns lh-min nav" id="myTab" role="tablist">
						<li class="nav-item" role="presentation">
							<a class="active" id="tab1960s" data-toggle="tab" href="#tab_1960s" role="tab" aria-controls="tab_1960s" aria-selected="true">1960s</a>
						</li>
						<li class="nav-item" role="presentation">
							<a class="" id="tab1970s" data-toggle="tab" href="#tab_1970s" role="tab" aria-controls="tab_1970s" aria-selected="false">1970s</a>
						</li>
						<li class="nav-item" role="presentation">
							<a class="" id="tab1980s" data-toggle="tab" href="#tab_1980s" role="tab" aria-controls="tab_1980s" aria-selected="false">1980s</a>
						</li>
						<li class="nav-item" role="presentation">
							<a class="" id="tab1990s" data-toggle="tab" href="#tab_1990s" role="tab" aria-controls="tab_1990s" aria-selected="false">1990s</a>
						</li>
						<li class="nav-item" role="presentation">
							<a class="" id="tab2000s" data-toggle="tab" href="#tab_2000s" role="tab" aria-controls="tab_2000s" aria-selected="false">2000s</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="col-9 col-sm-9 col-md-8 col-lg-6">
				<div class="tab-content" id="nav-tabContent">
					<div class="tab-pane fade show active" id="tab_1960s" role="tabpanel" aria-labelledby="tab1960s">
						<div class="description">
							<h5 class="timeline-title mb-3">1960</h5>
						</div>
						<ul class="timeline">
							<li class="year">
								1967
							</li>
							<li>
								Indonesia opened selected economic sectors of the country to foreign investment through the 1967 Foreign Investment Law.
							</li>
							<li>
								On 1 May 1967, a law & economic consultant firm was founded under the name of "Ali Budiardjo, SH. MSc". A few months later, Mardjono Reksodiputro joined as a legal assistant and worked in a small room at Ali Budiardjo's private house at Jalan Proklamasi.
							</li>
							<li>
								Ali Budiardjo handled the first investment license (PT Freeport Indonesia) and contract documentation under the said law.
							</li>
							<li class="year">
								1969
							</li>
							<li>
								On 1 January 1969, the firm added Mardjono Reksodiputro & Hoediatmo Hoed as  partners  of  the  firm  and changed  its  name  to "Ali Budiardjo & Associates".
							</li>
						</ul>
					</div>
					<div class="tab-pane fade" id="tab_1970s" role="tabpanel" aria-labelledby="tab1970s">
						<div class="description">
							<h5 class="timeline-title mb-3">1970</h5>
						</div>
						<ul class="timeline">
							<li class="year">
								1970
							</li>
							<li>
								Hoediatmo Hoed resigned from the firm and Nugroho joined as one of the partners. From 1 June 1970, the firm is known as "Ali Budiardjo, Nugroho, Reksodiputro".
							</li>
							<li>
								Aside from the founding partners, other lawyers in the firm included Abimanyu, Sitorus, Netty Amaludin and Achmad Kartohadiprodjo. 
							</li>
						</ul>
					</div>
					<div class="tab-pane fade" id="tab_1980s" role="tabpanel" aria-labelledby="tab1980s">
						<div class="description">
							<h5 class="timeline-title mb-3">1980</h5>
						</div>
						<ul class="timeline">
							<li class="year">
								1980
							</li>
							<li>
								ABNR formed an association with a major international law firm based in The Netherlands. 
							</li>
							<li class="year">
								1981
							</li>
							<li>
								Gregory Churchill and Victor P.G. De Seriere joined the firm as foreign legal consultants. 
							</li>
							<li class="year">
								1984
							</li>
							<li>
								Pacific Rim Advisory Council (PRAC) was founded in 1984 as an international law firm association to examine legal and business issues in Asia and the broader Pacific Rim region. ABNR was among its founding firms.
							</li>
							<li class="year">
								1985
							</li>
							<li>
								18 years after its establishment, ABNR has added more lawyers and finally moved from a humble home office to a full tenth floor of Gedung Sewu (presently known as Wisma Argo Manunggal) at Jalan Gatot Subroto, Jakarta, Indonesia.
							</li>
						</ul>
					</div>
					<div class="tab-pane fade" id="tab_1990s" role="tabpanel" aria-labelledby="tab1990s">
						<div class="description">
							<h5 class="timeline-title mb-3">1990</h5>
						</div>
						<ul class="timeline">
							<li class="year">
								1992
							</li>
							<li>
								ABNR celebrated its 25th anniversary and organized a seminar on investment issues in Indonesia. 
							</li>
							<li class="year">
								1993
							</li>
							<li>
								The number of ABNR lawyers had grown tremendously and the firm decided to move to a brand new building Graha Niaga, Sudirman Central Business District area, Jakarta, Indonesia.
							</li>
							<li class="year">
								1996
							</li>
							<li>
								Indonesia issued its first Yankee bonds. ABNR was the selected counsel for the international underwriters.
							</li>
							<li class="year">
								1997
							</li>
							<li>
								Asian Financial Crisis began. A US$ 40 billion bailout package was organized by the International Monetary Fund (IMF) for Indonesia. In return, Indonesia closed 16 financially insolvent banks and promised other wide-ranging reforms. ABNR was the consultant to the IMF for the Netherlands-Indonesia Legal and Judicial Reform Program, including on bankruptcy reform.
							</li>
							<li class="year">
								1998
							</li>
							<li>
								ABNR provided further   assistance   to IMF in the establishment of the Commercial Court and the Anti-Corruption Court. Two of ABNR partners actively involved on working together with BAPPENAS and MA in perfecting the Commercial Court.
							</li>
							<li>
								ABNR established the ABNR Foundation, a non-profit institution, which provides scholarship funding and other activities supporting the education and development of the next generation of lawyers. 
							</li>
							<li class="year">
								1999
							</li>
							<li>
								ABNR opened its office in Singapore.
							</li>
							<li>
								ABNR formed an exclusive association with a major US-based international law firm.
							</li>
						</ul>
					</div>
					<div class="tab-pane fade" id="tab_2000s" role="tabpanel" aria-labelledby="tab2000s">
						<div class="description">
							<h5 class="timeline-title mb-3">2000</h5>
						</div>
						<ul class="timeline">
							<li class="year">
								2007
							</li>
							<li>
								ABNR celebrated its 40th anniversary by i) cocktail reception at IBA Conference in Singapore and ii) donation/fundraising program with Yayasan Nurani Dunia. 
							</li>
							<li class="year">
								2011
							</li>
							<li>
								ABNR received awards for outstanding pro bono works from Lex Mundi Pro Bono Foundation Awards. 
							</li>
							<li class="year">
								2013
							</li>
							<li>
								ABNR won the "Indonesia Law Firm of the Year" from IFLR1000 Asia Awards.
							</li>
							<li class="year">
								2015
							</li>
							<li>
								ALB Top 50 Largest Law Firms survey for 2015 acknowledged ABNR as the largest law firm in Indonesia, with 112 fee earners, including 15 partners.
							</li>
							<li class="year">
								2016 and 2017
							</li>
							<li>
								ABNR again won the "Indonesia Law Firm of the Year" from IFLR1000 Asia Awards for two consecutive years.
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-sm-4 col-md-4 col-lg-2">
				<a class="target_link ml-sm-auto" href="<?php echo base_url('lib/download/Booklet_ABNR_50_Years.pdf') ?>" target="_blank">
					<i class="fal fa-file"></i> <span>ANBR's 50th Anniversary</span>
				</a>
			</div>
			
		</div>
	</div>
</div>