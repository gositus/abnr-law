<div class="row">
    <form method="post" id="content-form" action="<?php echo base_url('goadmin/'.$this->url.'/view/'.$row['id'])?>" data-toggle="validator" enctype="multipart/form-data">
        <div class="col-md-12">
            <?php $this->load->view('admin/template/fixed_heading', array('type' => 'view', 'name' => $row['name'])); ?>
        </div>
        <input type="hidden" name="id" id="row_id" data-table-name="<?php echo $this->url;?>" value="<?php echo $row['id'] ?>">
        <?php if(validation_errors()){ ?>
            <!-- SHOW ERROR -->
            <?php echo viewErrorValidation(); ?>
            <!-- END SHOW ERROR -->
        <?php } ?>

        <div class="col-md-8 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="m-y-0 font-bold">Information</h4>
                </div>


                <div class="panel-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label for="ap_name" class="control-label col-md-3">Name</label>
                            <div class="col-md-7">
                                <input type="text" name="ap_name" id="ap_name" class="form-control required" required value="<?php echo $row['name']; ?>">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="ap_privilege" class="control-label col-md-3">Privilege</label>
                            <div class="col-md-7">
                                <div class="custom-controls-stacked">
                                    <label class="custom-control custom-control-primary custom-radio">
                                        <input type="radio" name="ap_privilege" class="privilege custom-control-input" value="1" <?php if($row['privilege']==1) echo 'checked' ; ?> >
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-label">Administrator</span>
                                    </label>

                                    <label class="custom-control custom-control-primary custom-radio">
                                        <input type="radio" name="ap_privilege" class="privilege custom-control-input" value="2"  <?php if($row['privilege']==2) echo 'checked' ; ?> >
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-label">Data Entry</span>
                                    </label>

                                    <label class="custom-control custom-control-primary custom-radio">
                                        <input type="radio" name="ap_privilege" class="privilege custom-control-input" value="3" <?php if($row['privilege']==3) echo 'checked' ; ?> >
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-label">Custom User</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        
                        <div id="access-table" >
                            <table id="list-table" data-name-table="<?php echo $url; ?>" class="table table-striped dataTable">
                                <thead align="center">
                                    <tr>
                                        <th width="52%">Module</th>
                                        <th width="12%">Read</th>
                                        <th width="12%">Add</th>
                                        <th width="12%">Modify</th>
                                        <th width="12%">Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $html = '';
                                    foreach ($parent_modules->result_array() as $parent) :
                                        // Get child modules.
                                        $child_modules = $this->db->order_by('name','asc')->get_where('module', array('parent' => $parent['id'], 'flag' => 1));
                                        
                                        if ($child_modules->num_rows() > 0) :
                                        $html .= '<tr class="module-parent" id="' . $parent['alias'] . '">';
                                        $html .= '<td colspan="5" align="center">' . $parent['name'] . '</td>';
                                        $html .= '</tr>';
                                        
                                            foreach ($child_modules->result_array() as $child) :

                                                $last_child_modules =  $this->db->order_by('name','asc')->get_where('module', array('parent' => $child['id'], 'flag' => 1));
                                                $module_list = json_decode($row['module'],true);
                                                // pre($module_list);
                                                // $module_list = explode(',', $row[0]['module_list']);
                                                // $access = explode(',', $row[0]['access']);
                                                // $array_key = array_search($child['module_id'], $module_list);
                                                
                                                // $value = ($access[$array_key]) ? $access[$array_key] : 0;
                                                
                                                
                                                if ($last_child_modules->num_rows() > 0)
                                                {
                                                    foreach ($last_child_modules->result_array() as $last) :
                                                        // pre($last_child_modules->result_array());
                                                        foreach ($module_list as $key => $modul) {
                                                             if ($key == $last['id']) {
                                                                $add = (($modul & 1) == 1) ? 'checked="checked"' : '';
                                                                $edit = (($modul & 2) == 2) ? 'checked="checked"' : '';
                                                                $delete = (($modul & 4) == 4) ? 'checked="checked"' : '';
                                                                $read = (($modul & 8) == 8) ? 'checked="checked"' : '';
                                                                
                                                            $html .= '<tr class="child-' . $child['alias'] . ' child-module" align="center">';
                                                            $html .= '<td>' . $last['name'] . '</td>';
                                                            $html .= '<td><input type="checkbox" value="8" class="access-read" ' . $read . ' name="' . $last['alias'] . '" /></td>';
                                                            $html .= '<td><input type="checkbox" value="1" class="access-add" ' . $add . ' name="' . $last['alias'] . '" /></td>';
                                                            $html .= '<td><input type="checkbox" value="2" class="access-modify" ' . $edit . ' name="' . $last['alias'] . '" /></td>';
                                                            $html .= '<td><input type="checkbox" value="4" class="access-delete" ' . $delete . ' name="' . $last['alias'] . '" /></td>';
                                                            $html .= '<td style="display:none;"><input id="total-' . $last['alias'] . '" type="hidden" name="' . $last['id'] . '" class="access_total" value="0" /></td>';
                                                            $html .= '</tr>';
                                                            }
                                                            
                                                        }
                                                        if (!in_array($last['id'], array_keys($module_list))) {
                                                            $html .= '<tr class="child-' . $last['alias'] . ' child-module" align="center">';
                                                                $html .= '<td>' . $last['name'] . '</td>';
                                                                $html .= '<td><input type="checkbox" value="8" class="access-read"  name="' . $last['alias'] . '" /></td>';
                                                                $html .= '<td><input type="checkbox" value="1" class="access-add"  name="' . $last['alias'] . '" /></td>';
                                                                $html .= '<td><input type="checkbox" value="2" class="access-modify"  name="' . $last['alias'] . '" /></td>';
                                                                $html .= '<td><input type="checkbox" value="4" class="access-delete"  name="' . $last['alias'] . '" /></td>';
                                                                $html .= '<td style="display:none;"><input id="total-' . $last['alias'] . '" type="hidden" name="' . $last['id'] . '" class="access_total" value="0" /></td>';
                                                                $html .= '</tr>';
                                                        }
                                                    endforeach;
                                                } else 
                                                {
                                                
                                                    // foreach ($last_child_modules->result_array() as $last) :
                                                        // pre($last_child_modules->result_array());
                                                        foreach ($module_list as $key => $modul) {
                                                            if ($key == $child['id']) {
                                                                $add = (($modul & 1) == 1) ? 'checked="checked"' : '';
                                                                $edit = (($modul & 2) == 2) ? 'checked="checked"' : '';
                                                                $delete = (($modul & 4) == 4) ? 'checked="checked"' : '';
                                                                $read = (($modul & 8) == 8) ? 'checked="checked"' : '';

                                                                $html .= '<tr class="child-' . $child['alias'] . ' child-module" align="center">';
                                                                $html .= '<td>' . $child['name'] . '</td>';
                                                                $html .= '<td><input type="checkbox" value="8" class="access-read" ' . $read . ' name="' . $child['alias'] . '" /></td>';
                                                                $html .= '<td><input type="checkbox" value="1" class="access-add" ' . $add . ' name="' . $child['alias'] . '" /></td>';
                                                                $html .= '<td><input type="checkbox" value="2" class="access-modify" ' . $edit . ' name="' . $child['alias'] . '" /></td>';
                                                                $html .= '<td><input type="checkbox" value="4" class="access-delete" ' . $delete . ' name="' . $child['alias'] . '" /></td>';
                                                                $html .= '<td style="display:none;"><input id="total-' . $child['alias'] . '" type="hidden" name="' . $child['id'] . '" class="access_total" value="0" /></td>';
                                                                $html .= '</tr>';
                                                                    // break;
                                                            }
                                                        }
                                                        if (!in_array($child['id'], array_keys($module_list))) {
                                                            $html .= '<tr class="child-' . $child['alias'] . ' child-module" align="center">';
                                                                $html .= '<td>' . $child['name'] . '</td>';
                                                                $html .= '<td><input type="checkbox" value="8" class="access-read"  name="' . $child['alias'] . '" /></td>';
                                                                $html .= '<td><input type="checkbox" value="1" class="access-add"  name="' . $child['alias'] . '" /></td>';
                                                                $html .= '<td><input type="checkbox" value="2" class="access-modify"  name="' . $child['alias'] . '" /></td>';
                                                                $html .= '<td><input type="checkbox" value="4" class="access-delete"  name="' . $child['alias'] . '" /></td>';
                                                                $html .= '<td style="display:none;"><input id="total-' . $child['alias'] . '" type="hidden" name="' . $child['id'] . '" class="access_total" value="0" /></td>';
                                                                $html .= '</tr>';
                                                        }
                                                    // endforeach;
                                                  
                                                } 
                                            endforeach;
                                        endif;
                                        
                                    endforeach;
                                    
                                    echo $html;
                                    ?>
                                    
                                </tbody>
                            </table>
                        </div>

                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="csrf-token">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-12 col-xs-12">
            <?php $this->load->view('admin/template/view_flag');?>
        </div>
    </form>
</div>
<?php if($row['privilege'] != 3) {?>
<script type="text/javascript">
	$(function(){
		$('#access-table').hide();
	});
</script>
<?php } ?>