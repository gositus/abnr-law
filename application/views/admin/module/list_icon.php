<div id="list-icon" class="modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span>
                        <i class="zmdi zmdi-close"></i>
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="3d-rotation" data-code="f101">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-3d-rotation zmdi-hc-fw"></i>
                            <span>3d-rotation</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="airplane-off" data-code="f102">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-airplane-off zmdi-hc-fw"></i>
                            <span>airplane-off</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="airplane" data-code="f103">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-airplane zmdi-hc-fw"></i>
                            <span>airplane</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="album" data-code="f104">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-album zmdi-hc-fw"></i>
                            <span>album</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="archive" data-code="f105">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-archive zmdi-hc-fw"></i>
                            <span>archive</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="assignment-account" data-code="f106">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-assignment-account zmdi-hc-fw"></i>
                            <span>assignment-account</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="assignment-alert" data-code="f107">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-assignment-alert zmdi-hc-fw"></i>
                            <span>assignment-alert</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="assignment-check" data-code="f108">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-assignment-check zmdi-hc-fw"></i>
                            <span>assignment-check</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="assignment-o" data-code="f109">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-assignment-o zmdi-hc-fw"></i>
                            <span>assignment-o</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="assignment-return" data-code="f10a">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-assignment-return zmdi-hc-fw"></i>
                            <span>assignment-return</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="assignment-returned" data-code="f10b">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-assignment-returned zmdi-hc-fw"></i>
                            <span>assignment-returned</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="assignment" data-code="f10c">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-assignment zmdi-hc-fw"></i>
                            <span>assignment</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="attachment-alt" data-code="f10d">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-attachment-alt zmdi-hc-fw"></i>
                            <span>attachment-alt</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="attachment" data-code="f10e">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-attachment zmdi-hc-fw"></i>
                            <span>attachment</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="audio" data-code="f10f">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-audio zmdi-hc-fw"></i>
                            <span>audio</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="badge-check" data-code="f110">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-badge-check zmdi-hc-fw"></i>
                            <span>badge-check</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="balance-wallet" data-code="f111">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-balance-wallet zmdi-hc-fw"></i>
                            <span>balance-wallet</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="balance" data-code="f112">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-balance zmdi-hc-fw"></i>
                            <span>balance</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="battery-alert" data-code="f113">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-battery-alert zmdi-hc-fw"></i>
                            <span>battery-alert</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="battery-flash" data-code="f114">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-battery-flash zmdi-hc-fw"></i>
                            <span>battery-flash</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="battery-unknown" data-code="f115">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-battery-unknown zmdi-hc-fw"></i>
                            <span>battery-unknown</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="battery" data-code="f116">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-battery zmdi-hc-fw"></i>
                            <span>battery</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="bike" data-code="f117">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-bike zmdi-hc-fw"></i>
                            <span>bike</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="block-alt" data-code="f118">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-block-alt zmdi-hc-fw"></i>
                            <span>block-alt</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="block" data-code="f119">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-block zmdi-hc-fw"></i>
                            <span>block</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="boat" data-code="f11a">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-boat zmdi-hc-fw"></i>
                            <span>boat</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="book-image" data-code="f11b">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-book-image zmdi-hc-fw"></i>
                            <span>book-image</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="book" data-code="f11c">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-book zmdi-hc-fw"></i>
                            <span>book</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="bookmark-outline" data-code="f11d">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-bookmark-outline zmdi-hc-fw"></i>
                            <span>bookmark-outline</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="bookmark" data-code="f11e">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-bookmark zmdi-hc-fw"></i>
                            <span>bookmark</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="brush" data-code="f11f">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-brush zmdi-hc-fw"></i>
                            <span>brush</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="bug" data-code="f120">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-bug zmdi-hc-fw"></i>
                            <span>bug</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="bus" data-code="f121">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-bus zmdi-hc-fw"></i>
                            <span>bus</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="cake" data-code="f122">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-cake zmdi-hc-fw"></i>
                            <span>cake</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="car-taxi" data-code="f123">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-car-taxi zmdi-hc-fw"></i>
                            <span>car-taxi</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="car-wash" data-code="f124">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-car-wash zmdi-hc-fw"></i>
                            <span>car-wash</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="car" data-code="f125">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-car zmdi-hc-fw"></i>
                            <span>car</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="card-giftcard" data-code="f126">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-card-giftcard zmdi-hc-fw"></i>
                            <span>card-giftcard</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="card-membership" data-code="f127">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-card-membership zmdi-hc-fw"></i>
                            <span>card-membership</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="card-travel" data-code="f128">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-card-travel zmdi-hc-fw"></i>
                            <span>card-travel</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="card" data-code="f129">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-card zmdi-hc-fw"></i>
                            <span>card</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="case-check" data-code="f12a">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-case-check zmdi-hc-fw"></i>
                            <span>case-check</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="case-download" data-code="f12b">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-case-download zmdi-hc-fw"></i>
                            <span>case-download</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="case-play" data-code="f12c">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-case-play zmdi-hc-fw"></i>
                            <span>case-play</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="case" data-code="f12d">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-case zmdi-hc-fw"></i>
                            <span>case</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="cast-connected" data-code="f12e">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-cast-connected zmdi-hc-fw"></i>
                            <span>cast-connected</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="cast" data-code="f12f">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-cast zmdi-hc-fw"></i>
                            <span>cast</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="chart-donut" data-code="f130">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-chart-donut zmdi-hc-fw"></i>
                            <span>chart-donut</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="chart" data-code="f131">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-chart zmdi-hc-fw"></i>
                            <span>chart</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="city-alt" data-code="f132">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-city-alt zmdi-hc-fw"></i>
                            <span>city-alt</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="city" data-code="f133">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-city zmdi-hc-fw"></i>
                            <span>city</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="close-circle-o" data-code="f134">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-close-circle-o zmdi-hc-fw"></i>
                            <span>close-circle-o</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="close-circle" data-code="f135">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-close-circle zmdi-hc-fw"></i>
                            <span>close-circle</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="close" data-code="f136">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-close zmdi-hc-fw"></i>
                            <span>close</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="cocktail" data-code="f137">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-cocktail zmdi-hc-fw"></i>
                            <span>cocktail</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="code-setting" data-code="f138">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-code-setting zmdi-hc-fw"></i>
                            <span>code-setting</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="code-smartphone" data-code="f139">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-code-smartphone zmdi-hc-fw"></i>
                            <span>code-smartphone</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="code" data-code="f13a">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-code zmdi-hc-fw"></i>
                            <span>code</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="coffee" data-code="f13b">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-coffee zmdi-hc-fw"></i>
                            <span>coffee</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="collection-bookmark" data-code="f13c">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-collection-bookmark zmdi-hc-fw"></i>
                            <span>collection-bookmark</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="collection-case-play" data-code="f13d">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-collection-case-play zmdi-hc-fw"></i>
                            <span>collection-case-play</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="collection-folder-image" data-code="f13e">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-collection-folder-image zmdi-hc-fw"></i>
                            <span>collection-folder-image</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="collection-image-o" data-code="f13f">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-collection-image-o zmdi-hc-fw"></i>
                            <span>collection-image-o</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="collection-image" data-code="f140">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-collection-image zmdi-hc-fw"></i>
                            <span>collection-image</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="collection-item-1" data-code="f141">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-collection-item-1 zmdi-hc-fw"></i>
                            <span>collection-item-1</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="collection-item-2" data-code="f142">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-collection-item-2 zmdi-hc-fw"></i>
                            <span>collection-item-2</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="collection-item-3" data-code="f143">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-collection-item-3 zmdi-hc-fw"></i>
                            <span>collection-item-3</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="collection-item-4" data-code="f144">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-collection-item-4 zmdi-hc-fw"></i>
                            <span>collection-item-4</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="collection-item-5" data-code="f145">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-collection-item-5 zmdi-hc-fw"></i>
                            <span>collection-item-5</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="collection-item-6" data-code="f146">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-collection-item-6 zmdi-hc-fw"></i>
                            <span>collection-item-6</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="collection-item-7" data-code="f147">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-collection-item-7 zmdi-hc-fw"></i>
                            <span>collection-item-7</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="collection-item-8" data-code="f148">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-collection-item-8 zmdi-hc-fw"></i>
                            <span>collection-item-8</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="collection-item-9-plus" data-code="f149">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-collection-item-9-plus zmdi-hc-fw"></i>
                            <span>collection-item-9-plus</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="collection-item-9" data-code="f14a">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-collection-item-9 zmdi-hc-fw"></i>
                            <span>collection-item-9</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="collection-item" data-code="f14b">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-collection-item zmdi-hc-fw"></i>
                            <span>collection-item</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="collection-music" data-code="f14c">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-collection-music zmdi-hc-fw"></i>
                            <span>collection-music</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="collection-pdf" data-code="f14d">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-collection-pdf zmdi-hc-fw"></i>
                            <span>collection-pdf</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="collection-plus" data-code="f14e">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-collection-plus zmdi-hc-fw"></i>
                            <span>collection-plus</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="collection-speaker" data-code="f14f">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-collection-speaker zmdi-hc-fw"></i>
                            <span>collection-speaker</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="collection-text" data-code="f150">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-collection-text zmdi-hc-fw"></i>
                            <span>collection-text</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="collection-video" data-code="f151">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-collection-video zmdi-hc-fw"></i>
                            <span>collection-video</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="compass" data-code="f152">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-compass zmdi-hc-fw"></i>
                            <span>compass</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="cutlery" data-code="f153">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-cutlery zmdi-hc-fw"></i>
                            <span>cutlery</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="delete" data-code="f154">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-delete zmdi-hc-fw"></i>
                            <span>delete</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="dialpad" data-code="f155">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-dialpad zmdi-hc-fw"></i>
                            <span>dialpad</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="dns" data-code="f156">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-dns zmdi-hc-fw"></i>
                            <span>dns</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="drink" data-code="f157">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-drink zmdi-hc-fw"></i>
                            <span>drink</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="edit" data-code="f158">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-edit zmdi-hc-fw"></i>
                            <span>edit</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="email-open" data-code="f159">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-email-open zmdi-hc-fw"></i>
                            <span>email-open</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="email" data-code="f15a">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-email zmdi-hc-fw"></i>
                            <span>email</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="eye-off" data-code="f15b">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-eye-off zmdi-hc-fw"></i>
                            <span>eye-off</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="eye" data-code="f15c">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-eye zmdi-hc-fw"></i>
                            <span>eye</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="eyedropper" data-code="f15d">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-eyedropper zmdi-hc-fw"></i>
                            <span>eyedropper</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="favorite-outline" data-code="f15e">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-favorite-outline zmdi-hc-fw"></i>
                            <span>favorite-outline</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="favorite" data-code="f15f">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-favorite zmdi-hc-fw"></i>
                            <span>favorite</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="filter-list" data-code="f160">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-filter-list zmdi-hc-fw"></i>
                            <span>filter-list</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="fire" data-code="f161">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-fire zmdi-hc-fw"></i>
                            <span>fire</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="flag" data-code="f162">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-flag zmdi-hc-fw"></i>
                            <span>flag</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="flare" data-code="f163">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-flare zmdi-hc-fw"></i>
                            <span>flare</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="flash-auto" data-code="f164">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-flash-auto zmdi-hc-fw"></i>
                            <span>flash-auto</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="flash-off" data-code="f165">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-flash-off zmdi-hc-fw"></i>
                            <span>flash-off</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="flash" data-code="f166">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-flash zmdi-hc-fw"></i>
                            <span>flash</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="flip" data-code="f167">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-flip zmdi-hc-fw"></i>
                            <span>flip</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="flower-alt" data-code="f168">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-flower-alt zmdi-hc-fw"></i>
                            <span>flower-alt</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="flower" data-code="f169">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-flower zmdi-hc-fw"></i>
                            <span>flower</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="font" data-code="f16a">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-font zmdi-hc-fw"></i>
                            <span>font</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="fullscreen-alt" data-code="f16b">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-fullscreen-alt zmdi-hc-fw"></i>
                            <span>fullscreen-alt</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="fullscreen-exit" data-code="f16c">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-fullscreen-exit zmdi-hc-fw"></i>
                            <span>fullscreen-exit</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="fullscreen" data-code="f16d">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-fullscreen zmdi-hc-fw"></i>
                            <span>fullscreen</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="functions" data-code="f16e">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-functions zmdi-hc-fw"></i>
                            <span>functions</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="gas-station" data-code="f16f">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-gas-station zmdi-hc-fw"></i>
                            <span>gas-station</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="gesture" data-code="f170">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-gesture zmdi-hc-fw"></i>
                            <span>gesture</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="globe-alt" data-code="f171">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-globe-alt zmdi-hc-fw"></i>
                            <span>globe-alt</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="globe-lock" data-code="f172">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-globe-lock zmdi-hc-fw"></i>
                            <span>globe-lock</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="globe" data-code="f173">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-globe zmdi-hc-fw"></i>
                            <span>globe</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="graduation-cap" data-code="f174">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-graduation-cap zmdi-hc-fw"></i>
                            <span>graduation-cap</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="group" data-code="f3e9">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-group zmdi-hc-fw"></i>
                            <span>group</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="home" data-code="f175">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-home zmdi-hc-fw"></i>
                            <span>home</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="hospital-alt" data-code="f176">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-hospital-alt zmdi-hc-fw"></i>
                            <span>hospital-alt</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="hospital" data-code="f177">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-hospital zmdi-hc-fw"></i>
                            <span>hospital</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="hotel" data-code="f178">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-hotel zmdi-hc-fw"></i>
                            <span>hotel</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="hourglass-alt" data-code="f179">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-hourglass-alt zmdi-hc-fw"></i>
                            <span>hourglass-alt</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="hourglass-outline" data-code="f17a">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-hourglass-outline zmdi-hc-fw"></i>
                            <span>hourglass-outline</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="hourglass" data-code="f17b">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-hourglass zmdi-hc-fw"></i>
                            <span>hourglass</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="http" data-code="f17c">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-http zmdi-hc-fw"></i>
                            <span>http</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="image-alt" data-code="f17d">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-image-alt zmdi-hc-fw"></i>
                            <span>image-alt</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="image-o" data-code="f17e">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-image-o zmdi-hc-fw"></i>
                            <span>image-o</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="image" data-code="f17f">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-image zmdi-hc-fw"></i>
                            <span>image</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="inbox" data-code="f180">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-inbox zmdi-hc-fw"></i>
                            <span>inbox</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="invert-colors-off" data-code="f181">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-invert-colors-off zmdi-hc-fw"></i>
                            <span>invert-colors-off</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="invert-colors" data-code="f182">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-invert-colors zmdi-hc-fw"></i>
                            <span>invert-colors</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="key" data-code="f183">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-key zmdi-hc-fw"></i>
                            <span>key</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="label-alt-outline" data-code="f184">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-label-alt-outline zmdi-hc-fw"></i>
                            <span>label-alt-outline</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="label-alt" data-code="f185">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-label-alt zmdi-hc-fw"></i>
                            <span>label-alt</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="label-heart" data-code="f186">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-label-heart zmdi-hc-fw"></i>
                            <span>label-heart</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="label" data-code="f187">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-label zmdi-hc-fw"></i>
                            <span>label</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="labels" data-code="f188">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-labels zmdi-hc-fw"></i>
                            <span>labels</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="lamp" data-code="f189">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-lamp zmdi-hc-fw"></i>
                            <span>lamp</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="landscape" data-code="f18a">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-landscape zmdi-hc-fw"></i>
                            <span>landscape</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="layers-off" data-code="f18b">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-layers-off zmdi-hc-fw"></i>
                            <span>layers-off</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="layers" data-code="f18c">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-layers zmdi-hc-fw"></i>
                            <span>layers</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="library" data-code="f18d">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-library zmdi-hc-fw"></i>
                            <span>library</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="link" data-code="f18e">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-link zmdi-hc-fw"></i>
                            <span>link</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="lock-open" data-code="f18f">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-lock-open zmdi-hc-fw"></i>
                            <span>lock-open</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="lock-outline" data-code="f190">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-lock-outline zmdi-hc-fw"></i>
                            <span>lock-outline</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="lock" data-code="f191">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-lock zmdi-hc-fw"></i>
                            <span>lock</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="mail-reply-all" data-code="f192">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-mail-reply-all zmdi-hc-fw"></i>
                            <span>mail-reply-all</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="mail-reply" data-code="f193">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-mail-reply zmdi-hc-fw"></i>
                            <span>mail-reply</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="mail-send" data-code="f194">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-mail-send zmdi-hc-fw"></i>
                            <span>mail-send</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="mall" data-code="f195">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-mall zmdi-hc-fw"></i>
                            <span>mall</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="map" data-code="f196">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-map zmdi-hc-fw"></i>
                            <span>map</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="menu" data-code="f197">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-menu zmdi-hc-fw"></i>
                            <span>menu</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="money-box" data-code="f198">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-money-box zmdi-hc-fw"></i>
                            <span>money-box</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="money-off" data-code="f199">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-money-off zmdi-hc-fw"></i>
                            <span>money-off</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="money" data-code="f19a">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-money zmdi-hc-fw"></i>
                            <span>money</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="more-vert" data-code="f19b">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-more-vert zmdi-hc-fw"></i>
                            <span>more-vert</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="more" data-code="f19c">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-more zmdi-hc-fw"></i>
                            <span>more</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="movie-alt" data-code="f19d">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-movie-alt zmdi-hc-fw"></i>
                            <span>movie-alt</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="movie" data-code="f19e">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-movie zmdi-hc-fw"></i>
                            <span>movie</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="nature-people" data-code="f19f">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-nature-people zmdi-hc-fw"></i>
                            <span>nature-people</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="nature" data-code="f1a0">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-nature zmdi-hc-fw"></i>
                            <span>nature</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="navigation" data-code="f1a1">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-navigation zmdi-hc-fw"></i>
                            <span>navigation</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="open-in-browser" data-code="f1a2">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-open-in-browser zmdi-hc-fw"></i>
                            <span>open-in-browser</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="open-in-new" data-code="f1a3">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-open-in-new zmdi-hc-fw"></i>
                            <span>open-in-new</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="palette" data-code="f1a4">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-palette zmdi-hc-fw"></i>
                            <span>palette</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="parking" data-code="f1a5">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-parking zmdi-hc-fw"></i>
                            <span>parking</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="pin-account" data-code="f1a6">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-pin-account zmdi-hc-fw"></i>
                            <span>pin-account</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="pin-assistant" data-code="f1a7">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-pin-assistant zmdi-hc-fw"></i>
                            <span>pin-assistant</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="pin-drop" data-code="f1a8">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-pin-drop zmdi-hc-fw"></i>
                            <span>pin-drop</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="pin-help" data-code="f1a9">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-pin-help zmdi-hc-fw"></i>
                            <span>pin-help</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="pin-off" data-code="f1aa">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-pin-off zmdi-hc-fw"></i>
                            <span>pin-off</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="pin" data-code="f1ab">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-pin zmdi-hc-fw"></i>
                            <span>pin</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="pizza" data-code="f1ac">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-pizza zmdi-hc-fw"></i>
                            <span>pizza</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="plaster" data-code="f1ad">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-plaster zmdi-hc-fw"></i>
                            <span>plaster</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="power-setting" data-code="f1ae">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-power-setting zmdi-hc-fw"></i>
                            <span>power-setting</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="power" data-code="f1af">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-power zmdi-hc-fw"></i>
                            <span>power</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="print" data-code="f1b0">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-print zmdi-hc-fw"></i>
                            <span>print</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="puzzle-piece" data-code="f1b1">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-puzzle-piece zmdi-hc-fw"></i>
                            <span>puzzle-piece</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="quote" data-code="f1b2">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-quote zmdi-hc-fw"></i>
                            <span>quote</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="railway" data-code="f1b3">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-railway zmdi-hc-fw"></i>
                            <span>railway</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="receipt" data-code="f1b4">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-receipt zmdi-hc-fw"></i>
                            <span>receipt</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="refresh-alt" data-code="f1b5">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-refresh-alt zmdi-hc-fw"></i>
                            <span>refresh-alt</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="refresh-sync-alert" data-code="f1b6">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-refresh-sync-alert zmdi-hc-fw"></i>
                            <span>refresh-sync-alert</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="refresh-sync-off" data-code="f1b7">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-refresh-sync-off zmdi-hc-fw"></i>
                            <span>refresh-sync-off</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="refresh-sync" data-code="f1b8">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-refresh-sync zmdi-hc-fw"></i>
                            <span>refresh-sync</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="refresh" data-code="f1b9">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-refresh zmdi-hc-fw"></i>
                            <span>refresh</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="roller" data-code="f1ba">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-roller zmdi-hc-fw"></i>
                            <span>roller</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="ruler" data-code="f1bb">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-ruler zmdi-hc-fw"></i>
                            <span>ruler</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="scissors" data-code="f1bc">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-scissors zmdi-hc-fw"></i>
                            <span>scissors</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="screen-rotation-lock" data-code="f1bd">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-screen-rotation-lock zmdi-hc-fw"></i>
                            <span>screen-rotation-lock</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="screen-rotation" data-code="f1be">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-screen-rotation zmdi-hc-fw"></i>
                            <span>screen-rotation</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="search-for" data-code="f1bf">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-search-for zmdi-hc-fw"></i>
                            <span>search-for</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="search-in-file" data-code="f1c0">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-search-in-file zmdi-hc-fw"></i>
                            <span>search-in-file</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="search-in-page" data-code="f1c1">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-search-in-page zmdi-hc-fw"></i>
                            <span>search-in-page</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="search-replace" data-code="f1c2">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-search-replace zmdi-hc-fw"></i>
                            <span>search-replace</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="search" data-code="f1c3">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-search zmdi-hc-fw"></i>
                            <span>search</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="seat" data-code="f1c4">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-seat zmdi-hc-fw"></i>
                            <span>seat</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="settings-square" data-code="f1c5">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-settings-square zmdi-hc-fw"></i>
                            <span>settings-square</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="settings" data-code="f1c6">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-settings zmdi-hc-fw"></i>
                            <span>settings</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="shape" data-code="f3eb">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-shape zmdi-hc-fw"></i>
                            <span>shape</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="shield-check" data-code="f1c7">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-shield-check zmdi-hc-fw"></i>
                            <span>shield-check</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="shield-security" data-code="f1c8">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-shield-security zmdi-hc-fw"></i>
                            <span>shield-security</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="shopping-basket" data-code="f1c9">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-shopping-basket zmdi-hc-fw"></i>
                            <span>shopping-basket</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="shopping-cart-plus" data-code="f1ca">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-shopping-cart-plus zmdi-hc-fw"></i>
                            <span>shopping-cart-plus</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="shopping-cart" data-code="f1cb">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-shopping-cart zmdi-hc-fw"></i>
                            <span>shopping-cart</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="sign-in" data-code="f1cc">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-sign-in zmdi-hc-fw"></i>
                            <span>sign-in</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="sort-amount-asc" data-code="f1cd">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-sort-amount-asc zmdi-hc-fw"></i>
                            <span>sort-amount-asc</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="sort-amount-desc" data-code="f1ce">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-sort-amount-desc zmdi-hc-fw"></i>
                            <span>sort-amount-desc</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="sort-asc" data-code="f1cf">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-sort-asc zmdi-hc-fw"></i>
                            <span>sort-asc</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="sort-desc" data-code="f1d0">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-sort-desc zmdi-hc-fw"></i>
                            <span>sort-desc</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="spellcheck" data-code="f1d1">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-spellcheck zmdi-hc-fw"></i>
                            <span>spellcheck</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="spinner" data-code="f3ec">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-spinner zmdi-hc-fw"></i>
                            <span>spinner</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="storage" data-code="f1d2">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-storage zmdi-hc-fw"></i>
                            <span>storage</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="store-24" data-code="f1d3">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-store-24 zmdi-hc-fw"></i>
                            <span>store-24</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="store" data-code="f1d4">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-store zmdi-hc-fw"></i>
                            <span>store</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="subway" data-code="f1d5">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-subway zmdi-hc-fw"></i>
                            <span>subway</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="sun" data-code="f1d6">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-sun zmdi-hc-fw"></i>
                            <span>sun</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="tab-unselected" data-code="f1d7">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-tab-unselected zmdi-hc-fw"></i>
                            <span>tab-unselected</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="tab" data-code="f1d8">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-tab zmdi-hc-fw"></i>
                            <span>tab</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="tag-close" data-code="f1d9">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-tag-close zmdi-hc-fw"></i>
                            <span>tag-close</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="tag-more" data-code="f1da">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-tag-more zmdi-hc-fw"></i>
                            <span>tag-more</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="tag" data-code="f1db">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-tag zmdi-hc-fw"></i>
                            <span>tag</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="thumb-down" data-code="f1dc">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-thumb-down zmdi-hc-fw"></i>
                            <span>thumb-down</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="thumb-up-down" data-code="f1dd">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-thumb-up-down zmdi-hc-fw"></i>
                            <span>thumb-up-down</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="thumb-up" data-code="f1de">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-thumb-up zmdi-hc-fw"></i>
                            <span>thumb-up</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="ticket-star" data-code="f1df">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-ticket-star zmdi-hc-fw"></i>
                            <span>ticket-star</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="toll" data-code="f1e0">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-toll zmdi-hc-fw"></i>
                            <span>toll</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="toys" data-code="f1e1">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-toys zmdi-hc-fw"></i>
                            <span>toys</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="traffic" data-code="f1e2">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-traffic zmdi-hc-fw"></i>
                            <span>traffic</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="translate" data-code="f1e3">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-translate zmdi-hc-fw"></i>
                            <span>translate</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="triangle-down" data-code="f1e4">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-triangle-down zmdi-hc-fw"></i>
                            <span>triangle-down</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="triangle-up" data-code="f1e5">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-triangle-up zmdi-hc-fw"></i>
                            <span>triangle-up</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="truck" data-code="f1e6">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-truck zmdi-hc-fw"></i>
                            <span>truck</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="turning-sign" data-code="f1e7">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-turning-sign zmdi-hc-fw"></i>
                            <span>turning-sign</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="ungroup" data-code="f3ed">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-ungroup zmdi-hc-fw"></i>
                            <span>ungroup</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="wallpaper" data-code="f1e8">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-wallpaper zmdi-hc-fw"></i>
                            <span>wallpaper</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="washing-machine" data-code="f1e9">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-washing-machine zmdi-hc-fw"></i>
                            <span>washing-machine</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="window-maximize" data-code="f1ea">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-window-maximize zmdi-hc-fw"></i>
                            <span>window-maximize</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="window-minimize" data-code="f1eb">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-window-minimize zmdi-hc-fw"></i>
                            <span>window-minimize</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="window-restore" data-code="f1ec">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-window-restore zmdi-hc-fw"></i>
                            <span>window-restore</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="wrench" data-code="f1ed">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-wrench zmdi-hc-fw"></i>
                            <span>wrench</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="zoom-in" data-code="f1ee">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-zoom-in zmdi-hc-fw"></i>
                            <span>zoom-in</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="zoom-out" data-code="f1ef">
                        <div class="demo-icon-block">
                            <i class="zmdi zmdi-zoom-out zmdi-hc-fw"></i>
                            <span>zoom-out</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="aspect-ratio-alt" data-code="f364">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-aspect-ratio-alt zmdi-hc-fw"></i>
                        <span>aspect-ratio-alt</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="aspect-ratio" data-code="f365">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-aspect-ratio zmdi-hc-fw"></i>
                        <span>aspect-ratio</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="blur-circular" data-code="f366">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-blur-circular zmdi-hc-fw"></i>
                        <span>blur-circular</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="blur-linear" data-code="f367">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-blur-linear zmdi-hc-fw"></i>
                        <span>blur-linear</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="blur-off" data-code="f368">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-blur-off zmdi-hc-fw"></i>
                        <span>blur-off</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="blur" data-code="f369">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-blur zmdi-hc-fw"></i>
                        <span>blur</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="brightness-2" data-code="f36a">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-brightness-2 zmdi-hc-fw"></i>
                        <span>brightness-2</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="brightness-3" data-code="f36b">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-brightness-3 zmdi-hc-fw"></i>
                        <span>brightness-3</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="brightness-4" data-code="f36c">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-brightness-4 zmdi-hc-fw"></i>
                        <span>brightness-4</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="brightness-5" data-code="f36d">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-brightness-5 zmdi-hc-fw"></i>
                        <span>brightness-5</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="brightness-6" data-code="f36e">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-brightness-6 zmdi-hc-fw"></i>
                        <span>brightness-6</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="brightness-7" data-code="f36f">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-brightness-7 zmdi-hc-fw"></i>
                        <span>brightness-7</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="brightness-auto" data-code="f370">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-brightness-auto zmdi-hc-fw"></i>
                        <span>brightness-auto</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="brightness-setting" data-code="f371">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-brightness-setting zmdi-hc-fw"></i>
                        <span>brightness-setting</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="broken-image" data-code="f372">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-broken-image zmdi-hc-fw"></i>
                        <span>broken-image</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="center-focus-strong" data-code="f373">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-center-focus-strong zmdi-hc-fw"></i>
                        <span>center-focus-strong</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="center-focus-weak" data-code="f374">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-center-focus-weak zmdi-hc-fw"></i>
                        <span>center-focus-weak</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="compare" data-code="f375">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-compare zmdi-hc-fw"></i>
                        <span>compare</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="crop-16-9" data-code="f376">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-crop-16-9 zmdi-hc-fw"></i>
                        <span>crop-16-9</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="crop-3-2" data-code="f377">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-crop-3-2 zmdi-hc-fw"></i>
                        <span>crop-3-2</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="crop-5-4" data-code="f378">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-crop-5-4 zmdi-hc-fw"></i>
                        <span>crop-5-4</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="crop-7-5" data-code="f379">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-crop-7-5 zmdi-hc-fw"></i>
                        <span>crop-7-5</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="crop-din" data-code="f37a">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-crop-din zmdi-hc-fw"></i>
                        <span>crop-din</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="crop-free" data-code="f37b">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-crop-free zmdi-hc-fw"></i>
                        <span>crop-free</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="crop-landscape" data-code="f37c">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-crop-landscape zmdi-hc-fw"></i>
                        <span>crop-landscape</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="crop-portrait" data-code="f37d">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-crop-portrait zmdi-hc-fw"></i>
                        <span>crop-portrait</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="crop-square" data-code="f37e">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-crop-square zmdi-hc-fw"></i>
                        <span>crop-square</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="exposure-alt" data-code="f37f">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-exposure-alt zmdi-hc-fw"></i>
                        <span>exposure-alt</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="exposure" data-code="f380">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-exposure zmdi-hc-fw"></i>
                        <span>exposure</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="filter-b-and-w" data-code="f381">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-filter-b-and-w zmdi-hc-fw"></i>
                        <span>filter-b-and-w</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="filter-center-focus" data-code="f382">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-filter-center-focus zmdi-hc-fw"></i>
                        <span>filter-center-focus</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="filter-frames" data-code="f383">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-filter-frames zmdi-hc-fw"></i>
                        <span>filter-frames</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="filter-tilt-shift" data-code="f384">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-filter-tilt-shift zmdi-hc-fw"></i>
                        <span>filter-tilt-shift</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="gradient" data-code="f385">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-gradient zmdi-hc-fw"></i>
                        <span>gradient</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="grain" data-code="f386">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-grain zmdi-hc-fw"></i>
                        <span>grain</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="graphic-eq" data-code="f387">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-graphic-eq zmdi-hc-fw"></i>
                        <span>graphic-eq</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="hdr-off" data-code="f388">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-hdr-off zmdi-hc-fw"></i>
                        <span>hdr-off</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="hdr-strong" data-code="f389">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-hdr-strong zmdi-hc-fw"></i>
                        <span>hdr-strong</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="hdr-weak" data-code="f38a">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-hdr-weak zmdi-hc-fw"></i>
                        <span>hdr-weak</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="hdr" data-code="f38b">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-hdr zmdi-hc-fw"></i>
                        <span>hdr</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="iridescent" data-code="f38c">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-iridescent zmdi-hc-fw"></i>
                        <span>iridescent</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="leak-off" data-code="f38d">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-leak-off zmdi-hc-fw"></i>
                        <span>leak-off</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="leak" data-code="f38e">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-leak zmdi-hc-fw"></i>
                        <span>leak</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="looks" data-code="f38f">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-looks zmdi-hc-fw"></i>
                        <span>looks</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="loupe" data-code="f390">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-loupe zmdi-hc-fw"></i>
                        <span>loupe</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="panorama-horizontal" data-code="f391">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-panorama-horizontal zmdi-hc-fw"></i>
                        <span>panorama-horizontal</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="panorama-vertical" data-code="f392">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-panorama-vertical zmdi-hc-fw"></i>
                        <span>panorama-vertical</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="panorama-wide-angle" data-code="f393">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-panorama-wide-angle zmdi-hc-fw"></i>
                        <span>panorama-wide-angle</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="photo-size-select-large" data-code="f394">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-photo-size-select-large zmdi-hc-fw"></i>
                        <span>photo-size-select-large</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="photo-size-select-small" data-code="f395">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-photo-size-select-small zmdi-hc-fw"></i>
                        <span>photo-size-select-small</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="picture-in-picture" data-code="f396">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-picture-in-picture zmdi-hc-fw"></i>
                        <span>picture-in-picture</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="slideshow" data-code="f397">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-slideshow zmdi-hc-fw"></i>
                        <span>slideshow</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="texture" data-code="f398">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-texture zmdi-hc-fw"></i>
                        <span>texture</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="tonality" data-code="f399">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-tonality zmdi-hc-fw"></i>
                        <span>tonality</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="vignette" data-code="f39a">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-vignette zmdi-hc-fw"></i>
                        <span>vignette</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="wb-auto" data-code="f39b">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-wb-auto zmdi-hc-fw"></i>
                        <span>wb-auto</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="eject-alt" data-code="f39c">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-eject-alt zmdi-hc-fw"></i>
                        <span>eject-alt</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="eject" data-code="f39d">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-eject zmdi-hc-fw"></i>
                        <span>eject</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="equalizer" data-code="f39e">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-equalizer zmdi-hc-fw"></i>
                        <span>equalizer</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="fast-forward" data-code="f39f">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-fast-forward zmdi-hc-fw"></i>
                        <span>fast-forward</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="fast-rewind" data-code="f3a0">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-fast-rewind zmdi-hc-fw"></i>
                        <span>fast-rewind</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="forward-10" data-code="f3a1">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-forward-10 zmdi-hc-fw"></i>
                        <span>forward-10</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="forward-30" data-code="f3a2">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-forward-30 zmdi-hc-fw"></i>
                        <span>forward-30</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="forward-5" data-code="f3a3">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-forward-5 zmdi-hc-fw"></i>
                        <span>forward-5</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="hearing" data-code="f3a4">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-hearing zmdi-hc-fw"></i>
                        <span>hearing</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="pause-circle-outline" data-code="f3a5">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-pause-circle-outline zmdi-hc-fw"></i>
                        <span>pause-circle-outline</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="pause-circle" data-code="f3a6">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-pause-circle zmdi-hc-fw"></i>
                        <span>pause-circle</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="pause" data-code="f3a7">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-pause zmdi-hc-fw"></i>
                        <span>pause</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="play-circle-outline" data-code="f3a8">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-play-circle-outline zmdi-hc-fw"></i>
                        <span>play-circle-outline</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="play-circle" data-code="f3a9">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-play-circle zmdi-hc-fw"></i>
                        <span>play-circle</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="play" data-code="f3aa">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-play zmdi-hc-fw"></i>
                        <span>play</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="playlist-audio" data-code="f3ab">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-playlist-audio zmdi-hc-fw"></i>
                        <span>playlist-audio</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="playlist-plus" data-code="f3ac">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-playlist-plus zmdi-hc-fw"></i>
                        <span>playlist-plus</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="repeat-one" data-code="f3ad">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-repeat-one zmdi-hc-fw"></i>
                        <span>repeat-one</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="repeat" data-code="f3ae">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-repeat zmdi-hc-fw"></i>
                        <span>repeat</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="replay-10" data-code="f3af">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-replay-10 zmdi-hc-fw"></i>
                        <span>replay-10</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="replay-30" data-code="f3b0">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-replay-30 zmdi-hc-fw"></i>
                        <span>replay-30</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="replay-5" data-code="f3b1">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-replay-5 zmdi-hc-fw"></i>
                        <span>replay-5</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="replay" data-code="f3b2">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-replay zmdi-hc-fw"></i>
                        <span>replay</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="shuffle" data-code="f3b3">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-shuffle zmdi-hc-fw"></i>
                        <span>shuffle</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="skip-next" data-code="f3b4">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-skip-next zmdi-hc-fw"></i>
                        <span>skip-next</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="skip-previous" data-code="f3b5">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-skip-previous zmdi-hc-fw"></i>
                        <span>skip-previous</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="stop" data-code="f3b6">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-stop zmdi-hc-fw"></i>
                        <span>stop</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="surround-sound" data-code="f3b7">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-surround-sound zmdi-hc-fw"></i>
                        <span>surround-sound</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="tune" data-code="f3b8">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-tune zmdi-hc-fw"></i>
                        <span>tune</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="volume-down" data-code="f3b9">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-volume-down zmdi-hc-fw"></i>
                        <span>volume-down</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="volume-mute" data-code="f3ba">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-volume-mute zmdi-hc-fw"></i>
                        <span>volume-mute</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="volume-off" data-code="f3bb">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-volume-off zmdi-hc-fw"></i>
                        <span>volume-off</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="volume-up" data-code="f3bc">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-volume-up zmdi-hc-fw"></i>
                        <span>volume-up</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="cloud-box" data-code="f217">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-cloud-box zmdi-hc-fw"></i>
                        <span>cloud-box</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="cloud-circle" data-code="f218">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-cloud-circle zmdi-hc-fw"></i>
                        <span>cloud-circle</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="cloud-done" data-code="f219">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-cloud-done zmdi-hc-fw"></i>
                        <span>cloud-done</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="cloud-download" data-code="f21a">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-cloud-download zmdi-hc-fw"></i>
                        <span>cloud-download</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="cloud-off" data-code="f21b">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-cloud-off zmdi-hc-fw"></i>
                        <span>cloud-off</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="cloud-outline-alt" data-code="f21c">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-cloud-outline-alt zmdi-hc-fw"></i>
                        <span>cloud-outline-alt</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="cloud-outline" data-code="f21d">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-cloud-outline zmdi-hc-fw"></i>
                        <span>cloud-outline</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="cloud-upload" data-code="f21e">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-cloud-upload zmdi-hc-fw"></i>
                        <span>cloud-upload</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="cloud" data-code="f21f">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-cloud zmdi-hc-fw"></i>
                        <span>cloud</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="download" data-code="f220">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-download zmdi-hc-fw"></i>
                        <span>download</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="file-plus" data-code="f221">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-file-plus zmdi-hc-fw"></i>
                        <span>file-plus</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="file-text" data-code="f222">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-file-text zmdi-hc-fw"></i>
                        <span>file-text</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="file" data-code="f223">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-file zmdi-hc-fw"></i>
                        <span>file</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="folder-outline" data-code="f224">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-folder-outline zmdi-hc-fw"></i>
                        <span>folder-outline</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="folder-person" data-code="f225">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-folder-person zmdi-hc-fw"></i>
                        <span>folder-person</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="folder-star-alt" data-code="f226">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-folder-star-alt zmdi-hc-fw"></i>
                        <span>folder-star-alt</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="folder-star" data-code="f227">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-folder-star zmdi-hc-fw"></i>
                        <span>folder-star</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="folder" data-code="f228">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-folder zmdi-hc-fw"></i>
                        <span>folder</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="gif" data-code="f229">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-gif zmdi-hc-fw"></i>
                        <span>gif</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="upload" data-code="f22a">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-upload zmdi-hc-fw"></i>
                        <span>upload</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="account-add" data-code="f1ff">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-account-add zmdi-hc-fw"></i>
                        <span>account-add</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="account-box-mail" data-code="f200">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-account-box-mail zmdi-hc-fw"></i>
                        <span>account-box-mail</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="account-box-o" data-code="f201">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-account-box-o zmdi-hc-fw"></i>
                        <span>account-box-o</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="account-box-phone" data-code="f202">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-account-box-phone zmdi-hc-fw"></i>
                        <span>account-box-phone</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="account-box" data-code="f203">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-account-box zmdi-hc-fw"></i>
                        <span>account-box</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="account-calendar" data-code="f204">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-account-calendar zmdi-hc-fw"></i>
                        <span>account-calendar</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="account-circle" data-code="f205">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-account-circle zmdi-hc-fw"></i>
                        <span>account-circle</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="account-o" data-code="f206">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-account-o zmdi-hc-fw"></i>
                        <span>account-o</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="account" data-code="f207">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-account zmdi-hc-fw"></i>
                        <span>account</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="accounts-add" data-code="f208">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-accounts-add zmdi-hc-fw"></i>
                        <span>accounts-add</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="accounts-alt" data-code="f209">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-accounts-alt zmdi-hc-fw"></i>
                        <span>accounts-alt</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="accounts-list-alt" data-code="f20a">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-accounts-list-alt zmdi-hc-fw"></i>
                        <span>accounts-list-alt</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="accounts-list" data-code="f20b">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-accounts-list zmdi-hc-fw"></i>
                        <span>accounts-list</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="accounts-outline" data-code="f20c">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-accounts-outline zmdi-hc-fw"></i>
                        <span>accounts-outline</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="accounts" data-code="f20d">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-accounts zmdi-hc-fw"></i>
                        <span>accounts</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="face" data-code="f20e">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-face zmdi-hc-fw"></i>
                        <span>face</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="female" data-code="f20f">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-female zmdi-hc-fw"></i>
                        <span>female</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="male-alt" data-code="f210">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-male-alt zmdi-hc-fw"></i>
                        <span>male-alt</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="male-female" data-code="f211">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-male-female zmdi-hc-fw"></i>
                        <span>male-female</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="male" data-code="f212">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-male zmdi-hc-fw"></i>
                        <span>male</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="mood-bad" data-code="f213">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-mood-bad zmdi-hc-fw"></i>
                        <span>mood-bad</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="mood" data-code="f214">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-mood zmdi-hc-fw"></i>
                        <span>mood</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="run" data-code="f215">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-run zmdi-hc-fw"></i>
                        <span>run</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="walk" data-code="f216">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-walk zmdi-hc-fw"></i>
                        <span>walk</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="comment-alert" data-code="f25a">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-comment-alert zmdi-hc-fw"></i>
                        <span>comment-alert</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="comment-alt-text" data-code="f25b">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-comment-alt-text zmdi-hc-fw"></i>
                        <span>comment-alt-text</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="comment-alt" data-code="f25c">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-comment-alt zmdi-hc-fw"></i>
                        <span>comment-alt</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="comment-edit" data-code="f25d">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-comment-edit zmdi-hc-fw"></i>
                        <span>comment-edit</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="comment-image" data-code="f25e">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-comment-image zmdi-hc-fw"></i>
                        <span>comment-image</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="comment-list" data-code="f25f">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-comment-list zmdi-hc-fw"></i>
                        <span>comment-list</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="comment-more" data-code="f260">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-comment-more zmdi-hc-fw"></i>
                        <span>comment-more</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="comment-outline" data-code="f261">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-comment-outline zmdi-hc-fw"></i>
                        <span>comment-outline</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="comment-text-alt" data-code="f262">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-comment-text-alt zmdi-hc-fw"></i>
                        <span>comment-text-alt</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="comment-text" data-code="f263">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-comment-text zmdi-hc-fw"></i>
                        <span>comment-text</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="comment-video" data-code="f264">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-comment-video zmdi-hc-fw"></i>
                        <span>comment-video</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="comment" data-code="f265">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-comment zmdi-hc-fw"></i>
                        <span>comment</span>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3" data-name="comments" data-code="f266">
                      <div class="demo-icon-block">
                        <i class="zmdi zmdi-comments zmdi-hc-fw"></i>
                        <span>comments</span>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function(){
        $('.demo-icon-block').on('click', function(){
            var iconName = $(this).parent().attr('data-name');
            $('input[name=m_icon]').val(iconName);
            $('.form-icon .input-group-addon i').removeClass().addClass('zmdi zmdi-'+iconName+' zmdi-hc-lg zmdi-hc-fw');
            $('#list-icon').modal('hide');
        });
    })
</script>