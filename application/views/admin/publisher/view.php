 <div class="row">
     <form method="post" id="content-form" action="<?php echo base_url('goadmin/' . $this->url . '/view/' . $row['id']) ?>" data-toggle="validator" enctype="multipart/form-data">
         <div class="col-md-12">
             <?php $this->load->view('admin/template/fixed_heading', array('type' => 'view', 'name' => $row['name'])); ?>
         </div>
         <input type="hidden" name="id" id="row_id" data-table-name="<?php echo $this->url; ?>" value="<?php echo $row['id'] ?>">


         <?php if (validation_errors()) { ?>
             <!-- SHOW ERROR -->
             <?php echo viewErrorValidation(); ?>
             <!-- END SHOW ERROR -->
         <?php } ?>


         <div class="col-md-8">
             <div class="panel panel-default">
                 <div class="panel-heading">
                     <h4 class="m-y-0 font-bold">Information</h4>
                 </div>


                 <div class="panel-body">
                     <div class="form-horizontal">
                         <div class="form-group">
                             <label for="name" class="control-label col-md-3">Name</label>
                             <div class="col-md-7">
                                 <input type="text" name="name" id="name" class="form-control required " required value="<?php echo $row['name'] ?>">
                                 <div class="help-block with-errors"></div>
                             </div>
                         </div>



                         <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="csrf-token">
                     </div>
                 </div>
             </div>
         </div>

         <div class="col-md-4">
             <?php $this->load->view('admin/template/view_flag'); ?>
         </div>
         <div class="col-md-12 col-sm-12 col-xs-12">
             <div class="panel panel-default">
                 <div class="panel-heading">
                     <h4 class="m-y-0 font-bold">Content</h4>
                 </div>
                 <div class="panel-body">
                     <div class="form-horizontal">

                         <div class="form-group">
                             <div class="col-md-12">
                                 <textarea class="form-control content" name="content" rows="10" id="content"><?php echo $row['content'] ?></textarea>
                                 <div class="help-block with-errors"></div>
                             </div>
                         </div>
                         <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="csrf-token">
                     </div>
                 </div>
             </div>
         </div>
     </form>
     <?php $this->load->view('admin/template/log'); ?>
 </div>
