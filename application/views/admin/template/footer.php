<?php if (!empty($this->url) && ($this->url == 'item' || $this->url == 'caree' || $this->url == 'media')) { ?>
  <!-- Modal -->
  <div class="modal fade" id="addForm" tabindex="-1" role="dialog" aria-labelledby="addFormLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="addFormLabel">Add <?php echo $this->url ?> Category</h4>
        </div>
        <form action="" data-toggle="validator" method="post" id="modal-form-cms">
          <input type="hidden" name="table" value="<?php echo $this->url . '_category'; ?>">
          <div class="modal-body">
            <?php language()->result_array();
            $x = 0;
            foreach (language()->result_array() as $lang) : ?>
              <div class="form-group">

                <label for="name_<?php echo $lang['id'] ?>" class="control-label col-md-4"><img src="<?php echo base_url("lib/images/goadmin/flag/" . $lang['icon']); ?>" width="30"> Category Name</label>
                <div class="col-md-8">
                  <input type="text" name="name_<?php echo $lang['id'] ?>" id="name_<?php echo $lang['id'] ?>" class="form-control required" required>
                  <div class="help-block with-errors"></div>
                </div>
              </div>
            <?php $x++;
            endforeach; ?>
            <br>

          </div>
          <div class="modal-footer">
            <div class="col-md-11">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">
                <i class="zmdi zmdi-plus-square zmdi-hc-lg zmdi-hc-fw"></i>&nbsp;Submit
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
<?php } ?>

<?php if ($this->uri->rsegment(1) != 'login' || $this->session->userdata('admin_login') != '') { ?>
  </div>
  <!-- END CONTENT -->

  <!-- FOOTER -->
  <div class="site-footer">
    <?php echo date('Y'); ?> &copy; Go Online Solusi. v3.3.5
  </div>
  <!-- END FOOTER -->

  </div>
  <!-- END MAIN -->
<?php } ?>
<?php if ($this->uri->rsegment(1) != 'login' || $this->session->userdata('admin_login') != '') { ?>
  <script type="text/javascript" src="<?php echo base_url('lib/js/goadmin/lib.js'); ?>"></script>
<?php } ?>

<script type="text/javascript" src="<?php echo base_url('lib/js/goadmin/plugins/jqueryValidate/jquery.validate.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('lib/js/goadmin/plugins/jqueryClock/jquery.jclock.js'); ?>"></script>

<?php if ($this->uri->rsegment(2) == 'add' || $this->uri->rsegment(2) == 'view') { ?>
  <script type="importmap">
    {
				"imports": {
					"ckeditor5": "<?php echo base_url('lib/js/goadmin/ckeditor5/ckeditor5.js') ?>",
					"ckeditor5/": "<?php echo base_url('lib/js/goadmin/ckeditor5/') ?>"
				}
			}
		</script>

  <script type="module">
    import {
      Bookmark,
      ClassicEditor,
      Alignment,
      Autoformat,
      AutoImage,
      Autosave,
      BlockQuote,
      Bold,
      Essentials,
      FontColor,
      FullPage,
      Heading,
      ImageBlock,
      ImageCaption,
      ImageInline,
      ImageInsert,
      ImageInsertViaUrl,
      ImageResize,
      ImageStyle,
      ImageTextAlternative,
      ImageToolbar,
      ImageUpload,
      Indent,
      IndentBlock,
      Italic,
      Link,
      LinkImage,
      List,
      MediaEmbed,
      Paragraph,
      PasteFromOffice,
      RemoveFormat,
      SimpleUploadAdapter,
      SourceEditing,
      Subscript,
      Superscript,
      Table,
      TableCaption,
      TableCellProperties,
      TableColumnResize,
      TableProperties,
      TableToolbar,
      TextTransformation,
      Underline,
      GeneralHtmlSupport
    } from 'ckeditor5';
    const editorConfig = {
      toolbar: {
        items: [
          'sourceEditing', '|', 'heading', '|', 'fontColor', '|', 'bold', 'italic', 'underline', 'subscript', 'superscript', 'removeFormat', '|', 'link','bookmark', 'insertImage', 'mediaEmbed', 'insertTable', 'blockQuote', '|', 'alignment', '|', 'bulletedList', 'numberedList', 'outdent', 'indent'
        ],
        shouldNotGroupWhenFull: false
      },
      plugins: [
        Bookmark, Alignment, Autoformat, AutoImage, Autosave, BlockQuote, Bold, Essentials, FontColor, FullPage, Heading, ImageBlock, ImageCaption, ImageInline, ImageInsert, ImageInsertViaUrl, ImageResize, ImageStyle, ImageTextAlternative, ImageToolbar, ImageUpload, Indent, IndentBlock, Italic, Link, LinkImage, List, MediaEmbed, Paragraph, PasteFromOffice, RemoveFormat, SimpleUploadAdapter, SourceEditing, Subscript, Superscript, Table, TableCaption, TableCellProperties, TableColumnResize, TableProperties, TableToolbar, TextTransformation, Underline, GeneralHtmlSupport
      ],
      heading: {
        options: [{
            model: 'paragraph',
            title: 'Paragraph',
            class: 'ck-heading_paragraph'
          },
          {
            model: 'heading1',
            view: 'h1',
            title: 'Heading 1',
            class: 'ck-heading_heading1'
          },
          {
            model: 'heading2',
            view: 'h2',
            title: 'Heading 2',
            class: 'ck-heading_heading2'
          },
          {
            model: 'heading3',
            view: 'h3',
            title: 'Heading 3',
            class: 'ck-heading_heading3'
          },
          {
            model: 'heading4',
            view: 'h4',
            title: 'Heading 4',
            class: 'ck-heading_heading4'
          },
          {
            model: 'heading5',
            view: 'h5',
            title: 'Heading 5',
            class: 'ck-heading_heading5'
          },
          {
            model: 'heading6',
            view: 'h6',
            title: 'Heading 6',
            class: 'ck-heading_heading6'
          }
        ]
      },
      image: {
        toolbar: [
          'toggleImageCaption',
          'imageTextAlternative',
          '|',
          'imageStyle:inline',
          'imageStyle:wrapText',
          'imageStyle:breakText',
          '|',
          'resizeImage'
        ]
      },
      licenseKey: 'GPL',
      link: {
        decorators: {
          openInNewTab: {
            mode: 'manual',
            label: 'Open in a new tab',
            attributes: {
              target: '_blank',
              rel: 'noopener noreferrer'
            }
          }
        }
      },
      placeholder: '',
      table: {
        contentToolbar: ['tableColumn', 'tableRow', 'mergeTableCells', 'tableProperties', 'tableCellProperties']
      },
      simpleUpload: {
        // Upload the images to the server .
        uploadUrl: '<?php echo base_url('goadmin/ajax/upload-image') ?>',
      },
      htmlSupport: {
        allow: [{
          name: /.*/,
          attributes: true,
          classes: true,
          styles: true
        }]
      }
    };


    if ($('.content').length) {
      var allEditors = document.querySelectorAll('.content');
      for (var i = 0; i < allEditors.length; ++i) {
        (function(index) {
          ClassicEditor.create(allEditors[index], editorConfig);

        })(i);
      }
    }
  </script>
<?php /* ?>
  <script type="text/javascript" src="<?php echo base_url('lib/js/goadmin/plugins/ckeditor/ckeditor.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('lib/js/goadmin/plugins/ckeditor/adapters/jquery.js');?>"></script>
  <script type="text/javascript" src="<?php echo base_url('lib/js/goadmin/plugins/ckeditor5/ckeditor.js');?>"></script>
  <!-- <script src="<?php echo base_url('lib/js/goadmin/module_ckeditor.js');?>" type="module"></script> -->
	<script type="text/javascript" src="<?php echo base_url('lib/js/goadmin/plugins/ckeditor5/ckeditor_run.js');?>"></script>
  <style type="text/css">
    .ck.ck-content.ck-editor__editable.ck-rounded-corners.ck-editor__editable_inline{
      min-height: 250px;
    }
  </style>

	<?php  */
} ?>

</body>

</html>