        <?php if(!empty($row['id']))
        {
            if($this->uri->segments[2]=='admin'){
                $logs = select_log($this->url,$row['id'],'all');
            } else {
                 $logs = select_log($this->url,$row['id']);
            }
        } else if (!empty($row[$default]['id']))
        {
             $logs = select_log($this->url,$row[$default]['id']) ;
        } ?>
        <div class="col-md-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="m-y-0 font-bold">ADMIN LOG</h4>
                </div>


                <div class="panel-body">
                    <div class="form-horizontal">
                        <table id="list-log" data-name-table="<?php echo $url; ?>" class="table table-striped dataTable">
                            <thead align="center">
                                <tr>
                                    <th>No</th>
                                    <th class="text-center">User</th>
                                    <th class="text-center">Action</th>
                                    <th class="text-center">IP</th>
                                    <th class="text-center">User Agent</th>
                                    <th class="text-center">Date</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($logs as $key => $value) { ?>
                                <tr>    
                                    <td><?php echo $key+1; ?></td>
                                    <td class="text-center"><?php echo $value['admin_name'] ?></td>
                                    <td class="text-center"><?php echo $value['desc'] ?></td>
                                    <td class="text-center"><?php echo $value['ip'] ?></td>
                                    <td class="text-center"><?php echo $value['user_agent'] ?></td>
                                    <td class="text-center"><?php echo format_date($value['date']); ?></td>
                                </tr>
                            <?php  } ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>