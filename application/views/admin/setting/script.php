 <div class="row">
    <form method="post" id="content-form" action="<?php echo base_url('goadmin/'.$this->url);?>" data-toggle="validator" enctype="multipart/form-data">
        <div class="col-md-12">
            <?php $this->load->view('admin/template/fixed_heading', array('type' => 'view', 'name' => $row['name'])); ?>
        </div>
        <input type="hidden" name="id" id="row_id" data-table-name="<?php echo $this->url;?>" value="<?php echo $row['id'] ?>">

        
        <?php if(validation_errors()){ ?>
            <!-- SHOW ERROR -->
            <?php echo viewErrorValidation();?>
            <!-- END SHOW ERROR -->
        <?php } ?>

        
        <div class="col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="m-y-0 font-bold">Script</h4>
                </div>

                <div class="panel-body">
                    <div class="form-horizontal">
                        
                    <?php foreach ($script as $key => $value) {?>
                        <div class="form-group">
                            <label for="<?php echo $value['key'] ?>" class="control-label col-md-3"><?php echo $value['label'] ?></label>
                            <div class="col-md-7">
                                <textarea name="<?php echo $value['key'] ?>" id="<?php echo $value['key'] ?>" class=" form-control" rows="5"><?php echo $value['value'] ?></textarea>
                                <div class="help-block with-errors"></div>
                                <small><?php echo $value['note']; ?></small>
                            </div>
                        </div>
                    <?php } ?>
                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="csrf-token">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="m-y-0 font-bold">Re-captcha &amp; Key</h4>
                </div>

                <div class="panel-body">
                    <div class="form-horizontal">
                        
                    <?php foreach ($captcha as $key => $value) {?>
                        <div class="form-group">
                            <label for="<?php echo $value['key'] ?>" class="control-label col-md-3"><?php echo $value['label'] ?></label>
                            <div class="col-md-7">
                                <textarea name="<?php echo $value['key'] ?>" id="<?php echo $value['key'] ?>" class=" form-control"><?php echo $value['value'] ?></textarea>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    <?php } ?>
                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="csrf-token">
                    </div>
                </div>
            </div>
        </div>
    </form>
    <?php $this->load->view('admin/template/log'); ?>
</div>
