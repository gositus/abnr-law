 <div class="row">
    <form method="post" id="content-form" action="<?php echo base_url('goadmin/'.$this->url.'/view/'.$row['id'])?>" data-toggle="validator" enctype="multipart/form-data">
        <div class="col-md-12">
            <?php $this->load->view('admin/template/fixed_heading', array('type' => 'view', 'name' => $row['name'])); ?>
        </div>
        <input type="hidden" name="id" id="row_id" data-table-name="<?php echo $this->url;?>" value="<?php echo $row['id'] ?>">

        
        <?php if(validation_errors()){ ?>
            <!-- SHOW ERROR -->
            <?php echo viewErrorValidation();?>
            <!-- END SHOW ERROR -->
        <?php } ?>

        
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="m-y-0 font-bold">Information</h4>
                </div>


                <div class="panel-body">
                    <div class="form-horizontal">        
                        <div class="form-group">
                            <label for="s_name" class="control-label col-md-3">Name</label>
                            <div class="col-md-7">
                                <input type="text" name="s_name" id="s_name" class="form-control required " required value="<?php echo $row['name'] ?>">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                       <div class="form-group">
                            <label for="s_type" class="control-label col-md-3">Type</label>
                            <div class="col-md-7">
                                <select name="s_type" id="s_type" class="form-control required" required>
                                    <option value="">--</option>
                                    <option value="1" <?php echo($row['type']==1)? 'selected' : ''; ?>>Page</option>
                                    <option value="2" <?php echo($row['type']==2)? 'selected' : ''; ?>>Banner</option>
                                </select>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <?php if(!empty($row['banner_size'])){ $size = explode(";", $row['banner_size']) ;} ?>
                        <div class="form-group banner-section">
                            <label for="s_width" class="control-label col-md-3">Banner Width</label>
                            <div class="col-md-7">
                                <input type="text" name="s_width" id="s_width" class="form-control number" placeholder="(px)" value="<?php echo (!empty($size))? $size[0] : ''; ?>">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group banner-section">
                            <label for="s_height" class="control-label col-md-3">Banner Height</label>
                            <div class="col-md-7">
                                <input type="text" name="s_height" id="s_height" class="form-control number" placeholder="(px)" value="<?php echo (!empty($size))? $size[1] : ''; ?>">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        
                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="csrf-token">
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <?php $this->load->view('admin/template/view_flag');?>
        </div>
    </form>
    <?php $this->load->view('admin/template/log'); ?>
</div>
<script type="text/javascript">
    <?php if($row['type']==1) { ?>
    $(".banner-section").hide();
    <?php } ?>

</script>

