<div class="row">
    <form method="post" id="content-form" action="<?php echo base_url('goadmin/' . $this->url . '/add') ?>" data-toggle="validator" enctype="multipart/form-data">

        <div class="col-md-12">
            <?php $this->load->view('admin/template/fixed_heading', array('type' => 'add')); ?>
        </div>

        <?php if (validation_errors()) { ?>
            <!-- SHOW ERROR -->
            <?php echo viewErrorValidation(); ?>
            <!-- END SHOW ERROR -->
        <?php } ?>

        <div class="col-md-8 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="m-y-0 font-bold">Information</h4>
                </div>


                <div class="panel-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label for="category" class="control-label col-md-3">Category</label>
                            <div class="col-md-7">
                                <select name="category" class="form-control required" required>
                                    <option value="">--</option>
                                    <?php foreach ($category as $value) { ?>
                                        <option value="<?php echo $value['id'] ?>"><?php echo $value['name'] ?></option>
                                    <?php } ?>
                                </select>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="publisher" class="control-label col-md-3">Publisher</label>
                            <div class="col-md-7">
                                <select name="publisher" class="form-control required" required>
                                    <option value="">--</option>
                                    <?php foreach ($publisher as $value) { ?>
                                        <option value="<?php echo $value['id'] ?>"><?php echo $value['name'] ?></option>
                                    <?php } ?>
                                </select>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="publish_date" class="control-label col-md-3">Publish Date</label>
                            <div class="col-md-7">
                                <input type="text" name="publish_date" class="required form-control datepicker-start" id="publish_date" readonly>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="due_date" class="control-label col-md-3">Due Date</label>
                            <div class="col-md-7">
                                <input type="text" name="due_date" class="required form-control datepicker-start" id="due_date" readonly>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>


                                <div class="form-group">
                                    <label for="name" class="control-label col-md-3">Title </label>
                                    <div class="col-md-7">
                                        <input type="text" name="name" id="name" class="form-control required" required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>


                        <div class="form-group">
                            <label for="author" class="control-label col-md-3">Author</label>
                            <div class="col-md-7">
                                <input type="text" name="author" id="author" class="form-control" value="">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <?php $x = 0;
                        foreach (language()->result_array() as $lang) : ?>
                            <div class="language lang-<?php echo $lang['attr'] ?>" <?php if ($x == 0) echo ' style="display:block"'; ?>>
                                <div class="form-group">
                                    <label for="description_<?php echo $lang['id'] ?>" class="control-label col-md-3">Description </label>
                                    <div class="col-md-7">
                                        <textarea name="description_<?php echo $lang['id'] ?>" id="description_<?php echo $lang['id'] ?>" class="form-control"></textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>

                            </div>
                        <?php $x++;
                        endforeach; ?>

                        <?php $x = 0;
                        foreach (language()->result_array() as $lang) : ?>
                            <div class="language lang-<?php echo $lang['attr'] ?>" <?php if ($x == 0) echo ' style="display:block"'; ?>>
                                <div class="form-group">
                                    <label for="file_<?php echo $lang['id'] ?>" class="control-label col-md-3">File </label>
                                    <div class="col-md-7">
                                        <input type="file" name="file_<?php echo $lang['id'] ?>" id="file_<?php echo $lang['id'] ?>" class="form-control required" required accept=".pdf,.docx,doc,.xls,.xlsx">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>

                            </div>
                        <?php $x++;
                        endforeach; ?>


                        <div class="form-group upload-image">
                            <label class="control-label col-md-3">Image</label>
                            <div class="col-md-7">
                                <div class="row">
                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                        <label class="btn btn-default file-upload-btn">
                                            Choose file...
                                            <button class="file-upload-input" name="icon" id="ckfinder-upload" type="button"></button>
                                        </label>
                                    </div>
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div class="wrap-info-path">
                                            <input type="text" class="info-path-image  show form-control" readonly id="finder-image" name="image">
                                            <input type="file" name="image" style="display: none;" accept="image/*">
                                        </div>
                                    </div>
                                </div>
                                <p class="help-block">
                                    <small>Recommended size <?php echo $this->image_width . ' * ' . $this->image_height; ?> px</small>
                                </p>
                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="wrap-preview-image show">
                                            <img src="" class="upload-preview">
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-12 col-xs-12">
            <?php $this->load->view('admin/template/add_flag'); ?>
        </div>

    </form>
</div>