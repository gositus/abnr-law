<?php if($this->session->flashdata('success')) { ?>
	<script>
		$(function(){
			showToast('success', 'bottom', "<?php echo $this->session->flashdata('success');?>");
		})
	</script>
<?php } ?>
<div class="row">
	<div class="col-md-12">
		<?php $this->load->view('admin/template/fixed_heading', array('type' => 'list')); ?>
	</div>
    
    <div class="col-md-12">	
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="m-y-0 font-bold">List <?php echo $title; ?></h4>
                <div class="error-input"><?php echo validation_errors();?></div>
            </div>
            <div class="panel-body">
                <div class="form-horizontal">
                	<div class="table-responsive">
						
						<table id="list-table" data-table-name="<?php echo $url; ?>" class="table table-striped dataTable">
							<thead>
								<tr>
									<th>No</th>
									<th>Name</th>
                                    <th>Sort</th>
									<?php $this->load->view('admin/template/list_table_heading')?>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
                	</div>
				</div>
			</div>
		</div>
	</div>
</div>
    <script>
        // window.addEventListener('load', function () {
        //     $( ".table_search" ).change();
        // });

        function order_up(id) {
            $.ajax({
                url: "<?php echo base_url() ?>" + "/goadmin/achievement-publication/order_up/" + id,
                success: function(html) {
                    $('#list-table').DataTable().ajax.reload();
                }
            });
        }

        function order_down(id) {
            $.ajax({
                url: "<?php echo base_url() ?>" + "/goadmin/achievement-publication/order_down/" + id,
                success: function(html) {
                    $('#list-table').DataTable().ajax.reload();
                }
            });
        }
    </script>
<?php 
	//SHOW DATA TO DATATABLE
	// ====================================================================================
	$showData = array(

		//NAMA FIELD BERDASARKAN ATTRIBUTE YG ADA PADA TABLE
		'field' => array(
			'name',
            'sort'
		),

		// ================================================================================
		// CUSTOM CONTENT (KOSONGKAN BILA TIDAK DIPAKAI)
		// ROW = HASIL OBJECT DARI AJAX 
		// (CONTOH: UNTUK MENGAMBIL VALUE DARI TABLE (row.icon))
		// CONTOH PENGGUNAAN : 'icon'  => '<img src="\'+base_url+\'lib/images/\'+row.icon+\'"/>',
		// row.icon = "icon" NAMA ATTRIBUTE TABEL. YG DIRUBAH HANYALAH ATTRIBUTE TABLE!!
		// ================================================================================
		'custom_content' => array(
            'sort'	=> '
					var order="";
					if(row.first==0){
						order+= "<a href=\"javascript:;\" onclick=\"order_up("+row.id+")\" data-id=\""+row.id+"\" style=\"display: block;\"><i class=\"fa fa-2x fa-caret-up\"></i></a>";
					}
					if(row.last==0){
						order+= "<a href=\"javascript:;\" onclick=\"order_down("+row.id+")\" data-id=\""+row.id+"\" style=\"display: block;\"><i class=\"fa fa-2x fa-caret-down\"></i></a>";
					}

					if(row.first==1 && row.last==1){
						return ""
					}else{
						return order
					}'
		)
	);
?>
<?php $this->load->view('admin/template/list_table_data', $showData); ?>