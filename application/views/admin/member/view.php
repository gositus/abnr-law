<div class="row">
    <form method="post" id="content-form" action="<?php echo base_url('goadmin/'.$this->url.'/view/'.$row['id'])?>" data-toggle="validator" enctype="multipart/form-data">
        <div class="col-md-12">
            <?php $this->load->view('admin/template/fixed_heading', array('type' => 'view', 'name' => $row['name'])); ?>
        </div>
        <input type="hidden" name="id" id="row_id" data-table-name="<?php echo $this->url;?>" value="<?php echo $row['id'] ?>">

        
        <?php if(validation_errors()){ ?>
            <!-- SHOW ERROR -->
            <?php echo viewErrorValidation();?>
            <!-- END SHOW ERROR -->
        <?php } ?>

        
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="m-y-0 font-bold">Information</h4>
                </div>


                <div class="panel-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label for="name" class="control-label col-md-3">Registration Date</label>
                            <div class="col-md-7">
                                <input type="text" class="form-control disabled" disabled  autocomplete="off" value="<?php echo format_date($row['date_added']);?>">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="control-label col-md-3">Name</label>
                            <div class="col-md-7">
                                <input type="text" name="name" id="name" class="form-control required" required  autocomplete="off" value="<?php echo $row['name'];?>">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="email" class="control-label col-md-3">Email</label>
                            <div class="col-md-7">
                                <input type="text" name="email" id="email" class="email form-control required check_email" required autocomplete="off " value="<?php echo $row['email'];?>">
                                <div class="help-block">
                                   <p>Note: Digunakan untuk Forgot Password</p>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="phone" class="control-label col-md-3">Phone</label>
                            <div class="col-md-7">
                                <input type="text" name="phone" id="phone" class="number form-control" value="<?php echo $row['phone'];?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="dob" class="control-label col-md-3">DOB</label>
                            <div class="col-md-7">
                                <input type="text" name="dob" id="dob" class=" form-control datepicker-dob" <?php if($row['dob'] != '0000-00-00'){?> value="<?php echo format_date($row['dob']);?>" <?php } ?>>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="gender" class="control-label col-md-3">Gender</label>
                            <div class="col-md-7">
                                <select name="gender" id="gender" class=" form-control" value="<?php echo $row['gender'];?>">
                                    <option value="">--</option>
                                    <option value="m" <?php echo ($row['gender']=='m')? 'selected' : '';?> >Male</option>
                                    <option value="f" <?php echo ($row['gender']=='f')? 'selected' : '';?> >Female</option>
                                </select>
                            </div>
                        </div>


                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="csrf-token">
                    </div>
                </div>
            </div>
        </div>
        <?php if (check_access($this->url, 'edit')) { ?>
        <div class="col-md-4">
            <?php $this->load->view('admin/template/view_flag');?>
        </div>
        <?php } ?>
         <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="m-y-0 font-bold">Change Password</h4>
                </div>


                <div class="panel-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label for="password" class="control-label col-md-3">Password</label>
                            <div class="col-md-7">
                                <input type="password" name="password" id="password" class="form-control pass_word" minlength="8" >
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="repassword" class="control-label col-md-3">Re - Password</label>
                            <div class="col-md-7">
                                <input type="password" name="repassword" id="repassword" class="form-control pass_word" equalTo="#password"  >
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </form>
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="m-y-0 font-bold">Member Log</h4>
                </div>


                <div class="panel-body">
                    <div class="form-horizontal">
                        <table id="list-log" data-name-table="<?php echo $url; ?>" class="table table-striped dataTable">
                            <thead align="center">
                                <tr>
                                    <th>No</th>
                                    <th>Action</th>
                                    <th class="text-center">IP</th>
                                    <th class="text-center">User Agent</th>
                                    <th class="text-center">Date</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($member_log as $key => $val) { ?>
                                <tr>    
                                    <td><?php echo $key+1; ?></td>
                                    <td class="text-center"><?php echo $val['action'] ?></td>
                                    <td class="text-center"><?php echo $val['ip'] ?></td>
                                    <td class="text-center"><?php echo $val['user_agent'] ?></td>
                                    <td class="text-center"><?php echo format_date($val['date']); ?></td>
                                </tr>
                            <?php  } ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>

<?php $this->load->view('admin/template/log'); ?>
</div>
<script type="text/javascript">
    $(function(){
        var password= null;
        $.validator.addMethod("pass_word", function(value) {
            $.ajax({
                type: "POST",
                url: '<?php echo base_url("goadmin/admin/check_pattern"); ?>',
                data:{
                        'key':value,
                        '<?php echo $this->security->get_csrf_token_name();?>':$("#csrf-token").val()
                    },
                    success: function(data){
                        var obj = JSON.parse(data);
                        password= obj.a;
                        $("#csrf-token").val(obj.b);
                        
                    }
                });
             if (password == 'false'){
                return false;
             } else{
                return true;
             }
        },'Must contain Uppercase, Lowercase, Digit and special characters');

    })

</script>