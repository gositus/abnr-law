 <div class="row">
    <form method="post" id="content-form" action="<?php echo base_url('goadmin/'.$this->url.'/view/'.$row['id'])?>" data-toggle="validator" enctype="multipart/form-data">
        <div class="col-md-12">
            <?php $this->load->view('admin/template/fixed_heading', array('type' => 'view', 'name' => $row['name'])); ?>
        </div>
        <input type="hidden" name="id" id="row_id" data-table-name="<?php echo $this->url;?>" value="<?php echo $row['id'] ?>">

        
        <?php if(validation_errors()){ ?>
            <!-- SHOW ERROR -->
            <?php echo viewErrorValidation();?>
            <!-- END SHOW ERROR -->
        <?php } ?>

        
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="m-y-0 font-bold">Information</h4>
                </div>


                <div class="panel-body">
                    <div class="form-horizontal">        
                        <div class="form-group">
                            <label for="name" class="control-label col-md-3">Name</label>
                            <div class="col-md-7">
                                <input type="text" name="name" id="name" class="form-control required " required value="<?php echo $row['name'] ?>">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                       <div class="form-group">
                            <label for="coordinate" class="control-label col-md-3">Coordinate</label>
                            <div class="col-md-7">
                                <input type="text" name="coordinate" id="coordinate" class="form-control required" required value="<?php echo $row['coordinate'] ?>">
                                <div class="help-block with-errors"><small>Ex : -5.0000000;106.8068505</small></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="country_id" class="control-label col-md-3">Country</label>
                            <div class="col-md-7">
                                <select name="country_id" id="country_id" class="form-control">
                                    <option value="">--</option>
                                    <?php foreach ($country as $key => $value) {?>
                                        <option value="<?php echo $value['id'] ?>" <?php echo($value['id']==$row['country_id'])? 'selected': ''; ?>><?php echo $value['name'] ?></option>
                                    <?php } ?>
                                </select>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="city_id" class="control-label col-md-3">City</label>
                            <div class="col-md-7">
                                <select name="city_id" id="city_id" class="form-control">
                                    <option value="">--</option>
                                    <?php foreach ($city as $key => $value) {?>
                                        <option value="<?php echo $value['id'] ?>" <?php echo($value['id']==$row['city_id'])? 'selected': ''; ?>><?php echo $value['name'] ?></option>
                                    <?php } ?>
                                </select>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="phone" class="control-label col-md-3">Phone</label>
                            <div class="col-md-7">
                                <input type="text" name="phone" id="phone" class="form-control " value="<?php echo $row['phone'] ?>">
                                <div class="help-block with-errors"><small>Separate with semicolon(;)</small></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="mobile" class="control-label col-md-3">Mobile</label>
                            <div class="col-md-7">
                                <input type="text" name="mobile" id="mobile" class="form-control " value="<?php echo $row['mobile'] ?>" >
                                <div class="help-block with-errors"><small>Separate with semicolon(;)</small></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="control-label col-md-3">Email</label>
                            <div class="col-md-7">
                                <input type="text" name="email" id="email" class="form-control " value="<?php echo $row['email'] ?>">
                                <div class="help-block with-errors"><small>Separate with semicolon(;)</small></div>
                            </div>
                        </div>
                        
                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="csrf-token">
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <?php $this->load->view('admin/template/view_flag');?>
        </div>
    </form>
    <?php $this->load->view('admin/template/log'); ?>
</div>
<script type="text/javascript">
    <?php if($row['type']==1) { ?>
    $(".banner-section").hide();
    <?php } ?>

</script>

