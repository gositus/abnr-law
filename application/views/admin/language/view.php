 <div class="row">
    <form method="post" id="content-form" action="<?php echo base_url('goadmin/'.$this->url.'/view/'.$row['id'])?>" data-toggle="validator" enctype="multipart/form-data">
        <div class="col-md-12">
            <?php $this->load->view('admin/template/fixed_heading', array('type' => 'view', 'name' => $row['name'])); ?>
        </div>
        <input type="hidden" name="id" id="row_id" data-table-name="<?php echo $this->url;?>" value="<?php echo $row['id'] ?>">

        
        <?php if(validation_errors()){ ?>
            <!-- SHOW ERROR -->
            <?php echo viewErrorValidation();?>
            <!-- END SHOW ERROR -->
        <?php } ?>

        
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="m-y-0 font-bold">Information</h4>
                </div>


                <div class="panel-body">
                    <div class="form-horizontal">        
                        <div class="form-group">
                            <label for="name" class="control-label col-md-3">Name</label>
                            <div class="col-md-7">
                                <input type="text" name="name" id="name" class="form-control required " required value="<?php echo $row['name'] ?>">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="attr" class="control-label col-md-3">Attr</label>
                            <div class="col-md-7">
                                <input type="text" name="attr" id="attr" class="form-control required " required value="<?php echo $row['attr'] ?>">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="sort" class="control-label col-md-3">Sort</label>
                            <div class="col-md-7">
                                <input type="number" name="sort" id="sort" class="number form-control" value="<?php echo $row['sort'] ?>" min="0" max="999">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group upload-image <?php echo ($row['icon']) ? 'show' : ''; ?>">
                            <label class="control-label col-md-3">Icon</label>
                            <div class="col-md-7">
                                <div class="row">
                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                        <label class="btn btn-default file-upload-btn">
                                            Choose file...
                                            <input type="file" class="file-upload-input" name="icon">
                                        </label>
                                    </div>
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div class="wrap-info-path">
                                            <input type="text" class="info-path-image form-control <?php echo ($row['icon']) ? 'show' : ''; ?>" disabled value="<?php echo $row['icon']; ?>">
                                        </div>
                                    </div>
                                </div>
                                <p class="help-block">
                                    <small>Recommended size <?php echo $this->image_width . ' * ' . $this->image_height ; ?> px</small>
                                </p>

                                <div class="row">
                                	<div class="col-md-9">
                                		<div class="wrap-preview-image <?php echo ($row['icon']) ? 'show' : ''; ?>">
                                		    <img src="<?php echo base_url('lib/images/flag/'.$row['icon']); ?>" class="upload-preview">
                                		</div>		
                                	</div>
                                	<div class="col-md-3">
                                		<div class="wrap-delete-image <?php echo ($row['icon']) ? 'show' : ''; ?>">
                                			<button type="button" class="btn btn-danger btn-delete-img"><i class="zmdi zmdi-close"></i></button>
                                		</div>	
                                	</div>
                                </div>
                            </div>
                        </div>
                        
                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="csrf-token">
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <?php $this->load->view('admin/template/view_flag');?>
        </div>
    </form>
    <?php $this->load->view('admin/template/log'); ?>
</div>
