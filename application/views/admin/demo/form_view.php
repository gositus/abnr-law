<script>
	$(function(){
		$('#table-data').DataTable();
	});
</script>

<div class="row">

	<!-- NOTIFICATION -->
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="m-y-0">Notification</h4>
			</div>
			<div class="panel-body">
				<div class="form-horizontal">
					<div class="alert alert-success alert-icon-bg alert-dismissable">
						<div class="alert-icon">
							<i class="zmdi zmdi-check"></i>
						</div>
						<div class="alert-message">
							<button class="close" data-dismiss="alert">
								<span>
									<i class="zmdi zmdi-close"></i>
								</span>
							</button>
							You successfully read this important alert message.
						</div>
					</div>

					<div class="alert alert-info alert-icon-bg alert-dismissable">
						<div class="alert-icon">
							<i class="zmdi zmdi-info-outline"></i>
						</div>
						<div class="alert-message">
							<button class="close" data-dismiss="alert">
								<span>
									<i class="zmdi zmdi-close"></i>
								</span>
							</button>
							This alert needs your attention, but it's not super important.
						</div>
					</div>


					<div class="alert alert-warning alert-icon-bg alert-dismissable">
						<div class="alert-icon">
							<i class="zmdi zmdi-alert-triangle"></i>
						</div>
						<div class="alert-message">
							<button class="close" data-dismiss="alert">
								<span>
									<i class="zmdi zmdi-close"></i>
								</span>
							</button>
							Better check yourself, you're not looking too good.
						</div>
					</div>

					<div class="alert alert-danger alert-icon-bg alert-dismissable">
						<div class="alert-icon">
							<i class="zmdi zmdi-close-circle-o"></i>
						</div>
						<div class="alert-message">
							<button class="close" data-dismiss="alert">
								<span>
									<i class="zmdi zmdi-close"></i>
								</span>
							</button>
							Change a few things up and try submitting again.
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>


	<!-- MODAL -->
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="m-y-0">Modal</h4>
			</div>

			<div class="panel-body">
				<div class="col-md-2">
					<div class="btn btn-success m-w-120" data-toggle="modal" data-target="#successmodal1">Success</div>
					<div id="successmodal1" class="modal">
						<div class="modal-dialog">
							<div class="modal-content bg-success animated bounceIn">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">
										<span>
											<i class="zmdi zmdi-close"></i>
										</span>
									</button>
								</div>
								<div class="modal-body">
									<div class="text-center">
										<div>
											<i class="zmdi zmdi-check zmdi-hc-5x"></i>
										</div>
										<h3>Success</h3>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
										<br>Pellentesque lacinia non massa a euismod.</p>
										<div class="m-y-30">
											<button type="button" data-dismiss="modal" class="btn btn-default">Continue</button>
											<button type="button" data-dismiss="modal" class="btn btn-success">Close</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-2">
					<div class="btn btn-info m-w-120" data-toggle="modal" data-target="#infomodal1">Info</div>
					<div id="infomodal1" class="modal">
						<div class="modal-dialog">
							<div class="modal-content bg-info animated bounceIn">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">
										<span>
											<i class="zmdi zmdi-close"></i>
										</span>
									</button>
								</div>
								<div class="modal-body">
									<div class="text-center">
										<div>
											<i class="zmdi zmdi-help zmdi-hc-5x"></i>
										</div>
										<h3>Info</h3>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
										<br>Pellentesque lacinia non massa a euismod.</p>
										<div class="m-y-30">
											<button type="button" data-dismiss="modal" class="btn btn-default">Continue</button>
											<button type="button" data-dismiss="modal" class="btn btn-info">Close</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-2">
					<div class="btn btn-warning m-w-120" data-toggle="modal" data-target="#warningmodal1">Warning</div>
					<div id="warningmodal1" class="modal">
						<div class="modal-dialog">
							<div class="modal-content bg-warning animated bounceIn">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">
										<span>
											<i class="zmdi zmdi-close"></i>
										</span>
									</button>
								</div>
								<div class="modal-body">
									<div class="text-center">
										<div>
											<i class="zmdi zmdi-alert-circle-o zmdi-hc-5x"></i>
										</div>
										<h3>Warning</h3>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
										<br>Pellentesque lacinia non massa a euismod.</p>
										<div class="m-y-30">
											<button type="button" data-dismiss="modal" class="btn btn-default">Continue</button>
											<button type="button" data-dismiss="modal" class="btn btn-warning">Close</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-2">
					<div class="btn btn-danger m-w-120" data-toggle="modal" data-target="#dangermodal1">Danger</div>
					<div id="dangermodal1" class="modal">
						<div class="modal-dialog">
							<div class="modal-content bg-danger animated bounceIn">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">
										<span>
											<i class="zmdi zmdi-close"></i>
										</span>
									</button>
								</div>
								<div class="modal-body">
									<div class="text-center">
										<div>
											<i class="zmdi zmdi-alert-triangle zmdi-hc-5x"></i>
										</div>
										<h3>Danger</h3>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
										<br>Pellentesque lacinia non massa a euismod.</p>
										<div class="m-y-30">
											<button type="button" data-dismiss="modal" class="btn btn-default">Continue</button>
											<button type="button" data-dismiss="modal" class="btn btn-danger">Close</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- BUTTON -->
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="m-y-0">BUTTON</h4>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-lg-2 col-sm-4 col-xs-6 m-y-5">
						<button type="button" class="btn btn-default m-w-120">Default</button>
					</div>
					<div class="col-lg-2 col-sm-4 col-xs-6 m-y-5">
						<button type="button" class="btn btn-primary m-w-120">Primary</button>
					</div>
					<div class="col-lg-2 col-sm-4 col-xs-6 m-y-5">
						<button type="button" class="btn btn-success m-w-120">Success</button>
					</div>
					<div class="col-lg-2 col-sm-4 col-xs-6 m-y-5">
						<button type="button" class="btn btn-info m-w-120">Info</button>
					</div>
					<div class="col-lg-2 col-sm-4 col-xs-6 m-y-5">
						<button type="button" class="btn btn-warning m-w-120">Warning</button>
					</div>
					<div class="col-lg-2 col-sm-4 col-xs-6 m-y-5">
						<button type="button" class="btn btn-danger m-w-120">Danger</button>
					</div>
				</div>

				<hr>


			</div>
		</div>
	</div>

	<!-- TOAST -->
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="m-y-0">TOAST</h4>
			</div>
			<div class="panel-body">
				<div class="col-lg-2 col-sm-4 col-xs-6 m-y-5">
					<button type="button" class="btn btn-success m-w-120" id="toastSuccess">Success</button>
				</div>
				<div class="col-lg-2 col-sm-4 col-xs-6 m-y-5">
					<button type="button" class="btn btn-info m-w-120" id="toastInfo">Info</button>
				</div>
				<div class="col-lg-2 col-sm-4 col-xs-6 m-y-5">
					<button type="button" class="btn btn-warning m-w-120" id="toastWarning">Warning</button>
				</div>
				<div class="col-lg-2 col-sm-4 col-xs-6 m-y-5">
					<button type="button" class="btn btn-danger m-w-120" id="toastDanger">Danger</button>
				</div>
			</div>
		</div>
	</div>
	
	<script>
		$(function(){
			$("#toastSuccess").click(function() {
		        var type = 'success',
		            content = 'Inconceivable!',
		            title = "Success";
		        toastr[type](content, title);
		    });
		    $("#toastInfo").click(function() {
		        var type = 'info',
		            content = 'Inconceivable!',
		            title = "Info";
		        toastr[type](content, title);
		    });
		    $("#toastWarning").click(function() {
		        var type = 'warning',
		            content = 'Inconceivable!',
		            title = "Warning";
		        toastr[type](content, title);
		    });
		    $("#toastDanger").click(function() {
		        var type = 'error',
		            content = 'Inconceivable!',
		            title = "Error";
		        toastr[type](content, title);
		    });
		})
	</script>

	<!-- FORM -->
	<div class="col-md-8">

		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="m-y-0">Information</h4>
			</div>

			<div class="panel-body">
				<div class="form-horizontal">
					<div class="form-group">
						<label class="col-md-12">Form Input</label>
					</div>
					<div class="form-group">
						<label for="" class="col-md-3 control-label">Email</label>
						<div class="col-md-7">
							<input type="text" name="exemail" class="form-control" placeholder="Email">
						</div>
					</div>

					<div class="form-group">
						<label for="" class="col-md-3 control-label">Password</label>
						<div class="col-md-7">
							<input type="password" name="expassword" class="form-control" placeholder="Password">
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3">File Upload</label>
						<div class="col-md-7">
							<label class="btn btn-default file-upload-btn">
								Choose file...
								<input type="file" class="file-upload-input" name="file">
							</label>
							<p class="help-block">
								<small>Recommended size 200x200 px</small>
							</p>
						</div>
					</div>

					<div class="form-group">
						<label for="" class="col-md-3 control-label">Address</label>
						<div class="col-md-7">
							<textarea id="form-control-4" class="form-control" rows="3" placeholder="Address"></textarea>
						</div>
					</div>

					<hr>
					<div class="form-group">
						<label class="col-md-12">Select Box</label>
						<label class="control-label col-md-3">Select</label>
						<div class="col-md-7">
							<select name="" id="" class="custom-select">
								<option value="1">HTML</option>
								<option value="1">CSS</option>
								<option value="1">Bootstrap</option>
								<option value="1">JQUERY</option>
								<option value="1">PHP</option>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3">Select with Search</label>
						<div class="col-md-7">
							<select name="" id="" class="form-control" data-plugin="bs-select">
								<option value="1">HTML</option>
								<option value="1">CSS</option>
								<option value="1">Bootstrap</option>
								<option value="1">JQUERY</option>
								<option value="1">PHP</option>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3">Multiple Select</label>
						<div class="col-md-7">
							<select name="" id="" class="form-control" data-plugin="bs-select" multiple="multiple">
								<option value="1">HTML</option>
								<option value="1">CSS</option>
								<option value="1">Bootstrap</option>
								<option value="1">JQUERY</option>
								<option value="1">PHP</option>
							</select>
						</div>
					</div>

					<script>
						$('[data-plugin="bs-select"]').select2({
							theme : "bootstrap"
						});
					</script>

					<hr>


					<div class="form-group">
						<label class="col-md-12">Checkbox</label>
						<label class="col-md-3 control-label">Type 1</label>
						<div class="col-md-7">
							<div class="custom-controls-stacked">
								<label class="custom-control custom-control-default custom-checkbox">
									<input type="checkbox" class="custom-control-input" name="checkbox1" checked>
									<span class="custom-control-indicator"></span>
									<span class="custom-control-label">Default</span>
								</label>
								<label class="custom-control custom-control-primary custom-checkbox">
									<input type="checkbox" class="custom-control-input" name="checkbox1" checked>
									<span class="custom-control-indicator"></span>
									<span class="custom-control-label">Primary</span>
								</label>
								<label class="custom-control custom-control-success custom-checkbox">
									<input type="checkbox" class="custom-control-input" name="checkbox1" checked>
									<span class="custom-control-indicator"></span>
									<span class="custom-control-label">Success</span>
								</label>
								<label class="custom-control custom-control-info custom-checkbox">
									<input type="checkbox" class="custom-control-input" name="checkbox1" checked>
									<span class="custom-control-indicator"></span>
									<span class="custom-control-label">Info</span>
								</label>
								<label class="custom-control custom-control-warning custom-checkbox">
									<input type="checkbox" class="custom-control-input" name="checkbox1" checked>
									<span class="custom-control-indicator"></span>
									<span class="custom-control-label">Warning</span>
								</label>
								<label class="custom-control custom-control-danger custom-checkbox">
									<input type="checkbox" class="custom-control-input" name="checkbox1" checked>
									<span class="custom-control-indicator"></span>
									<span class="custom-control-label">Danger</span>
								</label>
							</div>
						</div>
					</div>


					<!-- <div class="form-group">
						<label class="col-md-3 control-label">Type 2</label>	
						<div class="col-md-7">
							<div class="btn-group" data-toggle="buttons">
								<label class="btn btn-default">
									<input type="checkbox" name="buttonCheckboxes" autocomplete="off"> PHP
								</label>
								<label class="btn btn-default">
									<input type="checkbox" name="buttonCheckboxes" autocomplete="off"> Ruby
								</label>
								<label class="btn btn-default">
									<input type="checkbox" name="buttonCheckboxes" autocomplete="off"> Python
								</label>
							</div>
						</div>
					</div> -->

					<div class="form-group">
						<label class="col-md-3 control-label">Type 2</label>
						<div class="col-md-7">
							<div class="switches-stacked">
								<label class="switch switch-primary">
									<input type="checkbox" class="s-input" name="checkbox3" checked>
									<span class="s-content">
										<span class="s-track"></span>
										<span class="s-handle"></span>
									</span>
								</label>
								<label class="switch switch-success">
									<input type="checkbox" class="s-input" name="checkbox3" checked>
									<span class="s-content">
										<span class="s-track"></span>
										<span class="s-handle"></span>
									</span>
								</label>
								<label class="switch switch-info">
									<input type="checkbox" class="s-input" name="checkbox3" checked>
									<span class="s-content">
										<span class="s-track"></span>
										<span class="s-handle"></span>
									</span>
								</label>
								<label class="switch switch-warning">
									<input type="checkbox" class="s-input" name="checkbox3" checked>
									<span class="s-content">
										<span class="s-track"></span>
										<span class="s-handle"></span>
									</span>
								</label>
								<label class="switch switch-danger">
									<input type="checkbox" class="s-input" name="checkbox3" checked>
									<span class="s-content">
										<span class="s-track"></span>
										<span class="s-handle"></span>
									</span>
								</label>
							</div>
						</div>
					</div>


					

				</div>
			</div>
		</div>
	</div>

	<div class="col-md-4">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="m-y-0">Status</h4>
			</div>

			<div class="panel-body">
				<div class="form-horizontal">
					<div class="form-group">
						<label for="" class="col-md-12">Type 1</label>
						<label class="control-label col-md-3">Status</label>
						<div class="col-md-7">
							<div class="custom-controls-stacked">
								<label class="custom-control custom-control-primary custom-radio">
									<input type="radio" name="status1" class="custom-control-input">
									<span class="custom-control-indicator"></span>
									<span class="custom-control-label">Active</span>
								</label>

								<label class="custom-control custom-control-danger custom-radio">
									<input type="radio" name="status1" class="custom-control-input" checked>
									<span class="custom-control-indicator"></span>
									<span class="custom-control-label">Inactive</span>
								</label>

								<label class="custom-control custom-control-default custom-radio">
									<input type="radio" name="status1" class="custom-control-input">
									<span class="custom-control-indicator"></span>
									<span class="custom-control-label">Delete</span>
								</label>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-12">Type 2</label>

						<label for="" class="col-md-3 control-label">Status</label>
						<div class="col-md-9">
							<div class="btn-group" data-toggle="buttons">
								<label class="btn btn-outline-primary active">
									<input type="radio" name="status2"> Active
								</label>
								<label class="btn btn-outline-primary">
									<input type="radio" name="status2"> Inactive
								</label>
								<label class="btn btn-outline-primary">
									<input type="radio" name="status2"> Delete
								</label>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<!-- DATA TABLE -->
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="m-y-0">DATA TABLE</h4>
			</div>
			<div class="panel-body">
				<div class="table-responsive">
				    <table class="table table-striped table-bordered dataTable" id="table-data">
				        <thead>
				            <tr>
				                <th>Rendering engine</th>
				                <th>Browser</th>
				                <th>Platform(s)</th>
				                <th>Engine version</th>
				                <th>CSS grade</th>
				            </tr>
				        </thead>
				        <tbody>
				            <tr>
				                <td>Trident</td>
				                <td>Internet Explorer 4.0</td>
				                <td>Win 95+</td>
				                <td>4</td>
				                <td>X</td>
				            </tr>
				            <tr>
				                <td>Trident</td>
				                <td>Internet Explorer 5.0</td>
				                <td>Win 95+</td>
				                <td>5</td>
				                <td>C</td>
				            </tr>
				            <tr>
				                <td>Trident</td>
				                <td>Internet Explorer 5.5</td>
				                <td>Win 95+</td>
				                <td>5.5</td>
				                <td>A</td>
				            </tr>
				            <tr>
				                <td>Trident</td>
				                <td>Internet Explorer 6</td>
				                <td>Win 98+</td>
				                <td>6</td>
				                <td>A</td>
				            </tr>
				            <tr>
				                <td>Trident</td>
				                <td>Internet Explorer 7</td>
				                <td>Win XP SP2+</td>
				                <td>7</td>
				                <td>A</td>
				            </tr>
				            <tr>
				                <td>Trident</td>
				                <td>AOL browser (AOL desktop)</td>
				                <td>Win XP</td>
				                <td>6</td>
				                <td>A</td>
				            </tr>
				            <tr>
				                <td>Gecko</td>
				                <td>Firefox 1.0</td>
				                <td>Win 98+ / OSX.2+</td>
				                <td>1.7</td>
				                <td>A</td>
				            </tr>
				            <tr>
				                <td>Gecko</td>
				                <td>Firefox 1.5</td>
				                <td>Win 98+ / OSX.2+</td>
				                <td>1.8</td>
				                <td>A</td>
				            </tr>
				            <tr>
				                <td>Gecko</td>
				                <td>Firefox 2.0</td>
				                <td>Win 98+ / OSX.2+</td>
				                <td>1.8</td>
				                <td>A</td>
				            </tr>
				            <tr>
				                <td>Gecko</td>
				                <td>Firefox 3.0</td>
				                <td>Win 2k+ / OSX.3+</td>
				                <td>1.9</td>
				                <td>A</td>
				            </tr>
				            <tr>
				                <td>Gecko</td>
				                <td>Camino 1.0</td>
				                <td>OSX.2+</td>
				                <td>1.8</td>
				                <td>A</td>
				            </tr>
				            <tr>
				                <td>Gecko</td>
				                <td>Camino 1.5</td>
				                <td>OSX.3+</td>
				                <td>1.8</td>
				                <td>A</td>
				            </tr>
				            <tr>
				                <td>Gecko</td>
				                <td>Netscape 7.2</td>
				                <td>Win 95+ / Mac OS 8.6-9.2</td>
				                <td>1.7</td>
				                <td>A</td>
				            </tr>
				            <tr>
				                <td>Gecko</td>
				                <td>Netscape Browser 8</td>
				                <td>Win 98SE+</td>
				                <td>1.7</td>
				                <td>A</td>
				            </tr>
				            <tr>
				                <td>Gecko</td>
				                <td>Netscape Navigator 9</td>
				                <td>Win 98+ / OSX.2+</td>
				                <td>1.8</td>
				                <td>A</td>
				            </tr>
				            <tr>
				                <td>Gecko</td>
				                <td>Mozilla 1.0</td>
				                <td>Win 95+ / OSX.1+</td>
				                <td>1</td>
				                <td>A</td>
				            </tr>
				            <tr>
				                <td>Gecko</td>
				                <td>Mozilla 1.1</td>
				                <td>Win 95+ / OSX.1+</td>
				                <td>1.1</td>
				                <td>A</td>
				            </tr>
				            <tr>
				                <td>Gecko</td>
				                <td>Mozilla 1.2</td>
				                <td>Win 95+ / OSX.1+</td>
				                <td>1.2</td>
				                <td>A</td>
				            </tr>
				            <tr>
				                <td>Gecko</td>
				                <td>Mozilla 1.3</td>
				                <td>Win 95+ / OSX.1+</td>
				                <td>1.3</td>
				                <td>A</td>
				            </tr>
				            <tr>
				                <td>Gecko</td>
				                <td>Mozilla 1.4</td>
				                <td>Win 95+ / OSX.1+</td>
				                <td>1.4</td>
				                <td>A</td>
				            </tr>
				            <tr>
				                <td>Gecko</td>
				                <td>Mozilla 1.5</td>
				                <td>Win 95+ / OSX.1+</td>
				                <td>1.5</td>
				                <td>A</td>
				            </tr>
				            <tr>
				                <td>Gecko</td>
				                <td>Mozilla 1.6</td>
				                <td>Win 95+ / OSX.1+</td>
				                <td>1.6</td>
				                <td>A</td>
				            </tr>
				            <tr>
				                <td>Gecko</td>
				                <td>Mozilla 1.7</td>
				                <td>Win 98+ / OSX.1+</td>
				                <td>1.7</td>
				                <td>A</td>
				            </tr>
				            <tr>
				                <td>Gecko</td>
				                <td>Mozilla 1.8</td>
				                <td>Win 98+ / OSX.1+</td>
				                <td>1.8</td>
				                <td>A</td>
				            </tr>
				            <tr>
				                <td>Gecko</td>
				                <td>Seamonkey 1.1</td>
				                <td>Win 98+ / OSX.2+</td>
				                <td>1.8</td>
				                <td>A</td>
				            </tr>
				            <tr>
				                <td>Gecko</td>
				                <td>Epiphany 2.20</td>
				                <td>Gnome</td>
				                <td>1.8</td>
				                <td>A</td>
				            </tr>
				            <tr>
				                <td>Webkit</td>
				                <td>Safari 1.2</td>
				                <td>OSX.3</td>
				                <td>125.5</td>
				                <td>A</td>
				            </tr>
				            <tr>
				                <td>Webkit</td>
				                <td>Safari 1.3</td>
				                <td>OSX.3</td>
				                <td>312.8</td>
				                <td>A</td>
				            </tr>
				            <tr>
				                <td>Webkit</td>
				                <td>Safari 2.0</td>
				                <td>OSX.4+</td>
				                <td>419.3</td>
				                <td>A</td>
				            </tr>
				            <tr>
				                <td>Webkit</td>
				                <td>Safari 3.0</td>
				                <td>OSX.4+</td>
				                <td>522.1</td>
				                <td>A</td>
				            </tr>
				            <tr>
				                <td>Webkit</td>
				                <td>OmniWeb 5.5</td>
				                <td>OSX.4+</td>
				                <td>420</td>
				                <td>A</td>
				            </tr>
				            <tr>
				                <td>Webkit</td>
				                <td>iPod Touch / iPhone</td>
				                <td>iPod</td>
				                <td>420.1</td>
				                <td>A</td>
				            </tr>
				            <tr>
				                <td>Webkit</td>
				                <td>S60</td>
				                <td>S60</td>
				                <td>413</td>
				                <td>A</td>
				            </tr>
				            <tr>
				                <td>Presto</td>
				                <td>Opera 7.0</td>
				                <td>Win 95+ / OSX.1+</td>
				                <td>-</td>
				                <td>A</td>
				            </tr>
				            <tr>
				                <td>Presto</td>
				                <td>Opera 7.5</td>
				                <td>Win 95+ / OSX.2+</td>
				                <td>-</td>
				                <td>A</td>
				            </tr>
				            <tr>
				                <td>Presto</td>
				                <td>Opera 8.0</td>
				                <td>Win 95+ / OSX.2+</td>
				                <td>-</td>
				                <td>A</td>
				            </tr>
				            <tr>
				                <td>Presto</td>
				                <td>Opera 8.5</td>
				                <td>Win 95+ / OSX.2+</td>
				                <td>-</td>
				                <td>A</td>
				            </tr>
				            <tr>
				                <td>Presto</td>
				                <td>Opera 9.0</td>
				                <td>Win 95+ / OSX.3+</td>
				                <td>-</td>
				                <td>A</td>
				            </tr>
				            <tr>
				                <td>Presto</td>
				                <td>Opera 9.2</td>
				                <td>Win 88+ / OSX.3+</td>
				                <td>-</td>
				                <td>A</td>
				            </tr>
				            <tr>
				                <td>Presto</td>
				                <td>Opera 9.5</td>
				                <td>Win 88+ / OSX.3+</td>
				                <td>-</td>
				                <td>A</td>
				            </tr>
				            <tr>
				                <td>Presto</td>
				                <td>Opera for Wii</td>
				                <td>Wii</td>
				                <td>-</td>
				                <td>A</td>
				            </tr>
				            <tr>
				                <td>Presto</td>
				                <td>Nokia N800</td>
				                <td>N800</td>
				                <td>-</td>
				                <td>A</td>
				            </tr>
				            <tr>
				                <td>Presto</td>
				                <td>Nintendo DS browser</td>
				                <td>Nintendo DS</td>
				                <td>8.5</td>
				                <td>C/A
				                    <sup>1</sup>
				                </td>
				            </tr>
				            <tr>
				                <td>KHTML</td>
				                <td>Konqureror 3.1</td>
				                <td>KDE 3.1</td>
				                <td>3.1</td>
				                <td>C</td>
				            </tr>
				            <tr>
				                <td>KHTML</td>
				                <td>Konqureror 3.3</td>
				                <td>KDE 3.3</td>
				                <td>3.3</td>
				                <td>A</td>
				            </tr>
				            <tr>
				                <td>KHTML</td>
				                <td>Konqureror 3.5</td>
				                <td>KDE 3.5</td>
				                <td>3.5</td>
				                <td>A</td>
				            </tr>
				            <tr>
				                <td>Tasman</td>
				                <td>Internet Explorer 4.5</td>
				                <td>Mac OS 8-9</td>
				                <td>-</td>
				                <td>X</td>
				            </tr>
				            <tr>
				                <td>Tasman</td>
				                <td>Internet Explorer 5.1</td>
				                <td>Mac OS 7.6-9</td>
				                <td>1</td>
				                <td>C</td>
				            </tr>
				            <tr>
				                <td>Tasman</td>
				                <td>Internet Explorer 5.2</td>
				                <td>Mac OS 8-X</td>
				                <td>1</td>
				                <td>C</td>
				            </tr>
				            <tr>
				                <td>Misc</td>
				                <td>NetFront 3.1</td>
				                <td>Embedded devices</td>
				                <td>-</td>
				                <td>C</td>
				            </tr>
				            <tr>
				                <td>Misc</td>
				                <td>NetFront 3.4</td>
				                <td>Embedded devices</td>
				                <td>-</td>
				                <td>A</td>
				            </tr>
				            <tr>
				                <td>Misc</td>
				                <td>Dillo 0.8</td>
				                <td>Embedded devices</td>
				                <td>-</td>
				                <td>X</td>
				            </tr>
				            <tr>
				                <td>Misc</td>
				                <td>Links</td>
				                <td>Text only</td>
				                <td>-</td>
				                <td>X</td>
				            </tr>
				            <tr>
				                <td>Misc</td>
				                <td>Lynx</td>
				                <td>Text only</td>
				                <td>-</td>
				                <td>X</td>
				            </tr>
				            <tr>
				                <td>Misc</td>
				                <td>IE Mobile</td>
				                <td>Windows Mobile 6</td>
				                <td>-</td>
				                <td>C</td>
				            </tr>
				            <tr>
				                <td>Misc</td>
				                <td>PSP browser</td>
				                <td>PSP</td>
				                <td>-</td>
				                <td>C</td>
				            </tr>
				            <tr>
				                <td>Other browsers</td>
				                <td>All others</td>
				                <td>-</td>
				                <td>-</td>
				                <td>U</td>
				            </tr>
				        </tbody>
				        <tfoot>
				        <tr>
				            <th>Rendering engine</th>
				            <th>Browser</th>
				            <th>Platform(s)</th>
				            <th>Engine version</th>
				            <th>CSS grade</th>
				        </tr>
				        </tfoot>
				    </table>
				</div>
			</div>
		</div>
	</div>
</div>



<!-- <div class="form-group upload-image">
    <label class="control-label col-md-3">File Upload</label>
    <div class="col-md-7">
        <div class="row">
            <div class="col-md-4 col-sm-12 col-xs-12">
                <label class="btn btn-default file-upload-btn">
                    Choose file...
                    <input type="file" class="file-upload-input" name="file">
                </label>
            </div>
            <div class="col-md-8 col-sm-12 col-xs-12">
                <div class="wrap-info-path">
                    <input type="text" class="info-path-image form-control" disabled>
                </div>
            </div>
        </div>
        <p class="help-block">
            <small>Recommended size <?php echo $this->width . ' * ' . $this->height ?> px</small>
        </p>
        <div class="wrap-preview-image">
            <img src="" class="upload-preview">
        </div>
    </div>
</div> -->