<div class="row">
    <form method="post" id="content-form" action="<?php echo base_url('goadmin/' . $this->url . '/view/' . $row[$default]['id']) ?>" data-toggle="validator" enctype="multipart/form-data">
        <div class="col-md-12">
            <?php $this->load->view('admin/template/fixed_heading', array('type' => 'view', 'name' => $row[$default]['name'])); ?>
        </div>
        <input type="hidden" name="id" id="row_id" data-table-name="<?php echo $this->url; ?>" value="<?php echo $row[$default]['id'] ?>">


        <?php if (validation_errors()) { ?>
            <!-- SHOW ERROR -->
            <?php echo viewErrorValidation(); ?>
            <!-- END SHOW ERROR -->
        <?php } ?>


        <div class="col-md-8 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="m-y-0 font-bold">Information</h4>
                </div>


                <div class="panel-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label for="type" class="control-label col-md-3">Type</label>
                            <div class="col-md-7">
                                <select name="type" class="form-control profile-type required" required>
                                    <option value="">--</option>
                                    <option value="1" <?php echo ($row[$default]['type'] == 1) ? 'selected' : '' ?>>Partners</option>
                                    <option value="7" <?php echo ($row[$default]['type'] == 7) ? 'selected' : '' ?>>Senior Associates</option>
                                    <option value="2" <?php echo ($row[$default]['type'] == 2) ? 'selected' : '' ?>>Associates</option>
                                    <option value="3" <?php echo ($row[$default]['type'] == 3) ? 'selected' : '' ?>>Foreign Counsel</option>
                                    <option value="4" <?php echo ($row[$default]['type'] == 4) ? 'selected' : '' ?>>Trainee Associate</option>
                                    <option value="8" <?php echo ($row[$default]['type'] == 8) ? 'selected' : '' ?>>Assistant Lawyer</option>
                                    <option value="5" <?php echo ($row[$default]['type'] == 5) ? 'selected' : '' ?>>Of Counsel</option>
                                    <option value="6" <?php echo ($row[$default]['type'] == 6) ? 'selected' : '' ?>>Counsel</option>
                                </select>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="name" class="control-label col-md-3">Name </label>
                            <div class="col-md-7">
                                <input type="text" name="name" id="name" class="form-control required" required value="<?php echo $row[$default]['name']; ?>">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>


                        <!--<div class="form-group">-->
                        <!--    <label for="position" class="control-label col-md-3">Position</label>-->
                        <!--    <div class="col-md-7">-->
                        <!--        <input type="text" name="position" id="position" class="form-control required" required value="<?php echo $row[$default]['position'] ?>">-->
                        <!--        <div class="help-block with-errors"></div>-->
                        <!--    </div>-->
                        <!--</div>-->

                        <div class="form-group hide-profile">
                            <label for="url" class="control-label col-md-3">Phone</label>
                            <div class="col-md-7">
                                <input type="text" name="phone" id="phone" maxlength="20" class="form-control " value="<?php echo $row[$default]['phone'] ?>">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="form-group hide-profile">
                            <label for="email" class="control-label col-md-3">Email</label>
                            <div class="col-md-7">
                                <input type="text" name="email" id="email" class="email profile-required form-control required" value="<?php echo $row[$default]['email'] ?>">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="sort" class="control-label col-md-3">Sort</label>
                            <div class="col-md-7">
                                <input type="number" name="sort" id="sort" class="number form-control" value="<?php echo $row[$default]['sort']; ?>" readonly>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="hide-profile">
                            <?php $x = 0;
                            foreach (language()->result_array() as $lang) : ?>
                                <div class="language lang-<?php echo $lang['attr'] ?>" <?php if ($x == 0) echo ' style="display:block"'; ?>>
                                    <div class="form-group">
                                        <label for="file_<?php echo $lang['id'] ?>" class="control-label col-md-3">File </label>
                                        <div class="col-md-7">
                                            <input type="hidden" name="current_file_<?php echo $lang['id'] ?>" id="current_file_<?php echo $lang['id'] ?>" value="<?php echo $row[$lang['id']]['file'] ?>">
                                            <input type="file" name="file_<?php echo $lang['id'] ?>" id="file_<?php echo $lang['id'] ?>" class="form-control" accept=".pdf,.docx,doc,.xls,.xlsx">
                                            <div class="help-block with-errors"></div>
                                            <div class="row file">
                                                <div class="col-md-9">
                                                    <div class="wrap-preview-file <?php echo ($row[$lang['id']]['file']) ? 'show' : ''; ?>">
                                                        <label class="control-label"><a href="<?php echo base_url('files/profile/' . $row[$lang['id']]['file']) ?>"><?php echo $row[$lang['id']]['file'] ?></a></label>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="wrap-delete-image <?php echo ($row[$lang['id']]['file']) ? 'show' : ''; ?>">
                                                        <button type="button" data-field_id="profile_id" data-id="<?php echo $row[$default]['id'] ?>" data-table_name="content_to_profile" data-field="file" data-lang_id="<?php echo $lang['id'] ?>" class="btn btn-danger btn-delete-file"><i class="zmdi zmdi-close"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <?php $x++;
                            endforeach; ?>
                        </div>
                        <div class="form-group">
                            <label for="join_date" class="control-label col-md-3">Join Date</label>
                            <div class="col-md-7">
                                <input type="text" name="join_date" class="required form-control datepicker" id="join_date" <?php if($row[$default]['join_date'] != '0000-00-00 00:00:00'){?> value="<?php echo format_date($row[$default]['join_date']);  ?>" <?php } ?> readonly>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <?php if($row[$default]['type'] == 1 || $row[$default]['type'] == 7){ ?>
                        <div class="form-group">
                            <label for="promotion_date" class="control-label col-md-3">Promotion Date</label>
                            <div class="col-md-7">
                                <div class="row">
                                    <div class="col-md-10">
                                              <input type="text" name="promotion_date" class="form-control datepicker" id="promotion_date" <?php if(!empty($row[$default]['promotion_date']) && $row[$default]['promotion_date'] != '0000-00-00'){?> value="<?php echo format_date($row[$default]['promotion_date']);  ?>" <?php } ?> readonly>
                                             <div class="help-block with-errors"></div>
                                    </div>
                                        <div class="col-md-2">
                                           <button type="button" data-toggle="tooltip" data-placement="bottom" title="Delete Promotion Date" class="btn btn-danger btn-delete-promotion_date"><i class="zmdi zmdi-close"></i></button>
                                        </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                        <div class="form-group upload-image hide-profile">
                            <label class="control-label col-md-3">Image Banner</label>
                            <div class="col-md-7">
                                <div class="row">
                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                        <label class="btn btn-default file-upload-btn">
                                            Choose file...
                                            <button class="file-upload-input" name="icon" id="ckfinder-upload" type="button"></button>
                                        </label>
                                    </div>
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div class="wrap-info-path">
                                            <input type="text" class="info-path-image show form-control" readonly id="finder-image" name="image" value="<?php echo $row[$default]['image']; ?>">
                                            <input type="file" name="image" style="display: none;" accept="image/*">
                                        </div>
                                    </div>
                                </div>
                                <p class="help-block">
                                    <small>Recommended size <?php echo $this->image_width . ' * ' . $this->image_height; ?> px</small>
                                </p>
                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="wrap-preview-image <?php echo ($row[$default]['image']) ? 'show' : ''; ?>">
                                            <img src="<?php echo base_url('lib/images/profile/') . $row[$default]['image']; ?>" class="upload-preview">
                                        </div>
                                    </div>
                                    <!-- <div class="col-md-3">
                                                 <div class="wrap-delete-image <?php echo ($row[$lang['id']]['image']) ? 'show' : ''; ?>">
                                                     <button type="button" class="btn btn-danger btn-delete-img"><i class="zmdi zmdi-close"></i></button>
                                                 </div>
                                             </div> -->
                                </div>
                            </div>
                        </div>

                        <div class="form-group upload-image hide-profile">
                            <label class="control-label col-md-3">Image Profile</label>
                            <div class="col-md-7">
                                <div class="row">
                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                        <label class="btn btn-default file-upload-btn">
                                            Choose file...
                                            <button class="file-upload-input" name="image_profile" id="ckfinder-upload" type="button"></button>
                                        </label>
                                    </div>
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div class="wrap-info-path">
                                            <input type="text" class="info-path-image show form-control" readonly id="finder-image" name="image_profile" value="<?php echo $row[$default]['profile_image']; ?>">
                                            <input type="file" name="image_profile" style="display: none;" accept="image/*">
                                        </div>
                                    </div>
                                </div>
                                <p class="help-block">
                                    <small>Recommended size <?php echo $this->image_profile_width . ' * ' . $this->image_profile_height; ?> px</small>
                                </p>
                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="wrap-preview-image <?php echo ($row[$default]['profile_image']) ? 'show' : ''; ?>">
                                            <img src="<?php echo base_url('lib/images/profile/') . $row[$default]['profile_image']; ?>" class="upload-preview">
                                        </div>
                                    </div>
                                    <!-- <div class="col-md-3">
                                                 <div class="wrap-delete-image <?php echo ($row[$default]['profile_image']) ? 'show' : ''; ?>">
                                                     <button type="button" class="btn btn-danger btn-delete-img"><i class="zmdi zmdi-close"></i></button>
                                                 </div>
                                             </div> -->
                                </div>
                            </div>
                        </div>

                        <?php $x = 0; foreach (language()->result_array() as $lang) : ?>
                            <div class="language lang-<?php echo $lang['attr']?>" <?php if ($x == 0) echo ' style="display:block"'; ?> >
                                <div class="form-group">
                                    <label for="professional_membership_<?php echo $lang['id']?>" class="control-label col-md-3">Professional Membership </label>
                                    <div class="col-md-7">
                                        <textarea name="professional_membership_<?php echo $lang['id']?>" id="professional_membership_<?php echo $lang['id']?>" class="form-control" ><?php echo $row[$lang['id']]['professional_membership']; ?></textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="citizenship_<?php echo $lang['id']?>" class="control-label col-md-3">Citizenship </label>
                                    <div class="col-md-7">
                                        <textarea name="citizenship_<?php echo $lang['id']?>" id="citizenship_<?php echo $lang['id']?>" class="form-control" ><?php echo $row[$lang['id']]['citizenship']; ?></textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="language_<?php echo $lang['id']?>" class="control-label col-md-3">Language </label>
                                    <div class="col-md-7">
                                        <textarea name="language_<?php echo $lang['id']?>" id="language_<?php echo $lang['id']?>" class="form-control" ><?php echo $row[$lang['id']]['language']; ?></textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="form-group gohide">
                                    <label for="experience_<?php echo $lang['id']?>" class="control-label col-md-3">Experience </label>
                                    <div class="col-md-7">
                                        <textarea name="experience_<?php echo $lang['id']?>" id="experience_<?php echo $lang['id']?>" class="form-control" ><?php echo $row[$lang['id']]['experience']; ?></textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>



                            </div>
                            <?php $x++; endforeach; ?>

                        <?php /*/ $x = 0;
                            foreach (language()->result_array() as $lang) : ?>
                             <div class="language lang-<?php echo $lang['attr'] ?>" <?php if ($x == 0) echo 'style="display:block"'; ?>>
                                 <div class="form-group upload-image">
                                     <label class="control-label col-md-3">Image Right</label>
                                     <div class="col-md-7">
                                         <div class="row">
                                             <div class="col-md-4 col-sm-12 col-xs-12">
                                                 <label class="btn btn-default file-upload-btn">
                                                     Choose file...
                                                     <button class="file-upload-input" name="icon" id="ckfinder-upload-left" type="button"></button>
                                                 </label>
                                             </div>
                                             <div class="col-md-8 col-sm-12 col-xs-12">
                                                 <div class="wrap-info-path">
                                                     <input type="file" name="image_right_<?php echo $lang['id'] ?>" style="display: none;">
                                                     <input type="text" class="info-path-image show form-control" readonly id="finder-image-right-<?php echo $lang['id'] ?>" name="image_right_<?php echo $lang['id'] ?>" value="<?php echo  $row[$lang['id']]['image_right']; ?>">
                                                 </div>
                                             </div>
                                         </div>
                                         <p class="help-block">
                                             <small>Recommended size <?php echo $this->image_width_right . ' * ' . $this->image_height_right; ?> px</small>
                                         </p>
                                         <div class="row">
                                             <div class="col-md-9">
                                                 <div class="wrap-preview-image <?php echo ($row[$lang['id']]['image_right']) ? 'show' : ''; ?>">
                                                     <img src="<?php echo base_url('lib/images/profile/') . $row[$lang['id']]['image_right']; ?>" class="upload-preview">
                                                 </div>
                                             </div>
                                             <!-- <div class="col-md-3">
                                                 <div class="wrap-delete-image <?php echo ($row[$lang['id']]['image_right']) ? 'show' : ''; ?>">
                                                     <button type="button" class="btn btn-danger btn-delete-img"><i class="zmdi zmdi-close"></i></button>
                                                 </div>
                                             </div> -->
                                         </div>
                                     </div>
                                 </div>
                             </div>
                         <?php $x++;
                            endforeach; /*/ ?>
                    </div>
                </div>
            </div>
            <div class="panel panel-default award gohide">
                <div class="panel-heading">
                    <h4 class="m-y-0 font-bold">Award</h4>
                </div>
                <div class="panel-body">
                    <div class="form-horizontal">
                        <div class="award-append">
                            <?php if (!empty($award_data)) { ?>
                                <?php foreach ($award_data as $key => $val) {
                                    ?>

                                    <div class="form-group stage">
                                        <div class="row award-close">
                                            <input type="hidden" name="award_id[]" value="<?php echo $val[$default]['id'] ?>">
                                            <input type="hidden" name="count[]" value="count">
                                            <label for="type" class="control-label col-md-2 stage-label">Data <?php echo $key + 1 ?></label>
                                            <div class="col-md-8">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <label class="control-label"><a href="<?php echo base_url('files/profile/award/' . $val[$default]['file']) ?>"><?php echo $val[$default]['file'] ?></a></label>
                                                        <div class="help-block with-errors"></div>
                                                    </div>

                                                    <?php $x = 0;
                                                    foreach (language()->result_array() as $lang) : ?>
                                                        <div class="language lang-<?php echo $lang['attr'] ?>" <?php if ($x == 0) echo ' style="display:block"'; ?>>
                                                            <div class="col-md-6">
                                                                <input type="text" name="award_name_<?php echo $lang['id'] ?>[]" class="form-control profile-required required" value="<?php echo $val[$lang['id']]['name'] ?>" placeholder="Title">
                                                                <div class="help-block with-errors"></div>
                                                            </div>
                                                        </div>
                                                        <?php $x++;
                                                    endforeach; ?>

                                                    <div class="col-md-12">
                                                        <input type="text" name="award_url[]" class="form-control url" value="<?php echo $val[$default]['url'] ?>" placeholder="Award Url">
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <a href="javascript:" data-id="<?php echo base_url('goadmin/profile/delete_award/' . $val[$default]['id']) ?>" class="btn btn-danger remove-stage"><i class="zmdi zmdi-close"></i></a>
                                            </div>
                                        </div>

                                    </div>
                                <?php }
                            } ?>
                        </div>

                        <div class="action text-right">
                            <a id="add-award" href="javascript:" class="btn btn-primary m-y-5">Add Award</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-12 col-xs-12">
            <?php $this->load->view('admin/template/view_flag'); ?>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 hide-profile">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="m-y-0 font-bold">Content</h4>
                </div>
                <div class="panel-body">
                    <div class="form-horizontal">
                        <?php $x = 0;
                        foreach (language()->result_array() as $lang) : ?>
                            <div class="language lang-<?php echo $lang['attr'] ?>" <?php if ($x == 0) echo 'style="display:block"'; ?>>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <textarea class="form-control content" name="content_<?php echo $lang['id'] ?>" rows="10" id="content_<?php echo $lang['id'] ?>"><?php echo $row[$lang['id']]['content']; ?> </textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <?php $x++;
                        endforeach; ?>

                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="csrf-token">
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="csrf-token">
    </form>
    <?php $this->load->view('admin/template/log'); ?>
</div>
<div class="gohide">
    <div id="master_clone" class="form-group stage">
        <div class="row award-close">
            <label for="type" class="control-label col-md-2 stage-label">Data 1</label>
            <input type="hidden" name="count[]" value="count">
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-6">
                        <input type="file" name="award_file[]" class="form-control required" value="" accept=".jpg,.png,.jpeg" placeholder="Award File">
                        <div class="help-block with-errors"></div>
                    </div>
                    <?php $x = 0;
                    foreach (language()->result_array() as $lang) : ?>
                        <div class="col-md-6 language lang-<?php echo $lang['attr'] ?>" <?php if ($x == 0) echo ' style="display:block"'; ?>>
                            <div class="">
                                <input type="text" name="award_name_<?php echo $lang['id'] ?>[]" class="form-control required" value="" placeholder="Title">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <?php $x++;
                    endforeach; ?>

                    <div class="col-md-12">
                        <input type="text" name="award_url[]" class="form-control url" value="" placeholder="Award Url">
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


<script>
    $(document).ready(function () {
        $(".profile-type").change();
    });
    $('.btn-delete-promotion_date').click(function(event){
        $('input[name="promotion_date"]').val("");
    })
    $('#add-award').click(function (event) {
        var clone = $('#master_clone').clone();
        clone.removeAttr('id');
        clone.find('.stage-label').text('Data ' + (parseInt($('.award .stage').length) + 1));
        clone.find('.finder-image').attr('name', 'photo[]');
        clone.find('.finder-image').val('');
        clone.find('.upload-preview').remove();
        var removeImageHtml = '\
            <div class="col-md-2">\
                <a href="javascript:;" class="btn btn-danger remove-stage"><i class="zmdi zmdi-close"></i></a>\
            </div>';
        clone.find('.award-close').append(removeImageHtml);
        $('.award-append').append(clone);
    });

    $('.award').on('click', '.remove-stage', function (event) {
        event.preventDefault();
        $(this).parents('.stage').remove();
        var id = $(this).data('id');

        if (id != undefined) {
            $.ajax({
                url: id,
                success: function (data) {
                }
            });
        }
    });
</script>