<div class="row">
    <form method="post" id="content-form" action="<?php echo base_url('goadmin/' . $this->url . '/add') ?>" data-toggle="validator" enctype="multipart/form-data">

        <div class="col-md-12">
            <?php $this->load->view('admin/template/fixed_heading', array('type' => 'add')); ?>
        </div>

        <?php if (validation_errors()) { ?>
            <!-- SHOW ERROR -->
            <?php echo viewErrorValidation(); ?>
            <!-- END SHOW ERROR -->
        <?php } ?>

        <div class="col-md-8 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="m-y-0 font-bold">Information</h4>
                </div>


                <div class="panel-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label for="type" class="control-label col-md-3">Type</label>
                            <div class="col-md-7">
                                <select id="type" name="type" class="form-control profile-type required" required>
                                    <option value="">--</option>
                                    <option value="1" <?php echo ($data_profile == 1) ? 'selected' : '' ?>>Partners</option>
                                    <option value="7" <?php echo ($data_profile == 7) ? 'selected' : '' ?>>Senior Associates</option>
                                    <option value="2" <?php echo ($data_profile == 2) ? 'selected' : '' ?>>Associates</option>
                                    <option value="3" <?php echo ($data_profile == 3) ? 'selected' : '' ?>>Foreign Counsel</option>
                                    <option value="4" <?php echo ($data_profile == 4) ? 'selected' : '' ?>>Trainee Associate</option>
                                    <option value="8" <?php echo ($data_profile == 8) ? 'selected' : '' ?>>Assistant Lawyer</option>
                                    <option value="5" <?php echo ($data_profile == 5) ? 'selected' : '' ?>>Of Counsel</option>
                                    <option value="6" <?php echo ($data_profile == 6) ? 'selected' : '' ?>>Counsel</option>
                                </select>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="name" class="control-label col-md-3">Name </label>
                            <div class="col-md-7">
                                <input type="text" name="name" id="name" class="form-control required" required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>


                        <!--<div class="form-group">-->
                        <!--    <label for="position" class="control-label col-md-3">Position</label>-->
                        <!--    <div class="col-md-7">-->
                        <!--        <input type="text" name="position" id="position" class="form-control required" required>-->
                        <!--        <div class="help-block with-errors"></div>-->
                        <!--    </div>-->
                        <!--</div>-->

                        <div class="form-group hide-profile">
                            <label for="url" class="control-label col-md-3">Phone</label>
                            <div class="col-md-7">
                                <input type="text" name="phone" id="phone" maxlength="20" class="form-control ">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="form-group hide-profile">
                            <label for="email" class="control-label col-md-3">Email</label>
                            <div class="col-md-7">
                                <input type="text" name="email" id="email" class="email profile-required form-control required">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="sort" class="control-label col-md-3">Sort</label>
                            <div class="col-md-7">
                                <input type="number" name="sort" id="sort" class="number form-control" value="" readonly>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="hide-profile ">
                            <?php $x = 0;
                            foreach (language()->result_array() as $lang) : ?>
                                <div class="language lang-<?php echo $lang['attr'] ?>" <?php if ($x == 0) echo ' style="display:block"'; ?>>
                                    <div class="form-group">
                                        <label for="file_<?php echo $lang['id'] ?>" class="control-label col-md-3">File </label>
                                        <div class="col-md-7">
                                            <input type="file" name="file_<?php echo $lang['id'] ?>" id="file_<?php echo $lang['id'] ?>" class="form-control" accept=".pdf,.docx,doc,.xls,.xlsx">
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                </div>
                                <?php $x++;
                            endforeach; ?>
                        </div>
                        <div class="form-group">
                            <label for="join_date" class="control-label col-md-3">Join Date</label>
                            <div class="col-md-7">
                                <input type="text" name="join_date" class="required form-control datepicker" id="join_date" readonly>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <?php if($data_profile == 1 || $data_profile == 7){ ?>
                         <div class="form-group">
                            <label for="promotion_date" class="control-label col-md-3">Promotion Date</label>
                            <div class="col-md-7">
                                <input type="text" name="promotion_date" class="form-control datepicker" id="promotion_date"readonly>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <?php } ?>
                        <div class="form-group upload-image hide-profile">
                            <label class="control-label col-md-3">Image Banner</label>
                            <div class="col-md-7">
                                <div class="row">
                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                        <label class="btn btn-default file-upload-btn">
                                            Choose file...
                                            <button class="file-upload-input" name="icon" id="ckfinder-upload" type="button"></button>
                                        </label>
                                    </div>
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div class="wrap-info-path">
                                            <input type="text" class="info-path-image profile-required show form-control <?php echo ($data_profile!=4)?"required":"" ?>" readonly id="finder-image" name="image">
                                            <input type="file" name="image" style="display: none;" accept="image/*">
                                        </div>
                                    </div>
                                </div>
                                <p class="help-block">
                                    <small>Recommended size <?php echo $this->image_width . ' * ' . $this->image_height; ?> px</small>
                                </p>
                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="wrap-preview-image show">
                                            <img src="" class="upload-preview">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group upload-image hide-profile">
                            <label class="control-label col-md-3">Image Profile</label>
                            <div class="col-md-7">
                                <div class="row">
                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                        <label class="btn btn-default file-upload-btn">
                                            Choose file...
                                            <button class="file-upload-input" name="icon" id="ckfinder-upload" type="button"></button>
                                        </label>
                                    </div>
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div class="wrap-info-path">
                                            <input type="text" class="info-path-image profile-required show form-control <?php echo ($data_profile!=4)?"required":"" ?>" readonly id="finder-image" name="image_profile">
                                            <input type="file" name="image_profile" style="display: none;" accept="image/*">
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                </div>
                                <p class="help-block">
                                    <small>Recommended size <?php echo $this->image_profile_width . ' * ' . $this->image_profile_height; ?> px</small>
                                </p>
                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="wrap-preview-image show">
                                            <img src="" class="upload-preview">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php $x = 0; foreach (language()->result_array() as $lang) : ?>
                            <div class="language lang-<?php echo $lang['attr']?>" <?php if ($x == 0) echo ' style="display:block"'; ?> >
                                <div class="form-group">
                                    <label for="professional_membership_<?php echo $lang['id']?>" class="control-label col-md-3">Professional Membership </label>
                                    <div class="col-md-7">
                                        <textarea name="professional_membership_<?php echo $lang['id']?>" id="professional_membership_<?php echo $lang['id']?>" class="form-control" ></textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="citizenship_<?php echo $lang['id']?>" class="control-label col-md-3">Citizenship </label>
                                    <div class="col-md-7">
                                        <textarea name="citizenship_<?php echo $lang['id']?>" id="citizenship_<?php echo $lang['id']?>" class="form-control" ></textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="language_<?php echo $lang['id']?>" class="control-label col-md-3">Language </label>
                                    <div class="col-md-7">
                                        <textarea name="language_<?php echo $lang['id']?>" id="language_<?php echo $lang['id']?>" class="form-control" ></textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="form-group gohide">
                                    <label for="experience_<?php echo $lang['id']?>" class="control-label col-md-3">Experience </label>
                                    <div class="col-md-7">
                                        <textarea name="experience_<?php echo $lang['id']?>" id="experience_<?php echo $lang['id']?>" class="form-control" ></textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <?php $x++; endforeach; ?>

                        <?php /*/ $x = 0;
                        foreach (language()->result_array() as $lang) : ?>
                            <div class="language lang-<?php echo $lang['attr'] ?>" <?php if ($x == 0) echo 'style="display:block"'; ?>>
                                <div class="form-group upload-image">
                                    <label class="control-label col-md-3">Image Right</label>
                                    <div class="col-md-7">
                                        <div class="row">
                                            <div class="col-md-4 col-sm-12 col-xs-12">
                                                <label class="btn btn-default file-upload-btn">
                                                    Choose file...
                                                    <button class="file-upload-input" name="icon" id="ckfinder-upload-right" type="button"></button>
                                                </label>
                                            </div>
                                            <div class="col-md-8 col-sm-12 col-xs-12">
                                                <div class="wrap-info-path">
                                                    <input type="file" name="image_right_<?php echo $lang['id'] ?>" style="display: none;">
                                                    <input type="text" class="info-path-image  show form-control required" readonly id="finder-image-right-<?php echo $lang['id'] ?>" name="image_right_<?php echo $lang['id'] ?>">
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <p class="help-block">
                                            <small>Recommended size <?php echo $this->image_width_right . ' * ' . $this->image_height_right; ?> px</small>
                                        </p>
                                        <div class="row">
                                            <div class="col-md-9">
                                                <div class="wrap-preview-image show">
                                                    <img src="" class="upload-preview">
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        <?php $x++;
                        endforeach; /*/ ?>

                    </div>
                </div>
            </div>
            <div class="panel panel-default award gohide">
                <div class="panel-heading">
                    <h4 class="m-y-0 font-bold">
                        Awards Data</h4>
                </div>
                <div class="panel-body">
                    <div class="form-horizontal">
                        <div class="award-append">
                        </div>

                        <div class="action text-right">
                            <a id="add-award" href="javascript:" class="btn btn-primary m-y-5">Add Award </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-12 col-xs-12">
            <?php $this->load->view('admin/template/add_flag'); ?>
            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="csrf-token">
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 hide-profile">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="m-y-0 font-bold">Content</h4>
                </div>
                <div class="panel-body">
                    <div class="form-horizontal">
                        <?php $x = 0;
                        foreach (language()->result_array() as $lang) : ?>
                            <div class="language lang-<?php echo $lang['attr'] ?>" <?php if ($x == 0) echo 'style="display:block"'; ?>>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <textarea class="form-control content" name="content_<?php echo $lang['id'] ?>" rows="10" id="content_<?php echo $lang['id'] ?>"></textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <?php $x++;
                        endforeach; ?>

                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="csrf-token">
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="gohide">
    <div id="master_clone" class="form-group stage">
        <div class="row award-close">
            <label for="type" class="control-label col-md-2 stage-label">Data 1</label>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-6">
                        <input type="file" name="award_file[]" class="form-control required" value="" accept=".jpg,.png,.jpeg" placeholder="Award File">
                        <div class="help-block with-errors"></div>
                    </div>
                    <?php $x = 0;
                    foreach (language()->result_array() as $lang) : ?>
                        <div class="col-md-6 language lang-<?php echo $lang['attr'] ?>" <?php if ($x == 0) echo ' style="display:block"'; ?>>
                            <div class="">
                                <input type="text" name="award_name_<?php echo $lang['id'] ?>[]" class="form-control required" value="" placeholder="Title">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <?php $x++;
                    endforeach; ?>

                    <div class="col-md-12">
                        <input type="text" name="award_url[]" class="form-control url" value="" placeholder="Award Url">
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $(".profile-type").change();
    });
    $('#add-award').click(function (event) {
        var clone = $('#master_clone').clone();
        clone.removeAttr('id');
        clone.find('.stage-label').text('Data ' + (parseInt($('.award .stage').length) + 1));
        clone.find('.finder-image').attr('name', 'photo[]');
        clone.find('.finder-image').val('');
        clone.find('.upload-preview').remove();
        var removeImageHtml = '\
                <a href="javascript:;" class="btn btn-danger remove-stage"><i class="zmdi zmdi-close"></i></a>';
        clone.find('.award-close').append(removeImageHtml);
        $('.award-append').append(clone);
    });

    $('.award').on('click', '.remove-stage', function (event) {
        event.preventDefault();
        $(this).parents('.stage').remove();
        var id = $(this).data('id');

        if (id != undefined) {
            $.ajax({
                url: id,
                success: function (data) {
                }
            });
        }
    });

    $("#type").on('change', function () {
        $.ajax({
            url: "<?php echo base_url('goadmin/profile/get_sort/') ?>" + $(this).val(),
            dataType: "json",
            success: function (data) {
                $('#sort').val(parseInt(data['sort']) + 1);
            }
        });
    });
    window.addEventListener('load', function () {
        $.ajax({
            url: "<?php echo base_url('goadmin/profile/get_sort/') ?>" + $('#type').val(),
            dataType: "json",
            success: function (data) {
                $('#sort').val(parseInt(data['sort']) + 1);
            }
        });
    });

</script>