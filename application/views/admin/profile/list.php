<?php if ($this->session->flashdata('success')) { ?>
	<script>
		$(function() {
			showToast('success', 'bottom', "<?php echo $this->session->flashdata('success'); ?>");
		})
	</script>
<?php } ?>
<style type="text/css">
	#list-table>thead>tr>th:nth-child(3), #list-table>tbody>tr>td:nth-child(3){
		display: none;
	}
</style>
<div class="row">
	<div class="col-md-12">
		<?php $this->load->view('admin/template/fixed_heading', array('type' => 'list')); ?>
	</div>

	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="m-y-0 font-bold">List <?php echo $title; ?></h4>
				<div class="error-input"><?php echo validation_errors(); ?></div>
			</div>
			<div class="panel-body">

				<select name="type" class="table_search form-control input-sm gohide" data-index="2">
					<option value="">-- Sort By Type --</option>
					<option value="1" <?php echo ($data_profile==1)?'selected':'' ?> >Partners</option>
                    <option value="7" <?php echo ($data_profile==2)?'selected':'' ?>>Senior Associates</option>
                    <option value="2" <?php echo ($data_profile==3)?'selected':'' ?>>Associates</option>
                    <option value="3" <?php echo ($data_profile==4)?'selected':'' ?>>Foreign Counsel</option>
                    <option value="4" <?php echo ($data_profile==5)?'selected':'' ?>>Assistant Lawyer</option>
                    <option value="5" <?php echo ($data_profile==6)?'selected':'' ?>>Of Counsel</option>
                    <option value="6" <?php echo ($data_profile==7)?'selected':'' ?>>Counsel</option>
				</select>
				

				<div class="form-horizontal">
					<div class="table-responsive">

						<table id="list-table" data-table-name="<?php echo $url; ?>" class="table table-striped dataTable">
							<thead>
								<tr>
									<th>No</th>
									<th>Name</th>
									<th>ID_type</th>
									<th>Type</th>
									<th>Image</th>
									<th>Join Date</th>
									<th>Order</th>
									<?php $this->load->view('admin/template/list_table_heading') ?>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
    // window.addEventListener('load', function () {
    //     $( ".table_search" ).change();
    // });

	function order_up(id) {
		$.ajax({
			url: "<?php echo base_url() ?>" + "/goadmin/profile/order_up/" + id,
			success: function(html) {
				$('#list-table').DataTable().ajax.reload();
			}
		});
	}

	function order_down(id) {
		$.ajax({
			url: "<?php echo base_url() ?>" + "/goadmin/profile/order_down/" + id,
			success: function(html) {
				$('#list-table').DataTable().ajax.reload();
			}
		});
	}
</script>
<?php
//SHOW DATA TO DATATABLE
// ====================================================================================
$showData = array(

	//NAMA FIELD BERDASARKAN ATTRIBUTE YG ADA PADA TABLE
	'field' => array(
		'name',
		'hide_type',
		'type',
		'image',
		'join_date',
		'sort',
	),

	// ================================================================================
	// CUSTOM CONTENT (KOSONGKAN BILA TIDAK DIPAKAI)
	// ROW = HASIL OBJECT DARI AJAX 
	// (CONTOH: UNTUK MENGAMBIL VALUE DARI TABLE (row.icon))
	// CONTOH PENGGUNAAN : 'icon'  => '<img src="\'+base_url+\'lib/images/\'+row.icon+\'"/>',
	// row.icon = "icon" NAMA ATTRIBUTE TABEL. YG DIRUBAH HANYALAH ATTRIBUTE TABLE!!
	// ================================================================================
	'custom_content' => array(
		'hide_type' => 'return row.type;',
		'type' => 'if(row.type==1) return "Partners"; else if(row.type==7) return "Senior Associates"; else if(row.type==2) return "Associates"; else if(row.type==3) return "Foreign Counsel"; else if(row.type==4) return "Trainee Associate"; else if(row.type==8) return "Assistant Lawyer"; else if(row.type==5) return "Of Counsel"; else if(row.type==6) return "Counsel";',
		'image' => '
				var strg = row.image;
				// var oke = str.split(".");
				// var strg = oke[0] + "_thumb." + oke[1];
				if(strg!=""){
				return "<img src=" +base_url+ "lib/images/profile/" +strg+ " / width=100>";
				}else{
					return "No image"
				}
			',
		'join_date' => 'if(row.join_date!=null){
		            var date = new Date(row.join_date.replace(/-/g, "/"));
					var monthNames = ["Jan", "Feb", "Mar","Apr", "May", "Jun", "Jul","Aug", "Sep", "Oct","Nov", "Dec"];
					var day = date.getDate();
					var monthIndex = date.getMonth();
					var year = date.getFullYear();
					return day + " " + monthNames[monthIndex] + " " + year;
					}else{
					    return "-";
					}',
		'sort'	=> '
					var order="";
					if(row.first==0){
						order+= "<a href=\"javascript:;\" onclick=\"order_up("+row.id+")\" data-id=\""+row.id+"\" style=\"display: block;\"><i class=\"fa fa-2x fa-caret-up\"></i></a>";
					}
					if(row.last==0){
						order+= "<a href=\"javascript:;\" onclick=\"order_down("+row.id+")\" data-id=\""+row.id+"\" style=\"display: block;\"><i class=\"fa fa-2x fa-caret-down\"></i></a>";
					}

					if(row.first==1 && row.last==1){
						return ""
					}else{
						return order
					}'


	)
);
?>
<?php $this->load->view('admin/template/list_table_data', $showData); ?>