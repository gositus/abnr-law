<div class="row">
    <form method="post" id="content-form" action="<?php echo base_url('goadmin/' . $this->url . '/add') ?>" data-toggle="validator" enctype="multipart/form-data">

        <div class="col-md-12">
            <?php $this->load->view('admin/template/fixed_heading', array('type' => 'add')); ?>
        </div>

        <?php if (validation_errors()) { ?>
            <!-- SHOW ERROR -->
            <?php echo viewErrorValidation(); ?>
            <!-- END SHOW ERROR -->
        <?php } ?>

        <div class="col-md-8 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="m-y-0 font-bold">Information</h4>
                </div>
                <div class="panel-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label for="name" class="control-label col-md-3">Name </label>
                            <div class="col-md-7">
                                <input type="text" name="name" id="name" class="form-control required" required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="year" class="control-label col-md-3">Year</label>
                            <div class="col-md-7">
                                <input type="number" name="year" id="year" class="number form-control required" value="">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="url" class="control-label col-md-3">URL</label>
                            <div class="col-md-7">
                                <input type="text" name="url" id="url" class="url form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="sort" class="control-label col-md-3">Sort</label>
                            <div class="col-md-7">
                                <input type="number" name="sort" id="sort" class="number form-control" value="">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="sort_home" class="control-label col-md-3">Logo Order</label>
                            <div class="col-md-7">
                                <select name="sort_home" class="form-control required" required>
                                    <option value="0">-Hide-</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                </select>
                                <p class="help-block">
                                    <small>Home page</small>
                                </p>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="affiliate" class="control-label col-md-3">Affiliate</label>
                            <div class="col-md-7">
                                <input type="checkbox" value="1" name="affiliate" id="affiliate">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="form-group upload-image">
                            <label class="control-label col-md-3">Image</label>
                            <div class="col-md-7">
                                <div class="row">
                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                        <label class="btn btn-default file-upload-btn">
                                            Choose file...
                                            <button class="file-upload-input" name="icon" id="ckfinder-upload" type="button"></button>
                                        </label>
                                    </div>
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div class="wrap-info-path">
                                            <input type="text" class="info-path-image  show form-control" readonly id="finder-image" name="image">
                                            <input type="file" name="image" style="display: none;" accept="image/*">
                                        </div>
                                    </div>
                                </div>
                                <p class="help-block">
                                    <small id="general-size">Recommended size <?php echo $this->image_width . ' * ' . $this->image_height; ?> px</small>
                                    <small id="affiliations-size" style="display: none;">Recommended size <?php echo $this->image_width_affiliations . ' * ' . $this->image_height_affiliations; ?> px</small>
                                </p>
                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="wrap-preview-image show">
                                            <img src="" class="upload-preview">
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>


                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-4 col-sm-12 col-xs-12">
            <?php $this->load->view('admin/template/add_flag'); ?>
        </div>
        <div class="col-sm-12 col-md-5">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="m-y-0 font-bold">Practice Area</h4>
                </div>
                <div class="panel-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="custom-controls-stacked">
                                    <?php foreach ($practice as $value){ ?>
                                        <label class="custom-control custom-control-primary custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" value="<?php echo $value['id'] ?>" name="practice[]">
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-label"><?php echo $value['name'] ?></span>
                                        </label>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="m-y-0 font-bold">Profile Lawyer</h4>
                </div>
                <div class="panel-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="custom-controls-stacked profile-column">
                                    <?php foreach ($profile as $value){ ?>
                                        <label class="custom-control custom-control-primary custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" value="<?php echo $value['id'] ?>" name="profile[]">
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-label"><?php echo $value['name'] ?></span>
                                        </label>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="m-y-0 font-bold">Content</h4>
                </div>
                <div class="panel-body">
                    <div class="form-horizontal">
                        <?php $x = 0;
                        foreach (language()->result_array() as $lang) : ?>
                            <div class="language lang-<?php echo $lang['attr'] ?>" <?php if ($x == 0) echo 'style="display:block"'; ?>>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <textarea class="form-control content" name="content_<?php echo $lang['id'] ?>" rows="10" id="content_<?php echo $lang['id'] ?>"></textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <?php $x++;
                        endforeach; ?>

                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="csrf-token">
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<script>
    $("#affiliate").change(function () {

        if (this.checked) {
            $('#general-size').hide();
            $('#affiliations-size').show();
        } else {
            $('#general-size').show();
            $('#affiliations-size').hide();
        }
    });
</script>