 <div class="row">
     <form method="post" id="content-form" action="<?php echo base_url('goadmin/' . $this->url . '/view/' . $row[$default]['id']) ?>" data-toggle="validator" enctype="multipart/form-data">
         <div class="col-md-12">
             <?php $this->load->view('admin/template/fixed_heading', array('type' => 'view', 'name' => $row[$default]['name'])); ?>
         </div>
         <input type="hidden" name="id" id="row_id" data-table-name="<?php echo $this->url; ?>" value="<?php echo $row[$default]['id'] ?>">


         <?php if (validation_errors()) { ?>
             <!-- SHOW ERROR -->
             <?php echo viewErrorValidation(); ?>
             <!-- END SHOW ERROR -->
         <?php } ?>


         <div class="col-md-8 col-sm-12 col-xs-12">
             <div class="panel panel-default">
                 <div class="panel-heading">
                     <h4 class="m-y-0 font-bold">Information</h4>
                 </div>


                 <div class="panel-body">
                     <div class="form-horizontal">


                         <?php $x = 0;
                            foreach (language()->result_array() as $lang) : ?>
                             <div class="language lang-<?php echo $lang['attr'] ?>" <?php if ($x == 0) echo 'style="display:block"'; ?>>
                                 <div class="form-group">
                                     <label for="name_<?php echo $lang['id'] ?>" class="control-label col-md-3">Title </label>
                                     <div class="col-md-7">
                                         <input type="text" name="name_<?php echo $lang['id'] ?>" id="name_<?php echo $lang['id'] ?>" value="<?php echo ($row[$lang['id']]['name']) ?>" class="form-control required" required>
                                         <div class="help-block with-errors"></div>
                                     </div>
                                 </div>

                             </div>
                         <?php $x++;
                            endforeach; ?>


                         <?php $x = 0;
                            foreach (language()->result_array() as $lang) : ?>
                             <div class="language lang-<?php echo $lang['attr'] ?>" <?php if ($x == 0) echo ' style="display:block"'; ?>>
                                 <div class="form-group">
                                     <label for="file_<?php echo $lang['id'] ?>" class="control-label col-md-3">File </label>
                                     <div class="col-md-7">
                                         <input type="hidden" name="current_file_<?php echo $lang['id'] ?>" id="current_file_<?php echo $lang['id'] ?>" value="<?php echo $row[$lang['id']]['file'] ?>">
                                         <input type="file" name="file_<?php echo $lang['id'] ?>" id="file_<?php echo $lang['id'] ?>" class="form-control" accept=".pdf,.docx,doc,.xls,.xlsx">
                                         <div class="help-block with-errors"></div>
                                         <div class="row file">
                                             <div class="col-md-9">
                                                 <div class="wrap-preview-file <?php echo ($row[$lang['id']]['file']) ? 'show' : ''; ?>">
                                                     <label class="control-label"><a href="<?php echo base_url('files/practice/' . $row[$lang['id']]['file']) ?>"><?php echo $row[$lang['id']]['file'] ?></a></label>
                                                 </div>
                                             </div>
                                             <div class="col-md-3">
                                                 <div class="wrap-delete-image <?php echo ($row[$lang['id']]['file']) ? 'show' : ''; ?>">
                                                     <button type="button" data-field_id="practice_id" data-id="<?php echo $row[$default]['id'] ?>" data-table_name="content_to_practice" data-field="file" data-lang_id="<?php echo $lang['id'] ?>" class="btn btn-danger btn-delete-file"><i class="zmdi zmdi-close"></i></button>
                                                 </div>
                                             </div>
                                         </div>
                                     </div>
                                 </div>

                             </div>
                         <?php $x++;
                            endforeach; ?>

                         <?php $x = 0;
                            foreach (language()->result_array() as $lang) : ?>
                             <div class="language lang-<?php echo $lang['attr'] ?>" <?php if ($x == 0) echo 'style="display:block"'; ?>>
                                 <div class="form-group upload-image">
                                     <label class="control-label col-md-3">Image</label>
                                     <div class="col-md-7">
                                         <div class="row">
                                             <div class="col-md-4 col-sm-12 col-xs-12">
                                                 <label class="btn btn-default file-upload-btn">
                                                     Choose file...
                                                     <button class="file-upload-input" name="icon" id="ckfinder-upload" type="button"></button>
                                                 </label>
                                             </div>
                                             <div class="col-md-8 col-sm-12 col-xs-12">
                                                 <div class="wrap-info-path">
                                                     <input type="text" class="info-path-image show form-control" readonly id="finder-image-<?php echo $lang['id'] ?>" name="image_<?php echo $lang['id'] ?>" value="<?php echo  $row[$lang['id']]['image']; ?>">
                                                     <input type="file" name="image_<?php echo $lang['id'] ?>" style="display: none;" accept="image/*">
                                                 </div>
                                             </div>
                                         </div>
                                         <p class="help-block">
                                             <small>Recommended size <?php echo $this->image_width . ' * ' . $this->image_height; ?> px</small>
                                         </p>
                                         <div class="row">
                                             <div class="col-md-9">
                                                 <div class="wrap-preview-image <?php echo ($row[$lang['id']]['image']) ? 'show' : ''; ?>">
                                                     <img src="<?php echo base_url('lib/images/practice/') . $row[$lang['id']]['image']; ?>" class="upload-preview">
                                                 </div>
                                             </div>
                                             <!-- <div class="col-md-3">
                                                 <div class="wrap-delete-image <?php echo ($row[$lang['id']]['image']) ? 'show' : ''; ?>">
                                                     <button type="button" class="btn btn-danger btn-delete-img"><i class="zmdi zmdi-close"></i></button>
                                                 </div>
                                             </div> -->
                                         </div>
                                     </div>
                                 </div>
                             </div>
                         <?php $x++;
                            endforeach; ?>

                         <?php $x = 0;
                            foreach (language()->result_array() as $lang) : ?>
                             <div class="language lang-<?php echo $lang['attr'] ?>" <?php if ($x == 0) echo 'style="display:block"'; ?>>
                                 <div class="form-group upload-image">
                                     <label class="control-label col-md-3">Image Right</label>
                                     <div class="col-md-7">
                                         <div class="row">
                                             <div class="col-md-4 col-sm-12 col-xs-12">
                                                 <label class="btn btn-default file-upload-btn">
                                                     Choose file...
                                                     <button class="file-upload-input" name="icon" id="ckfinder-upload-left" type="button"></button>
                                                 </label>
                                             </div>
                                             <div class="col-md-8 col-sm-12 col-xs-12">
                                                 <div class="wrap-info-path">
                                                     <input type="text" class="info-path-image show form-control" readonly id="finder-image-right-<?php echo $lang['id'] ?>" name="image_right_<?php echo $lang['id'] ?>" value="<?php echo  $row[$lang['id']]['image_right']; ?>">
                                                     <input type="file" name="image_right_<?php echo $lang['id'] ?>" style="display: none;" accept="image/*">
                                                 </div>
                                             </div>
                                         </div>
                                         <p class="help-block">
                                             <small>Recommended size <?php echo $this->image_width_right . ' * ' . $this->image_height_right; ?> px</small>
                                         </p>
                                         <div class="row">
                                             <div class="col-md-9">
                                                 <div class="wrap-preview-image <?php echo ($row[$lang['id']]['image_right']) ? 'show' : ''; ?>">
                                                     <img src="<?php echo base_url('lib/images/practice/') . $row[$lang['id']]['image_right']; ?>" class="upload-preview">
                                                 </div>
                                             </div>
                                             <!-- <div class="col-md-3">
                                                 <div class="wrap-delete-image <?php echo ($row[$lang['id']]['image_right']) ? 'show' : ''; ?>">
                                                     <button type="button" class="btn btn-danger btn-delete-img"><i class="zmdi zmdi-close"></i></button>
                                                 </div>
                                             </div> -->
                                         </div>
                                     </div>
                                 </div>
                             </div>
                         <?php $x++;
                            endforeach; ?>

                     </div>
                 </div>
             </div>
         </div>
         <div class="col-md-4 col-sm-12 col-xs-12">
             <?php $this->load->view('admin/template/view_flag'); ?>
         </div>
         <div class="col-md-12 col-sm-12 col-xs-12">
             <div class="panel panel-default">
                 <div class="panel-heading">
                     <h4 class="m-y-0 font-bold">Content</h4>
                 </div>
                 <div class="panel-body">
                     <div class="form-horizontal">
                         <?php $x = 0;
                            foreach (language()->result_array() as $lang) : ?>
                             <div class="language lang-<?php echo $lang['attr'] ?>" <?php if ($x == 0) echo 'style="display:block"'; ?>>
                                 <div class="form-group">
                                     <div class="col-md-12">
                                         <textarea class="form-control content" name="content_<?php echo $lang['id'] ?>" rows="10" id="content_<?php echo $lang['id'] ?>"><?php echo $row[$lang['id']]['content']; ?> </textarea>
                                         <div class="help-block with-errors"></div>
                                     </div>
                                 </div>
                             </div>
                         <?php $x++;
                            endforeach; ?>

                         <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="csrf-token">
                     </div>
                 </div>
             </div>
         </div>
     </form>
     <?php $this->load->view('admin/template/log'); ?>
 </div>