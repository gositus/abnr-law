 <?php if($this->session->flashdata('success')) { ?>
    <script>
        $(function(){
            showToast('success', 'bottom', "<?php echo $this->session->flashdata('success');?>");
        })
    </script>
<?php } ?>
 <div class="row">
    <form method="post" id="content-form" action="<?php echo base_url('goadmin/'.$this->url.'/doku')?>" data-toggle="validator" enctype="multipart/form-data">
        <div class="col-md-12">
            <?php $this->load->view('admin/template/fixed_heading', array('type' => 'view', 'name' => 'Doku Setting')); ?>
        </div>
        <input type="hidden" name="id" id="row_id" data-table-name="<?php echo $this->url;?>" value="1">

        
        <?php if(validation_errors()){ ?>
            <!-- SHOW ERROR -->
            <?php echo viewErrorValidation();?>
            <!-- END SHOW ERROR -->
        <?php } ?>

        
                <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="m-y-0 font-bold">Information</h4>
                </div>


                <div class="panel-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label for="mode" class="control-label col-md-3">Mode</label>
                            <div class="col-md-7">
                                <select name="mode" id="mode" class="form-control required" required >
                                    <option value="development" <?php if(setting_value('doku_mode')=='development') echo 'selected'; ?> >Development</option>
                                    <option value="production" <?php if(setting_value('doku_mode')=='production') echo 'selected'; ?> >Production</option>
                                    
                                </select>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="mall_id" class="control-label col-md-3">Mall ID</label>
                            <div class="col-md-7">
                                <input type="text" name="mall_id" id="mall_id" class="form-control required" required  autocomplete="off" value="<?php echo setting_value('doku_mallid');?>">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="shared_key" class="control-label col-md-3">Shared Key</label>
                            <div class="col-md-7">
                                <input type="text" name="shared_key" id="shared_key" class="form-control required" required  autocomplete="off" value="<?php echo setting_value('doku_sharedkey');?>">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="stagging" class="control-label col-md-3">Development</label>
                            <div class="col-md-7">
                                <input type="text" class="form-control "   autocomplete="off" value="http://staging.doku.com/Suite/Receive" disabled readonly>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="production" class="control-label col-md-3">Production</label>
                            <div class="col-md-7">
                                <input type="text" class="form-control "   autocomplete="off" value="https://pay.doku.com/Suite/Receive" disabled readonly>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-12 col-xs-12">
            <?php //$this->load->view('admin/template/view_flag');?>
        </div>
        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="csrf-token">
    </form>
    <?php $this->load->view('admin/template/log'); ?>
</div>

