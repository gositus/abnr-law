<div class="banner">
	<div class="container">
		<div class="row align-items-end no-gutters news-index caption-banner">
			<div class="col-12 col-sm-12 col-md-12 col-lg-8">
				<img class="w-100 banner-left news-index" src="<?php echo base_url('lib/images/page/' . $page['image']) ?>" alt="<?php echo $page['name'] ?>">
			</div>
			<div class="col-12 col-sm-12 col-md-12 col-lg-4">
				<!-- <img class="w-100 banner-right" src="<?php echo base_url('lib/images/page/') . $page['image_right'] ?>" alt="<?php echo $page['name'] ?>"> -->
				<div class="banner-right">
					<h1><?php echo html_entity_decode($page['heading']) ?></h1>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="news-index knowledge">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="title"><?php echo ucfirst($this->lang->line('news')) ?></h1>
                <div class="news-lists">
                    <?php foreach ($news as $value): ?>
                        <div class="col-list">
                            <a href="<?php echo base_url('news/'.$value['seo_url']) ?>">
                                <figure>
                                    <img src="<?php echo (empty($value['banner']))?base_url('lib/images/news/default.png'):base_url('lib/images/news/'.$value['banner']) ?>" alt="<?php echo $value['name'] ?>">
                                </figure>
                                <figcaption>
                                    <div class="date"><?php echo format_date($value['start']) ?></div>
                                    <h5><?php echo $value['name'] ?></h5>
                                </figcaption>
                            </a>
                        </div>
                    <?php endforeach; ?>

                </div>
                <div class="see-all text-center text-uppercase"><a href="<?php echo base_url('news') ?>">See more >></a></div>
            </div>
        </div>
    </div>
</div>

<div class="publication knowledge">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
                <h1 class="title"><?php echo ucwords($this->lang->line('publication')) ?></h1>
				<div class="publication-lists">
                    <?php foreach ($publications as $key => $value) { ?>
                        <div class="media">
                            <?php if (!empty($value['image'])) { ?>
                                <a target="_blank" href="<?php echo base_url('files/document/' . $value['file']) ?>">
                                    <img src="<?php echo base_url('lib/images/document/' . $value['image']) ?>" alt="<?php echo $value['name'] ?>">
                                </a>

                            <?php } else { ?>
                                <a target="_blank" href="<?php echo base_url('files/document/' . $value['file']) ?>">
                                    <img src="<?php echo base_url('lib/images/document/default.jpg') ?>" alt="<?php echo $value['name'] ?>">
                                </a>
                            <?php } ?>

                            <div class="media-body">
                                <a target="_blank" href="<?php echo base_url('files/document/' . $value['file']) ?>">
                                    <h5><?php echo $value['name'] ?></h5>
                                </a>
                                [<?php echo filesize_formatted(FCPATH . 'files/document/' . $value['file']) ?>]
                                <?php echo format_date($value["publish_date"]) ?><br>
                                Author: <?php echo $value['author'] ?><br>
                                Publisher: <?php echo $value['publisher'] ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <div class="see-all text-center text-uppercase"><a href="<?php echo base_url('publication') ?>">See more >></a></div>
			</div>
		</div>
	</div>
</div>