<div class="banner">
	<div class="container">
		<div class="row align-items-end no-gutters news-index caption-banner">
			<div class="col-12 col-sm-12 col-md-12 col-lg-8">
				<img class="w-100 banner-left news-index" src="<?php echo base_url('lib/images/page/' . $page['image']) ?>" alt="<?php echo $page['name'] ?>">
			</div>
			<div class="col-12 col-sm-12 col-md-12 col-lg-4">
				<!-- <img class="w-100 banner-right" src="<?php echo base_url('lib/images/page/') . $page['image_right'] ?>" alt="<?php echo $page['name'] ?>"> -->
				<div class="banner-right">
					<h1><?php echo $page['heading'] ?></h1>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="news-index">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="news-lists">
                    <?php foreach ($news as $value): ?>
					<div class="col-list">
						<a href="<?php echo base_url('news/'.$value['seo_url']) ?>">
							<figure>
								<img src="<?php echo (empty($value['banner']))?base_url('lib/images/news/default.png'):base_url('lib/images/news/'.$value['banner']) ?>" alt="<?php echo $value['name'] ?>">
							</figure>
							<figcaption>
								<div class="date"><?php echo format_date($value['start']) ?></div>
								<h5><?php echo $value['name'] ?></h5>
							</figcaption>
						</a>
					</div>
                    <?php endforeach; ?>
				</div>
				<div class="see-all text-uppercase">
					<a href="<?php echo base_url('news-archive') ?>">See archive >></a>
				</div>
			</div>
		</div>
	</div>
</div>



<div class="mid-content py-4 d-none">
	<div class="container-lg">
		<div class="row">
			<div class="col-12 col-sm-3 col-md-4 col-lg-4 text-sm-right pr-sm-0">
				<div class="description pr-0 pr-sm-3">
					<div class="row">
						<div class="col-sm-12 col-md-5 col-lg-5">
							<h2 class="small-title"><strong>ARCHIVES</strong></h2>
						</div>
						<div class="col-sm-12 col-md-7 col-lg-7">
							<div class="row">
								<div class="col col-md-5">
									<ul class="list-columns lh-min d-none d-sm-block">
										<?php foreach ($news_year as $value) { ?>
											<li><a href="<?php echo base_url('news/' . $value) ?>"><?php echo ($year == $value) ? '<font color="#000000"><strong>' . $value . '</strong></font>' : $value ?></a></li>
										<?php } ?>
									</ul>
									<div class="dropdown d-block d-sm-none">
										<button class="btn btn-secondary dropdown-toggle btn-block" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											<?php echo $year ?>
										</button>
										<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
											<?php foreach ($news_year as $value) { ?>
												<a class="dropdown-item" href="<?php echo base_url('news/' . $value) ?>"><?php echo $value ?></a>
											<?php } ?>
										</div>
									</div>
								</div>
								<div class="col col-md-7">
									<ul class="list-columns lh-min d-none d-sm-block">
										<?php foreach ($news_month as $value) { ?>
											<li><a href="<?php echo base_url('news/' . $year . '/' . $value) ?>"><?php echo ($month == $value) ? '<font color="#000000"><strong>' . DateTime::createFromFormat('!m', $value)->format('F') . '</strong></font>' : DateTime::createFromFormat('!m', $value)->format('F') ?></a></li>
										<?php } ?>
									</ul>
									<div class="dropdown d-block d-sm-none">
										<button class="btn btn-secondary dropdown-toggle btn-block" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											<?php echo (!empty($month)) ? $month : 'All' ?>
										</button>
										<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
											<?php foreach ($news_month as $value) { ?>
												<a class="dropdown-item" href="<?php echo base_url('news/' . $year . '/' . $value) ?>"><?php echo DateTime::createFromFormat('!m', $value)->format('F') ?></a>
											<?php } ?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
			<div class="col-12 col-sm-8 col-md-6">
				<div class="description">
					<div class="side-right">
						<?php foreach ($news as $value) { ?>
							<div class="news-item">
								<a href="<?php echo base_url('news/' . $value['seo_url']) ?>">
									<h4><b><?php echo $value['name'] ?></b></h4>
								</a>
								<span><?php echo format_date($value['start']) ?></span>
                                <?php echo str_replace('&nbsp;','',character_limiter(strip_tags($value['content']), 400)) ?>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>