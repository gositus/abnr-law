<div class="banner">
	<div class="container-lg">
		<div class="row align-items-end no-gutters caption-banner">
			<div class="col-8">
				<img class="w-100 banner-left" src="<?php echo base_url('lib/images/page/' . $page['image']) ?>" alt="<?php echo $page['name'] ?>">
			</div>
			<div class="col-4">
				<img class="w-100 banner-right" src="<?php echo base_url('lib/images/page/') . $page['image_right'] ?>" alt="<?php echo $page['name'] ?>">
			</div>
		</div>
	</div>
</div>
<div class="mid-content py-4">
	<div class="container-lg">
		<div class="row justify-content-center">

			<div class="col-12 col-sm-12 col-md-9">
				<div class="row">
					<div class="col-12 col-sm-6 col-md-12">
						<div class="description">
							<h5 class="title mb-2"><?php echo strtoupper($this->lang->line('assistant_law')) ?></h5>
							<ul class="list-columns two-columns pl-0">
								<?php foreach ($related as $value) { ?>
									<li><a class="no-hover"><?php echo strtoupper($value['name']) ?></a></li>
								<?php } ?>
							</ul>
						</div>
					</div>

				</div>
			</div>

		</div>
	</div>
</div>