<div class="banner">
	<div class="container">
		<div class="row align-items-end no-gutters caption-banner">
            <?php if(empty($news['image'])){ ?>
			<div class="col-8">
				<img class="w-100 banner-left" src="<?php echo (!empty($news['image'])) ? base_url('lib/images/news/') . $news['image'] : base_url('lib/images/page/' . $page['image']) ?>" alt="<?php echo $news['name'] ?>">
			</div>
			<div class="col-4">
				<img class="w-100 banner-right" src="<?php echo base_url('lib/images/page/') . $page['image_right'] ?>" alt="<?php echo $page['name'] ?>">
			</div>
            <?php }else{ ?>
                <div class="col-12">
                    <img class="w-100 banner-left" src="<?php echo (!empty($news['image'])) ? base_url('lib/images/news/') . $news['image'] : base_url('lib/images/page/' . $page['image']) ?>" alt="<?php echo $news['name'] ?>">
                </div>
            <?php } ?>
		</div>
	</div>
</div>

<div class="news-detail">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<div class="news-detail__left">
					<span class="date"><?php echo format_date($news['start']) ?></span>
					<h5><?php echo $news['name'] ?></h5>
					<?php echo $news['content'] ?>
						<?php if (!empty($news['file'])) { ?>
							<a class="target_link" href="<?php echo base_url('files/news/' . $news['file']) ?>" target="_blank">
								<i class="fal fa-file-pdf"></i> <span>DOWNLOAD FILE</span>
							</a>
					<?php } ?>
				</div>
			</div>
			<div class="col-md-4">
				<div class="news-detail__right">
					<h5>More Legal Updates</h5>
					<ul>
                        <?php foreach ($related_news as $value){ ?>
						<li>
							<a href="<?php echo base_url('news/'.$value['seo_url']) ?>">
								<span class="date"><?php echo format_date($value['start']) ?></span>
                                <?php echo $value['name'] ?>
							</a>
						</li>
						<?php } ?>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="mid-content py-4 d-none">
	<div class="container-lg">
		<div class="row">

			<div class="col-sm-3 col-md-4 col-lg-4 text-sm-right">
				<div class="description pr-3">
					<div class="row">
						<div class="col-6 col-sm-12 col-md-6 col-lg-6">
							<h2 class="small-title"><strong>NEWS DETAIL</strong></h2>
						</div>
						<div class="col-6 col-sm-12 col-md-6 col-lg-6 text-right text-sm-left">
							<a href="<?php echo base_url('news') ?>">Back</a>
						</div>
					</div>

				</div>
			</div>
			<div class="col-sm-8 col-md-6">
				<div class="description">
					<div class="side-right">
						<p>
							<?php echo format_date($news['start']) ?><br>
							<b><?php echo $news['name'] ?></b><br>
						</p>
						<?php echo $news['content'] ?>
						<?php if (!empty($news['file'])) { ?>
							<a class="target_link" href="<?php echo base_url('files/news/' . $news['file']) ?>" target="_blank">
								<i class="fal fa-file-pdf"></i> <span>DOWNLOAD FILE</span>
							</a>
						<?php } ?>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
<style type="text/css">
	.description .side-right table td {
		padding: 10px !important;
	}
</style>
<script>
	$("a").each(function() {
		id = $(this).text();
		var firstChar = id.charAt(0);
		var lastChar = id.substr(id.length - 1); // => "1"

		if (firstChar == "[" && lastChar == "]") { //is footnote
			$(this).html("<sup>" + id + "</sup>");
		}

	});
</script>