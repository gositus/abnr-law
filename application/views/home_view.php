<style type="text/css">
	.flex-direction-nav a:before {
		display: none;
	}

	.flex-direction-nav .flex-next,
	.flex-direction-nav .flex-prev {
		font-size: 0;
	}

	.flex-control-paging li a {
		display: inline-block;
		width: 22px;
		height: 22px;
		background: url(https://www.abnrlaw.com/css/themes/default/bullets.png) no-repeat;
		text-indent: -9999px;
		border: 0;
		margin: -2px -4px;
		color: #7d8658;
		box-shadow: none;
		transition: all .3s ease-in-out;
		-webkit-transition: all .3s ease-in-out;
		-o-transition: all .3s ease-in-out;
	}

	.flex-control-paging li a.flex-active {
		background: url(https://www.abnrlaw.com/css/themes/default/bullets.png) no-repeat;
		background-position: 0 -22px;
	}
</style>
<div class="main-content">
	<div class="container overflow-hidden">
		<div class="row justify-content-center align-items-center">

			<div class="col-md-12">
				<div class="slide-wrapper">
					<div class="flexslider border-none" id="slider">
						<ul class="slides">
							<?php $no = 1;
							foreach ($banner as $key => $value) { ?>
								<li id="slide<?php echo ($no) ?>">
									<?php
									if (pathinfo($value['image'], PATHINFO_EXTENSION) == 'mp4') { ?>
										<a class="fancybox fancybox.iframe" data-type="swf" href="<?php echo (!empty($value['url'])) ? $value['url'] : '#'; ?>" title="<?php echo $value['name'] ?>">
											<video muted autoplay id="video1" src="<?php echo base_url('lib/images/banner/' . $value['image']) ?>" style="width:100%"></video>
										</a>
									<?php } else { ?>
										<a href="<?php echo (!empty($value['url'])) ? $value['url'] : 'javascript:;'; ?>" title="<?php echo $value['name'] ?>">
											<img src="<?php echo base_url('lib/images/banner/' . $value['image']) ?>" alt="<?php echo $value['name'] ?>" />
										</a>
									<?php } ?>

								</li>
							<?php $no++;
							} ?>
							<?php foreach ($news as $key => $value) { ?>
								<?php if (!empty($value['banner'])) { ?>
									<li id="slide<?php echo ($no) ?>">
										<a href="<?php echo base_url('news/' . $value['seo_url']) ?>" title="<?php echo $value['name'] ?>">
											<img src="<?php echo base_url('lib/images/news/' . $value['banner']) ?>" alt="<?php echo $value['name'] ?>" />
										</a>
									</li>
								<?php } ?>
							<?php $no++;
							} ?>



						</ul>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="achievements-index home mt-0 d-none">
					<ul class="text-center">
						<li>
							<img src="<?php echo base_url('lib/images/badge/Indonesia_Law_Awards_2024_Badge_WINNER.png') ?>" alt="Indonesia Law Awards 2024 Badge_WINNER">
							<p class="mt-3">Indonesia Law Firm of the Year</p>
						</li>
						<li>
							<img src="<?php echo base_url('lib/images/badge/ABLJ_Law_Firm_Of_The_Year_2024.png') ?>" alt="ABLJ Law Firm Of The Year 2024">
							<p class="mt-3">Indonesia Law Firm of the Year</p>

						</li>
						<li>
							<img src="<?php echo base_url('lib/images/badge/HO_Awards_2024.png') ?>" alt="HO_Awards_2024">
							<p class="mt-3">The Best Full-Service Law Firm of the Year</p>

						</li>
						<li>
							<img src="<?php echo base_url('lib/images/badge/Badgesslawfirm-29.png') ?>" alt="Badgesslawfirm-29">
							<p class="mt-3">Most Recommended Law Firm</p>

						</li>
					</ul>
				</div>
			</div>
			<div class="col-md-3 order-md-1 d-none">
				<ul class="badges">
					<li>
						<a href="https://www.abnrlaw.com/news/abnr-named-alb-indonesia-law-firm-of-the-year-2022">
							<img src="<?php echo base_url('lib/images/badge/alb-indonesia-law-firm-of-the-year-2022.jpg') ?>" alt="ALB Indonesia Law Awards 2022">
						</a>
					</li>
					<li>
						<a href="https://www.abnrlaw.com/news/abnr-wins-twelve-awards-from-hk-based-asia-business-law-journal">
							<img src="<?php echo base_url('lib/images/badge/ABLJ-Best-overall-law-firm-2022---RGB-email-signature.jpg') ?>" alt="asia business law jurnal">
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function() {
		$('[data-fancybox]').fancybox();

		$('.fancybox').fancybox({
			maxWidth: 800,
			maxHeight: 600,
			fitToView: false,
			width: '70%',
			height: '70%',
			autoSize: false,
			closeClick: false,
			openEffect: 'none',
			closeEffect: 'none'
		}); // fancybox

		$('#video1').controls = false;
		$('.flexslider').flexslider({
			animation: "fade",
			animationLoop: true,
			video: true,
			controlNav: false,
			slideshow: true,
			slideshowSpeed: 4000,
			easing: "swing",
			start: function() {
				var active_id = $('.flex-active-slide').attr('id');
				if (active_id == "slide1") {
					video1.play();
				}
			},
			before: function() {
				$('video').each(function() {
					$(this).get(0).load();
				});
			},
			after: function() {
				var active_id = $('.flex-active-slide').attr('id');
				if (active_id == "slide1") {
					video1.play();
				}
			},
		});
	});
</script>