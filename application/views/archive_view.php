<div class="banner">
	<div class="container">
		<div class="row align-items-end no-gutters archive caption-banner">
			<div class="col-12 col-sm-12 col-md-12 col-lg-8">
				<img class="w-100 banner-left archive" src="<?php echo base_url('lib/images/page/' . $page['image']) ?>" alt="<?php echo $page['name'] ?>">
			</div>
			<div class="col-12 col-sm-12 col-md-12 col-lg-4">
				<!-- <img class="w-100 banner-right" src="<?php echo base_url('lib/images/page/') . $page['image_right'] ?>" alt="<?php echo $page['name'] ?>"> -->
				<div class="banner-right">
					<h1><?php echo str_replace(' ','<br>',$this->lang->line('news_archive'))  ?></h1>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="archive">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="archive__left">
                    <div class="col-archive">
                        <ul class="list-columns lh-min d-none d-sm-block">
                            <?php foreach ($news_year as $value) { ?>
                                <li><a href="<?php echo base_url('news-archive/' . $value) ?>"><?php echo ($year == $value) ? '<font color="#1471c1"><strong>' . $value . '</strong></font>' : $value ?></a></li>
                            <?php } ?>
                        </ul>
                        <div class="dropdown d-block d-sm-none">
                            <button class="btn btn-secondary dropdown-toggle btn-block" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <?php echo $year ?>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <?php foreach ($news_year as $value) { ?>
                                    <a class="dropdown-item" href="<?php echo base_url('news-archive/' . $value) ?>"><?php echo $value ?></a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-archive">
                        <ul class="list-columns lh-min d-none d-sm-block">
                            <?php foreach ($news_month as $value) { ?>
                                <li><a href="<?php echo base_url('news-archive/' . $year . '/' . $value) ?>"><?php echo ($month == $value) ? '<font color="#000000"><strong>' . DateTime::createFromFormat('!m', $value)->format('F') . '</strong></font>' : DateTime::createFromFormat('!m', $value)->format('F') ?></a></li>
                            <?php } ?>
                        </ul>
                        <div class="dropdown d-block d-sm-none">
                            <button class="btn btn-secondary dropdown-toggle btn-block" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <?php echo (!empty($month)) ? $month : 'All' ?>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <?php foreach ($news_month as $value) { ?>
                                    <a class="dropdown-item" href="<?php echo base_url('news-archive/' . $year . '/' . $value) ?>"><?php echo DateTime::createFromFormat('!m', $value)->format('F') ?></a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="archive__right">
                    <?php foreach ($news as $value) { ?>
                        <div class="news-item">
                            <a href="<?php echo base_url('news/' . $value['seo_url']) ?>">
                                <div class="date"><?php echo format_date($value['start']) ?></div>
                                <h4><?php echo $value['name'] ?></h4>
                            </a>
                            <div class="desc">
                                <?php echo str_replace('&nbsp;','',character_limiter(strip_tags($value['content']), 400)) ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="mid-content d-none">
	<div class="container">
		<div class="row">

			<div class="col-12 col-sm-3 col-md-4 col-lg-4 text-sm-right">
				<div class="description pr-0 pr-sm-3">
					<div class="row">
						<div class="col-sm-12 col-md-5 col-lg-5">
							<h2 class="small-title"><strong>ARCHIVES</strong></h2>
						</div>
						<div class="col-sm-12 col-md-7 col-lg-7">
							<div class="row">
								<div class="col col-md-5">
									<ul class="list-columns lh-min d-none d-sm-block">
										<?php foreach ($news_year as $value) { ?>
											<li><a href="<?php echo base_url('news/' . $value) ?>"><?php echo ($year == $value) ? '<font color="#000000"><strong>' . $value . '</strong></font>' : $value ?></a></li>
										<?php } ?>
									</ul>
									<div class="dropdown d-block d-sm-none">
										<button class="btn btn-secondary dropdown-toggle btn-block" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											<?php echo $year ?>
										</button>
										<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
											<?php foreach ($news_year as $value) { ?>
												<a class="dropdown-item" href="<?php echo base_url('news/' . $value) ?>"><?php echo $value ?></a>
											<?php } ?>
										</div>
									</div>
								</div>
								<div class="col col-md-7">
									<ul class="list-columns lh-min d-none d-sm-block">
										<?php foreach ($news_month as $value) { ?>
											<li><a href="<?php echo base_url('news/' . $year . '/' . $value) ?>"><?php echo ($month == $value) ? '<font color="#000000"><strong>' . DateTime::createFromFormat('!m', $value)->format('F') . '</strong></font>' : DateTime::createFromFormat('!m', $value)->format('F') ?></a></li>
										<?php } ?>
									</ul>
									<div class="dropdown d-block d-sm-none">
										<button class="btn btn-secondary dropdown-toggle btn-block" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											<?php echo (!empty($month)) ? $month : 'All' ?>
										</button>
										<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
											<?php foreach ($news_month as $value) { ?>
												<a class="dropdown-item" href="<?php echo base_url('news/' . $year . '/' . $value) ?>"><?php echo DateTime::createFromFormat('!m', $value)->format('F') ?></a>
											<?php } ?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
			<div class="col-12 col-sm-8 col-md-6">
				<div class="description">
					<div class="side-right">
						<?php foreach ($news as $value) { ?>
							<div class="news-item">
								<a href="<?php echo base_url('news/' . $value['seo_url']) ?>">
									<h4><b><?php echo $value['name'] ?></b></h4>
								</a>
								<span><?php echo format_date($value['start']) ?></span>
                                <?php echo str_replace('&nbsp;','',character_limiter(strip_tags($value['content']), 400)) ?>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>