<?php $coordiante = explode(';', setting_value('coordinat')); ?>
<div class="banner">
	<div class="container-lg">
		<div class="row align-items-end no-gutters caption-banner">
			<div class="col-8">
				<img class="w-100 banner-left" src="<?php echo base_url('lib/images/page/' . $page['image']) ?>" alt="<?php echo $page['name'] ?>">
			</div>
			<div class="col-4">
				<img class="w-100 banner-right" src="<?php echo base_url('lib/images/page/') . $page['image_right'] ?>" alt="<?php echo $page['name'] ?>">

			</div>
		</div>
	</div>
</div>
<div class="contact mid-content py-4">
	<div class="container-lg">
		<div class="row">
			<div class="col-sm-12 col-md-8 col-lg-8">

				<!-- 
<div style="overflow:hidden;height:600px;width:980px;">
<div id="gmap_canvas" style="height:600px;width:980px;"></div>
<a class="google-map-code" href="http://www.trivoo.net/gutscheine/sheego/" id="get-map-data">trivoo_advent</a>
</div>
-->

				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.293673040238!2d106.80570519999999!3d-6.224955500000001!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f1518f6a2b71%3A0x747663d716310b08!2sABNR+Counsellors+at+Law!5e0!3m2!1sid!2sid!4v1537951107607" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

				<form id="form_map" action="http://maps.google.com/maps" method="get" target="_blank">
					<p>Enter your starting address:</p>
					<input type="text" name="saddr" />
					<input type="hidden" name="daddr" value="Ali Budiardjo, Nugroho, Reksodiputro (ABNR), Graha CIMB Niaga, 24th Floor, Jl. Jendral Sudirman Kav 58, Senayan, Kebayoran Baru, Jakarta Selatan, DKI Jakarta, Indonesia" />
					<input type="submit" value="Get Directions" colour="blue" />
				</form>

			</div>


		</div>
	</div>
</div>

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>

<script type="text/javascript">
	function init_map() {
		var myOptions = {
			zoom: 15,
			center: new google.maps.LatLng(<?php echo $coordiante[0] ?>, <?php echo $coordiante[1] ?>),
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		map = new google.maps.Map(document.getElementById("gmap_canvas"), myOptions);
		marker = new google.maps.Marker({
			map: map,
			position: new google.maps.LatLng(<?php echo $coordiante[0] ?>, <?php echo $coordiante[1] ?>)
		});
		infowindow = new google.maps.InfoWindow({
			content: "<b>ABNR</b><br/><b>Graha CIMB Niaga, 24th Floor</b><br/>Jl. Jend. Sudirman Kav. 58<br/>Jakarta 12190"
		});
		google.maps.event.addListener(marker, "click", function() {
			nfowindow.open(map, marker);
		});
		infowindow.open(map, marker);
	}
	google.maps.event.addDomListener(window, 'load', init_map);
</script>