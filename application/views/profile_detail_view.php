<div class="banner">
    <div class="container">
        <div class="row align-items-end no-gutters profile-detail caption-banner">
            <div class="col-12 col-sm-12 col-md-12 col-lg-8">
                <img class="w-100 banner-left profile-detail" src="<?php echo (!empty($profile['image'])) ? base_url('lib/images/profile/') . $profile['image'] : base_url('lib/images/page/' . $page['image']) ?>" alt="<?php echo $profile['name'] ?>">
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-4">
                <div class="banner-name-right">
                    <?php echo $profile['name'] ?>
                    <?php if ($profile['id'] == 58) { ?>
                        <small>Assistant lawyer</small>
                    <?php } elseif($profile['id']==176){ ?>
                        <small class="text-uppercase">Associate (Foreign Legal Counsel)</small> <!-- request on 13 Mei 2024 -->
                    <?php } else { ?>
                        <small class="text-uppercase"><?php echo($this->lang->line(type_profile($profile['type']))) ?></small>
                    <?php } ?>
                </div>
                <?php /*/ ?>
				<img class="w-100 banner-right" src="<?php echo (!empty($profile['image_right'])) ? base_url('lib/images/profile/') . $profile['image_right'] : base_url('lib/images/page/' . $page['image_right']) ?>" alt="<?php echo $profile['name'] ?>">
				<?php /*/ ?>
            </div>
        </div>
    </div>
</div>

<div class="profile-detail">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="mid-content">
                    <div class="description profile-desc profile-desc--left">
                        <?php echo $profile['content'] ?>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="profile-desc profile-desc--right">

                    <?php if (!empty($profile['email'])) { ?>
                        <p>
                            <strong>EMAIL</strong><br>
                            <a href="mailto:<?php echo $profile['email'] ?>"><?php echo $profile['email'] ?></a>
                        </p>
                    <?php } ?>
                    <?php if (!empty($profile['phone'])) { ?>
                        <p>
                            <strong>PHONE</strong><br>
                            <a href="telp:<?php echo $profile['phone'] ?>"><?php echo $profile['phone'] ?></a>
                        </p>
                    <?php } ?>
                    <?php if (!empty($profile['professional_membership'])) { ?>
                        <p>
                            <strong>PROFESSIONAL MEMBERSHIP</strong><br>
                            <?php echo nl2br($profile['professional_membership']) ?>
                        </p>
                    <?php } ?>
                    <?php if (!empty($profile['experience'])) { ?>
                        <p>
                            <strong>EXPERIENCE</strong><br>
                            <?php echo nl2br($profile['experience']) ?>
                        </p>
                    <?php } ?>
                    <?php if (!empty($profile['citizenship'])) { ?>
                        <p>
                            <strong><?php echo $this->lang->line('citizenship') ?></strong><br>
                            <?php echo nl2br($profile['citizenship']) ?>
                        </p>
                    <?php } ?>
                    <?php if (!empty($profile['language'])) { ?>
                        <p>
                            <strong><?php echo $this->lang->line('language') ?></strong><br>
                            <?php echo nl2br($profile['language']) ?>
                        </p>
                    <?php } ?>
                    <?php if (!empty($profile_achievement)) { ?>
                        <p>
                            <strong><?php echo $this->lang->line('awards') ?></strong><br>
                        <ul>
                            <?php foreach ($profile_achievement as $value) { ?>
                                <li>
                                    <a href="<?php echo $value['url'] ?>" target="_blank">
                                        <img src="<?php echo base_url('lib/images/achievement/'.$value['image']) ?>" alt="<?php echo $value['name'] ?>">
                                    </a>
                                    <span><?php echo $value['name'] ?></span>
                                </li>
                            <?php } ?>
                        </ul>
                        </p>
                    <?php } ?>
                        <?php if (1) { ?>
                            <a class="download-pdf target_link" href="<?php echo base_url('profiles/pdf/' . $profile['seo_url']) ?>" target="_blank">
                                <i class="fal fa-file-pdf"></i> <span>DOWNLOAD<br> PDF PROFILE</span>
                            </a>
                        <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    window.addEventListener('load', function () {
        fleXenv.scrollTo('<?php echo $profile['seo_url'] ?>')
    })
</script>
