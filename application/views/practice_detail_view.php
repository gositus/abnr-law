<div class="banner">
	<div class="container">
		<div class="row align-items-end no-gutters caption-banner">
			<div class="col-7">
				<img class="w-100 banner-left" src="<?php echo (!empty($row['image'])) ? base_url('lib/images/practice/') . $row['image'] : base_url('lib/images/page/' . $page['image']) ?>" alt="<?php echo $row['name'] ?>">
			</div>
			<div class="col-4">
				<img class="w-100 banner-right" src="<?php echo (!empty($row['image'])) ? base_url('lib/images/practice/') . $row['image_right'] : base_url('lib/images/page/' . $page['image_right']) ?>" alt="<?php echo $row['name'] ?>">
			</div>
		</div>
	</div>
</div>
<div class="mid-content py-4">
	<div class="container">
		<div class="row">

			<div class="col-sm-4 col-md-4 col-lg-3 text-sm-right">
				<div class="description">

					<div class="dropdown d-block d-sm-none">
						<button class="btn btn-secondary dropdown-toggle btn-block" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<?php echo $row['name'] ?>
						</button>
						<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
							<?php foreach ($practice as $value) { ?>
								<li><a class="dropdown-item" href="<?php echo base_url('practice/' . $value['seo_url']) ?>"><?php echo ($row['id'] == $value['id']) ? '<font color="#000000"><strong>' . $value['name'] . '</strong></font>' : ($value['name']) ?></a></li>
							<?php } ?>

						</div>
					</div>

					<ul class="list-columns lh-min d-none d-sm-block">

						<?php foreach ($practice as $value) { ?>
							<li><a href="<?php echo base_url('practice/' . $value['seo_url']) ?>"><?php echo ($row['id'] == $value['id']) ? '<font color="#000000"><strong>' . $value['name'] . '</strong></font>' : ($value['name']) ?></a></li>
						<?php } ?>
					</ul>
				</div>
			</div>
			<div class="col-sm-8 col-md-8 col-lg-6">
				<div class="description">
					<?php echo $row['content'] ?>
				</div>
			</div>
			<div class="col-sm-12 col-md-12 col-lg-3 practice__right">
				<a class="target_link lg" href="#" data-toggle="modal" data-target="#importantlink">
					<i class="fal fa-link"></i> <span><?php echo $this->lang->line('gov_link'); ?></span>
				</a>

				<a class="target_link" href="<?php echo base_url('publication') ?>">
					<i class="fal fa-file"></i> <span><?php echo $this->lang->line('published_article'); ?></span>
				</a>
				<!-- <?php if (!empty($row['file'])) { ?>
					<a class="target_link" href="<?php echo base_url('files/practice/' . $row['file']) ?>">
						<i class="fal fa-file"></i> <span>DOWNLOAD FILE</span>
					</a>
				<?php  } ?> -->
                <?php if(!empty($practice_achievement)){ ?>
                <p class="mt-4">
                    <strong><?php echo $this->lang->line('achievements'); ?></strong><br>
                </p>
                <ul class="practice-achievement">
                    <?php foreach ($practice_achievement as $value){ ?>
                        <li>
                            <a href="<?php echo $value['url'] ?>" target="_blank">
                                <img src="<?php echo base_url('lib/images/achievement/'.$value['image']) ?>" alt="<?php echo $value['name'] ?>">
                            </a>
                            <span><?php echo $value['name'] ?></span>
                        </li>
                    <?php } ?>
                </ul>
                <?php } ?>
			</div>

		</div>
	</div>
</div>


<div class="modal fade" id="importantlink" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
	<div class="modal-dialog  modal-dialog-centered modal-dialog-scrollable">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="staticBackdropLabel"><?php echo $this->lang->line('important_link'); ?></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<img class="w-100" src="<?php echo base_url('lib/images/gositus/popuplogo.jpg') ?>">

				<div id="main_content">
					<br>
					<?php echo $gov_link['content'] ?>
				</div>
				<div class="main-foot">
				</div>
			</div>

		</div>
	</div>
</div>