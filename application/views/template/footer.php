<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="achievements">
                    <div class="row">
                        <div class="col-sm-9 col-md-9">
                            <address>
                                <strong>Ali Budiardjo, Nugroho, Reksodiputro<br>(ABNR Counsellors at Law)</strong>
                                <ul>
                                    <li>
                                        <?php echo nl2br(setting_value('address')) ?>
                                        <?php echo setting_value('city') . " " . setting_value('postcode') ?>
                                    </li>
                                    <li>
                                        <span>Phone : <?php echo setting_value('phone') ?></span>
                                        <span>Fax : <?php echo setting_value('fax') ?></a></span>
                                        <span>Email : <a href="mailto:<?php echo setting_value('email') ?>"><?php echo setting_value('email') ?></a></span>
                                    </li>
                                </ul>
                            </address>
                        </div>
                        <div class="col-sm-3 col-md-3">
                            <div class="international-affiliations">
                                <span>INTERNATIONAL AFFILIATIONS</span>
                                <?php foreach ($affiliate as $value) { ?>
                                    <a href="<?php echo (!empty($value['url'])) ? $value['url'] : 'javascript:;' ?>" target="_blank">
                                        <img src="<?php echo base_url('lib/images/achievement/' . $value['image']) ?>" title="<?php echo $value['name'] ?>">
                                    </a>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="col-sm-9 col-md-9 text-sm-right d-none">
                            <span>ACHIEVEMENTS</span>
                            <ul class="achievements-list ml-auto">
                                <?php foreach ($achievement as $value) { ?>
                                    <li>
                                        <a href="<?php echo (!empty($value['url'])) ? $value['url'] : 'javascript:;' ?>" target="_blank">
                                            <img src="<?php echo base_url('lib/images/achievement/' . $value['image']) ?>" title="<?php echo $value['name'] ?>">
                                        </a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright text-right">
            <ul>
                <li>            <small>
                <span>CONNECT WITH US</span>
            </small></li>
                <?php if (!empty(setting_value('linkedin'))) { ?>
                    <li><a target="_blank" href="https://linkedin.com/<?php echo setting_value('linkedin') ?>"><i class="fab fa-linkedin fa-fw"></i></a></li>
                <?php } ?>
                <?php if (!empty(setting_value('twitter'))) { ?>
                    <li><a target="_blank" href="https://twitter.com/<?php echo setting_value('twitter') ?>"><img src="<?php echo base_url('lib/images/square-x-twitter.svg') ?>" width="20px" height="20px" style="display: block; padding: 0; margin: 0; border: 0;"></a></li>
                <?php } ?>
            </ul>
            <small>
                <span>&copy; <?php echo date('Y') ?> ABNR ALL RIGHTS RESERVED</span>
            </small>
        </div>
    </div>
</footer>
</main>
<?php
$script = select_all_row('setting', array('category' => 'script', 'flag' => 1));
foreach ($script as $key => $value) {
    echo setting_value($value['key']) . '
	 	';
}
?>
</body>

</html>