<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="achievements">
                    <div class="row">
                        <div class="col-sm-9 col-md-9">
                            <address>
                                <strong>Ali Budiardjo, Nugroho, Reksodiputro<br>(ABNR Counsellors at Law)</strong>
                                <ul>
                                    <li>
                                        <?php echo nl2br(setting_value('address')) ?>
                                        <?php echo setting_value('city') . " " . setting_value('postcode') ?>
                                    </li>
                                    <li>
                                        <span>Phone : <?php echo setting_value('phone') ?></span>
                                        <span>Fax : <?php echo setting_value('fax') ?></a></span>
                                        <span>Email : <a href="mailto:<?php echo setting_value('email') ?>"><?php echo setting_value('email') ?></a></span>
                                    </li>
                                </ul>
                            </address>
                        </div>
                        <div class="col-sm-3 col-md-3">
                            <div class="international-affiliations">
                                <span>INTERNATIONAL AFFILIATIONS</span>
                                <?php foreach ($affiliate as $value) { ?>
                                    <a href="<?php echo (!empty($value['url'])) ? $value['url'] : 'javascript:;' ?>" target="_blank">
                                        <img src="<?php echo base_url('lib/images/achievement/' . $value['image']) ?>" title="<?php echo $value['name'] ?>">
                                    </a>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="col-sm-9 col-md-9 text-sm-right d-none">
                            <span>ACHIEVEMENTS</span>
                            <ul class="achievements-list ml-auto">
                                <?php foreach ($achievement as $value) { ?>
                                    <li>
                                        <a href="<?php echo (!empty($value['url'])) ? $value['url'] : 'javascript:;' ?>" target="_blank">
                                            <img src="<?php echo base_url('lib/images/achievement/' . $value['image']) ?>" title="<?php echo $value['name'] ?>">
                                        </a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright text-right">
            <ul>
                <?php if (!empty(setting_value('linkedin'))) { ?>
                    <li><a target="_blank" href="https://linkedin.com/<?php echo setting_value('linkedin') ?>"><i class="fab fa-linkedin fa-fw"></i></a></li>
                <?php } ?>
                <?php if (!empty(setting_value('twitter'))) { ?>
                    <li><a target="_blank" href="https://twitter.com/<?php echo setting_value('twitter') ?>"><i class="fab fa-twitter fa-fw"></i></a></li>
                <?php } ?>
            </ul>
            <small>
                <span>ABNR 2008 - <?php echo date('Y') ?>.</span>
            </small>
        </div>
    </div>
</footer>
</main>
<?php
$script = select_all_row('setting', array('category' => 'script', 'flag' => 1));
foreach ($script as $key => $value) {
    echo setting_value($value['key']) . '
	 	';
}
?>
</body>

</html>