<!DOCTYPE html>
<html lang="<?php echo get_language(current_language())['attr']; ?>">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php
    $asset_version=3;
    total_visitor('page_view');
    $uri = substr($this->uri->uri_string, 1);
    $current_lang = get_language(current_language())['attr'];
    $switchlang = str_replace(base_url(), '', current_url());
    $seo_setting = get_meta($uri); //get from seo tools
    $lang_active = language()->result_array();

    if (!empty($seo_setting)) {
        $title = $seo_setting['title'];
        $description = $seo_setting['description'];
        $keyword = $seo_setting['keyword'];
    } else {
        //get from controller
        $title = (!empty($meta['title'])) ? $meta['title'] : setting_value('web_title');
        $keyword = (!empty($meta['keyword'])) ? $meta['keyword'] : setting_value("meta_keyword");
        $description = (!empty($meta['description'])) ? $meta['description'] : setting_value("meta_description");
    }
    ?>

    <!-- Gositus Web Development (PT Go Online Solusi) | www.gositus.com | fb.com/gositus | instagram.com/gositus -->
    <title><?php echo $title; ?></title>
    <meta name="author" content="Gositus, www.gositus.com"/>
    <meta name="description" content="<?php echo $description; ?>"/>
    <meta name="keywords" content="<?php echo $keyword; ?>"/>
    <meta name="theme-color" content="<?php echo setting_value("corporate_color"); ?>"/>
    <meta name="msapplication-TileColor" content="<?php echo setting_value("corporate_color"); ?>"/>
    <meta name="Language" content="<?php echo get_language(current_language())['name']; ?>"/>
    <?php
    if (getDomain() == 'lab.gosit.us') echo '<meta name="robots" content="noindex, nofollow">';
    ?>

    <link rel="canonical" href="<?php echo current_url(); ?>"/><?php echo alternate_language_link(); ?>

    <?php
    // UNTUK SEO WAJIB DI PASANG > disesuaikan dengan url artikel > $type: news, blog, announcement atau event, dll
    /* if  (!empty($this->uri->segments[1]) &&  ($this->uri->segments[1] == 'news' || $this->uri->segments[1] == 'blog') && $this->uri->rsegments[2] == 'detail') {
    echo '<meta property="og:type" content="Article" />
    ';
        echo '<meta property="article:section" content="' . $type . '" />
    ';
        echo '<meta property="article:published_time" content="' . date("Y-m-d", strtotime($news['start'])) . 'T' . date("H:m:s", strtotime($news['start'])) . '+00:00" />
    ';
        echo '<meta property="article:modified_time" content="' . date("Y-m-d", strtotime($news['date_edited'])) . 'T' . date("H:i:s", strtotime($news['date_edited'])) . '+00:00" />
    ';
        echo '<meta property="og:updated_time" content="' . date("Y-m-d", strtotime($news['date_edited'])) . 'T' . date("H:i:s", strtotime($news['date_edited'])) . '+00:00" />
    ';
        echo '<link rel="alternate" type="application/json+oembed" href="' . base_url() . $type . '/json/' . $this->uri->rsegments[3] . '" />
    ';
        echo '<link rel="alternate" type="text/xml+oembed" href="' . base_url() . $type . '/xml/' . $this->uri->rsegments[3] . '" />
    ';
    } else {
    echo '<meta property="og:type" content="website" />
'; } */ ?>
    <meta property="og:title" content="<?php echo $title; ?>"/>
    <meta property="og:description" content="<?php echo $description; ?>"/>
    <meta property="og:url" content="<?php echo current_url(); ?>"/>
    <meta property="og:site_name" content="<?php echo setting_value("site_name"); ?>"/>
    <meta property="og:locale" content="<?php echo get_language(current_language())['attr']; ?>"/>
    <meta property="og:image" content="<?php echo (!empty($meta['image'])) ? $meta['image'] : base_url() . setting_value('logo_cache'); ?>"/>
    <meta property="og:image:secure_url" content="<?php echo (!empty($meta['image'])) ? $meta['image'] : base_url() . setting_value('logo_cache'); ?>"/>

    <link rel="manifest" href="<?php echo base_url('lib/favicon/site.webmanifest'); ?>"/>
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('lib/favicon/') . setting_value('favicon16') ?>"/>
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url('lib/favicon/') . setting_value('favicon32') ?>"/>
    <link rel="icon" type="image/png" sizes="192x192" href="<?php echo base_url('lib/favicon/') . setting_value('favicon192') ?>"/>
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url("lib/favicon/") . setting_value('faviconico') ?>"/>
    <link rel="mask-icon" href="<?php echo base_url("lib/favicon/") . setting_value('faviconsvg') ?>" color="<?php echo setting_value('corporate_color'); ?>"/>
    <link rel="apple-touch-icon" type="image/png" sizes="180x180" href="<?php echo base_url('lib/favicon/') . setting_value('favicon180') ?>"/>
    <link rel="apple-touch-icon" type="image/png" sizes="512x512" href="<?php echo base_url('lib/favicon/') . setting_value('favicon512') ?>"/>

    <link rel="dns-prefetch" href="//s7.addthis.com"/>
    <?php if (!empty(setting_value('live_chat'))) { ?>
        <link rel="dns-prefetch" href="//static.chatra.io"/>
        <link rel="dns-prefetch" href="//call.chatra.io"/>
    <?php } ?>
    <?php if (!empty(setting_value('google_analytic'))) { ?>
        <link rel="dns-prefetch" href="//www.googletagmanager.com"/>
    <?php } ?>


    <link rel="stylesheet" type="text/css" href="<?php echo base_url('lib/css/bootstrap.min.css?v='.$asset_version); ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('lib/css/fontawesome/fontawesome.min.css?v='.$asset_version); ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('lib/css/fontawesome/brands.min.css?v='.$asset_version); ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('lib/css/fontawesome/regular.min.css?v='.$asset_version); ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('lib/css/fontawesome/light.min.css?v='.$asset_version); ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('lib/css/animate.css?v='.$asset_version); ?>"/>

    <?php foreach ($css as $style) echo '<link rel="stylesheet" type="text/css" href="', base_url(), 'lib/plugins/', $style, '.css?v='.$asset_version.'" media="print" onload="this.media=\'all\'" />'; ?>

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('lib/css/front.css?v='.$asset_version), ''; ?>"/>

    <script type="text/javascript">
        var base_url = '<?php echo base_url(); ?>';
    </script>
    <script type="text/javascript" src="<?php echo base_url('lib/js/jquery-3.5.1.min.js?v='.$asset_version); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('lib/js/popper.min.js?v='.$asset_version); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('lib/js/bootstrap.min.js?v='.$asset_version); ?>"></script>


    <?php foreach ($js as $script) echo '<script type="text/javascript" src="', base_url(), 'lib/plugins/', $script, '.js?='.$asset_version.'"></script>';

    // delete_coo_kie();
    ?>
    <script type="text/javascript" src="<?php echo base_url('lib/js/jquery.run.js?v='.$asset_version); ?>"></script>

</head>

<body>
<main id="contain-wrapper">

    <header>
        <div class="container">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="<?php echo base_url() ?>">
                    <img src="<?php echo base_url('lib/images/logo/' . setting_value('logo')) ?>" alt="<?php echo setting_value('company_name') ?>">
                </a>
                <div class="form-nav">
                    <ul class="d-block d-lg-none language">
                        <?php foreach ($lang_active as $key => $value) { ?>
                            <li class="<?php echo ($current_lang == $value['attr']) ? 'active' : ''; ?>"><a href="<?php echo base_url($value['attr'] . '/' . $switchlang) ?>"><img src="<?php echo base_url('lib/images/gositus/' . $value['attr'] . '.jpg') ?>"></a></li>
                        <?php } ?>
                    </ul>
                    <form class="form-inline ml-auto" action="<?php echo base_url('search') ?>" method="POST">
                        <input class="form-control search-regex" name="text" type="search" placeholder="<?php echo strtoupper($this->lang->line('search')) ?>" aria-label="Search">
                        <button class="btn" type="submit"><i class="fal fa-search fa-fw"></i></button>
                    </form>
                    <button class="menu-mobile form-inline">
                        <div class="burger-menu">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </button>
                </div>
            </nav>
            <div class="navigation">

                <ul class="nav justify-content-center text-uppercase">
                    <li class="nav-item cta-dropdown-menu-abnr abnr d-none d-lg-flex">
                        <a class="nav-link hover-link text-uppercase <?php echo ($active == 'abnr') ? 'active' : ''; ?>" href="<?php echo base_url('abnr') ?>"><?php echo $this->lang->line('abnr'); ?></a>
                        <ul class="hover-dropdown-menu dropdown-nav" style="display: none;">
                            <li class="nav-item">
                                <a class="nav-link text-uppercase" href="<?php echo base_url('indonesia-overview') ?>"><?php echo $this->lang->line('abnr_indonesia'); ?></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-uppercase <?php echo ($active == 'global') ? 'active' : ''; ?>" href="<?php echo base_url('global-reach') ?>"><?php echo $this->lang->line('global'); ?></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-uppercase" href="<?php echo base_url('abnr-foundation') ?>"><?php echo $this->lang->line('abnr_foundation'); ?></a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item d-lg-none">
                        <a class="nav-link text-uppercase <?php echo ($active == 'abnr') ? 'active' : ''; ?>" href="<?php echo base_url('abnr') ?>"><?php echo $this->lang->line('abnr'); ?></a>
                        <ul class="dropdown-nav nav">
                            <li class="nav-item">
                                <a class="nav-link text-uppercase" href="<?php echo base_url('indonesia-overview') ?>"><?php echo $this->lang->line('abnr_indonesia'); ?></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-uppercase <?php echo ($active == 'global') ? 'active' : ''; ?>" href="<?php echo base_url('global-reach') ?>"><?php echo $this->lang->line('global'); ?></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-uppercase" href="<?php echo base_url('abnr-foundation') ?>"><?php echo $this->lang->line('abnr_foundation'); ?></a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-uppercase <?php echo ($active == 'practice') ? 'active' : ''; ?>" href="<?php echo base_url('practice') ?>"><?php echo $this->lang->line('practice_area'); ?></a>
                    </li>
                    <li class="nav-item cta-dropdown-menu-profile profile d-none d-lg-flex">
                        <a class="nav-link hover-link text-uppercase <?php echo ($active == 'profile') ? 'active' : ''; ?>" href="<?php echo base_url('profiles') ?>"><?php echo $this->lang->line('profiles'); ?></a>
                        <ul class="hover-dropdown-menu dropdown-nav" style="display: none;">
                            <li class="nav-item">
                                <a class="nav-link text-uppercase" href="<?php echo base_url('profiles/partners') ?>">partners</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-uppercase" href="<?php echo base_url('profiles/foreign-counsel') ?>">foreign counsel</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-uppercase" href="<?php echo base_url('profiles/counsel') ?>">counsel</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-uppercase" href="<?php echo base_url('profiles/senior-associates') ?>">senior associates</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-uppercase" href="<?php echo base_url('profiles/associates') ?>">associates</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link text-uppercase" href="<?php echo base_url('profiles/of-counsel') ?>">of counsel</a>
                            </li>
<!--                            <li class="nav-item">-->
<!--                                <a class="nav-link text-uppercase" href="--><?php //echo base_url('profiles/assistant-law') ?><!--">assistant lawyers / trainee associates</a>-->
<!--                            </li>-->
                        </ul>
                    </li>
                    <li class="nav-item d-lg-none">
                        <a class="nav-link text-uppercase <?php echo ($active == 'profile') ? 'active' : ''; ?>" href="<?php echo base_url('profiles') ?>"><?php echo $this->lang->line('profiles'); ?></a>
                        <ul class="dropdown-nav nav">
                            <li class="nav-item">
                                <a class="nav-link text-uppercase" href="<?php echo base_url('profiles/partners') ?>">partners</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-uppercase" href="<?php echo base_url('profiles/foreign-counsel') ?>">foreign counsel</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-uppercase" href="<?php echo base_url('profiles/counsel') ?>">counsel</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-uppercase" href="<?php echo base_url('profiles/senior-associates') ?>">senior associates</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-uppercase" href="<?php echo base_url('profiles/associates') ?>">associates</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link text-uppercase" href="<?php echo base_url('profiles/of-counsel') ?>">of counsel</a>
                            </li>
<!--                            <li class="nav-item">-->
<!--                                <a class="nav-link text-uppercase" href="--><?php //echo base_url('profiles/assistant-law') ?><!--">assistant lawyers / trainee associates</a>-->
<!--                            </li>-->
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-uppercase <?php echo ($active == 'achievements') ? 'active' : ''; ?>" href="<?php echo base_url('achievements') ?>"><?php echo $this->lang->line('achievements'); ?></a>
                    </li>
                    <li class="nav-item cta-dropdown-menu-knowledge knowledge d-none d-lg-flex">
                        <a class="nav-link hover-link text-uppercase <?php echo ($active == 'news' || $active == 'publication') ? 'active' : ''; ?>" href="<?php echo base_url('knowledge') ?>"><?php echo $this->lang->line('knowledge_center') ?></a>
                        <ul class="hover-dropdown-menu dropdown-nav" style="display: none;">
                            <li class="nav-item">
                                <a class="nav-link text-uppercase <?php echo ($active == 'news') ? 'active' : ''; ?>" href="<?php echo base_url('news') ?>"><?php echo $this->lang->line('news'); ?></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-uppercase <?php echo ($active == 'publication') ? 'active' : ''; ?>" href="<?php echo base_url('publication') ?>"><?php echo $this->lang->line('publication'); ?></a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item d-lg-none">
                        <a class="nav-link text-uppercase <?php echo ($active == 'knowledge') ? 'active' : ''; ?>" href="<?php echo base_url('knowledge') ?>"><?php echo $this->lang->line('knowledge_center') ?></a>
                        <ul class="dropdown-nav nav">
                            <li class="nav-item">
                                <a class="nav-link text-uppercase <?php echo ($active == 'news') ? 'active' : ''; ?>" href="<?php echo base_url('news') ?>"><?php echo $this->lang->line('news'); ?></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-uppercase <?php echo ($active == 'publication') ? 'active' : ''; ?>" href="<?php echo base_url('publication') ?>"><?php echo $this->lang->line('publication'); ?></a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-uppercase <?php echo ($active == 'contact') ? 'active' : ''; ?>" href="<?php echo base_url('contact') ?>"><?php echo $this->lang->line('contact'); ?></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-uppercase <?php echo ($active == 'career') ? 'active' : ''; ?>" href="<?php echo base_url('career') ?>"><?php echo $this->lang->line('join_us'); ?></a>
                    </li>
                </ul>

                <ul class="language d-none d-lg-block">
                    <?php foreach ($lang_active as $key => $value) { ?>
                        <li class="<?php echo ($current_lang == $value['attr']) ? 'active' : ''; ?>"><a href="<?php echo base_url($value['attr'] . '/' . $switchlang) ?>"><img src="<?php echo base_url('lib/images/gositus/' . $value['attr'] . '.jpg') ?>"></a></li>
                    <?php } ?>
                </ul>
            </div>
    </header>
		
	