<div class="banner">
	<div class="container">
        <div class="row align-items-end no-gutters caption-banner archive">
            <div class="col-12 col-sm-12 col-md-12 col-lg-8">
                <img class="w-100 banner-left profile-index" src="<?php echo base_url('lib/images/page/' . $page['image']) ?>" alt="<?php echo $page['name'] ?>">
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-4">
                <!-- <img class="w-100 banner-right" src="<?php echo base_url('lib/images/page/' . $page['image_right']) ?>" alt="<?php echo $page['name'] ?>"> -->
                <div class="banner-right">
                    <h1><?php echo $page['heading'] ?></h1>
                </div>
            </div>
        </div>
	</div>
</div>
<div class="mid-content py-4">
	<div class="container">
		<div class="row">

			<div class="col-md-12">
				<div class="description">
					<h2 class="small-title"><strong>ABNR principal areas of practice include:</strong></h2>
					<ul class="list-columns two-columns p-0" >
						<?php foreach ($practice as $value) { ?>
							<li><a href="<?php echo base_url('practice/' . $value['seo_url']) ?>"><?php echo strtoupper($value['name']) ?></a></li>
						<?php } ?>
					</ul>
				</div>
			</div>

		</div>
	</div>
</div>