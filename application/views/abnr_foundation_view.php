<div class="banner">
	<div class="container">
        <div class="row align-items-end no-gutters caption-banner archive">
            <div class="col-12 col-sm-12 col-md-12 col-lg-8">
                <img class="w-100 banner-left profile-index" src="<?php echo base_url('lib/images/page/' . $page['image']) ?>" alt="<?php echo $page['name'] ?>">
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-4">
                <!-- <img class="w-100 banner-right" src="<?php echo base_url('lib/images/page/' . $page['image_right']) ?>" alt="<?php echo $page['name'] ?>"> -->
                <div class="banner-right">
                    <h1><?php echo $page['heading'] ?></h1>
                </div>
            </div>
        </div>
	</div>
</div>
<div class="mid-content py-4">
	<div class="container">
		<div class="row">
			<div class="col-sm-4 col-md-4 col-lg-4 text-sm-right mb-4 mb-sm-0">
				<h1 class="page-title"><?php echo $page['name'] ?></h1>
				<ul class="nav flex-column">

					<li class="nav-item">
						<a class="nav-link text-uppercase" href="<?php echo base_url('abnr') ?>"><?php echo $this->lang->line('abnr'); ?></a>
					</li>
					<li class="nav-item">
						<a class="nav-link text-uppercase" href="<?php echo base_url('global-reach') ?>"><?php echo $this->lang->line('global'); ?></a>
					</li>
					<li class="nav-item">
						<a class="nav-link text-uppercase" href="<?php echo base_url('indonesia-overview') ?>"><?php echo $this->lang->line('abnr_indonesia'); ?></a>
					</li>

				</ul>
			</div>
			<div class="col-sm-8 col-md-8 col-lg-6">
				<div class="description">
					<?php echo $page['content'] ?>
				</div>
			</div>
			<div class="col-sm-4 col-md-4 col-lg-2">
				<a class="target_link ml-sm-auto" href="http://www.yayasanabnr.org" target="_blank">
					<i class="fal fa-file"></i> <span><?php echo $this->lang->line('to_be_involved'); ?></span>
				</a>
			</div>
		</div>
	</div>
</div>