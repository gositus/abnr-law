<div class="banner">
	<div class="container-lg">
		<div class="row align-items-end no-gutters caption-banner">
			<div class="col-8">
				<img class="w-100 banner-left" src="<?php echo base_url('lib/images/page/' . $page['image']) ?>" alt="<?php echo $page['name'] ?>">
			</div>
			<div class="col-4">
				<img class="w-100 banner-right" src="<?php echo base_url('lib/images/page/') . $page['image_right'] ?>" alt="<?php echo $page['name'] ?>">

			</div>
		</div>
	</div>
</div>
<div class="mid-content py-4">
	<div class="container-lg">
		

		<div class="row">
			<div class="col-md-4 col-lg-4 text-sm-right">
				<div class="description">
					<ul class="list-columns d-none d-md-block">
						<?php foreach ($list as $key => $value) { ?>
						<li>
							<a href="<?php echo base_url('career/'.$value['seo_url']) ?>"><?php echo strtoupper($value['name']) ?></a>
						</li>
						<?php } ?>
					</ul>

					<div class="dropdown d-block d-md-none">
						<button class="btn btn-secondary dropdown-toggle btn-block" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<?php echo strtoupper($career['name']) ?>
						</button>
						<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
							<?php foreach ($list as $key => $value) { ?>
							<li><a class="dropdown-item" href="<?php echo base_url('profiles/' . $value['seo_url']) ?>"><?php echo ($career['id'] == $value['id']) ? '<font color="#000000"><strong>' . strtoupper($value['name']) . '</strong></font>' : strtoupper($value['name']) ?></a></li>
							<?php } ?>
							
						</div>
					</div>

				</div>
			</div>
			<div class="col-md-6 col-lg-6">
				<div class="description">

				<h4 class="desc-title mb-2"><?php echo strtoupper($career['name']) ?></h4>
				<p class="mb-2">
			        <b>Closing Date: <?php echo date('M d, Y', strtotime($career['end'])) ?></b>        
			    </p> 

				<?php echo $career['description'] ?>
				<br>
				<?php echo $career['requirement'] ?>

				<p>&nbsp;</p>
				<p>If you are an interested and qualified candidate, you can click on Apply button bellow.</p>
				<p>&nbsp;</p>
			
		    
				<p><a href="#" class="d-none"><span>apply</span></a></p>
				<p>&nbsp;</p>
	
				</div>
			</div>
		</div>
	</div>
</div>