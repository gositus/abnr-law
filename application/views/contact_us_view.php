<div class="banner">
	<div class="container">
        <div class="row align-items-end no-gutters caption-banner archive">
            <div class="col-12 col-sm-12 col-md-12 col-lg-8">
                <img class="w-100 banner-left profile-index" src="<?php echo base_url('lib/images/page/' . $page['image']) ?>" alt="<?php echo $page['name'] ?>">
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-4">
                <!-- <img class="w-100 banner-right" src="<?php echo base_url('lib/images/page/' . $page['image_right']) ?>" alt="<?php echo $page['name'] ?>"> -->
                <div class="banner-right">
                    <h1><?php echo $page['heading'] ?></h1>
                </div>
            </div>
        </div>
	</div>
</div>
<div class="maps py-4">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-8 col-lg-8">
                <iframe src="https://maps.google.com/maps?width=100%25&amp;height=600&amp;hl=en&amp;q=ABNR%20Counselors%20at%20Law+(ABNR%20Counselors%20at%20Law)&amp;t=&amp;z=19&amp;ie=UTF8&amp;iwloc=B&amp;output=embed" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</div>
