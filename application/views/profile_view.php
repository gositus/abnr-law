<div class="banner">
    <div class="container">
        <div class="row align-items-end no-gutters profile-index caption-banner">
            <div class="col-12 col-sm-12 col-md-12 col-lg-8">
                <img class="w-100 banner-left profile-index" src="<?php echo base_url('lib/images/page/' . $page['image']) ?>" alt="<?php echo $page['name'] ?>">
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-4">
                <!-- <img class="w-100 banner-right" src="<?php echo base_url('lib/images/page/' . $page['image_right']) ?>" alt="<?php echo $page['name'] ?>"> -->
                <div class="banner-right">
                    <h1><?php echo $page['heading'] ?></h1>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="profile-index">
    <div class="container">
        <div class="row justify-content-center">

            <div class="col-12 col-sm-12">
                <div class="profile-filter">
                    <ul class="nav justify-content-center isotope-toolbar">
                        <li class="nav-item">
                            <a class="nav-link <?php echo (empty($type)) ? 'active' : '' ?> text-uppercase" data-type="*" name="isotope-filter" href="<?php echo base_url('profiles') ?>"><span>All</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link <?php echo ($type == 1) ? 'active' : '' ?> text-uppercase" data-type="partners" name="isotope-filter" href="<?php echo base_url('profiles/partners') ?>"><span>partners</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link <?php echo ($type == 3) ? 'active' : '' ?> text-uppercase" data-type="foreign_counsel" name="isotope-filter" href="<?php echo base_url('profiles/foreign-counsel') ?>"><span>foreign counsel</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link <?php echo ($type == 6) ? 'active' : '' ?> text-uppercase" data-type="counsel" name="isotope-filter" href="<?php echo base_url('profiles/counsel') ?>"><span>counsel</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link <?php echo ($type == 7) ? 'active' : '' ?> text-uppercase" data-type="senior_associates" name="isotope-filter" href="<?php echo base_url('profiles/senior-associates') ?>"><span>senior associates</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link <?php echo ($type == 2) ? 'active' : '' ?> text-uppercase" data-type="associates" name="isotope-filter" href="<?php echo base_url('profiles/associates') ?>"><span>associates</span></a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link <?php echo ($type == 5) ? 'active' : '' ?> text-uppercase" data-type="of_counsel" name="isotope-filter" href="<?php echo base_url('profiles/of-counsel') ?>"><span>of counsel</span></a>
                        </li>
                        <!--                        <li class="nav-item">-->
                        <!--                            <a class="nav-link --><?php //echo ($type==4)?'active':'' ?><!-- text-uppercase" data-type="assistant_law" name="isotope-filter" href="--><?php //echo base_url('profiles/assistant-law') ?><!--"><span>trainee associates</span></a>-->
                        <!--                        </li>-->
                    </ul>
                </div>
                <div class="profile-lists isotope-box mb-4">
                    <?php foreach ($profile_list as $value) { ?>
                        <div class="col-list" data-type="<?php echo type_profile($value['type']) ?>">
                            <div class="isotope-item">
                                <figure>
                                    <?php if (1) { ?>
                                        <a href="<?php echo base_url('profiles/' . $value['seo_url']) ?>">
                                            <img class="w-100" src="<?php echo (!empty($value['profile_image'])) ? base_url('lib/images/profile/' . $value['profile_image']) : base_url('lib/images/page/profiles_placeholder.png') ?>" alt="<?php echo($value['name']) ?>">
                                        </a>
                                    <?php } else { ?>
                                        <div>
                                            <img class="w-100" src="<?php echo (!empty($value['profile_image'])) ? base_url('lib/images/profile/' . $value['profile_image']) : base_url('lib/images/page/profiles_placeholder.png') ?>" alt="<?php echo($value['name']) ?>">
                                        </div>
                                    <?php } ?>
                                </figure>
                                <figcaption>
                                    <div class="figbody">
                                        <?php if (1) { ?>
                                            <a href="<?php echo base_url('profiles/' . $value['seo_url']) ?>"><?php echo($value['name']) ?></a>
                                        <?php } else { ?>
                                            <div class="title"> <?php echo($value['name']) ?></div>
                                        <?php } ?>
                                        <?php if ($value['id']=='176') { ?>
                                        <h5>Associate (Foreign Legal Counsel)</h5> <!-- request on 13 Mei 2024 -->
                                        <?php } else { ?>
                                        <h5><?php echo($this->lang->line(type_profile($value['type']))) ?></h5>
                                        <?php } ?>
                                    </div>
                                    <div class="figfooter">
                                        <?php if (!empty($value['email'])) { ?>
                                            <a href="mailto:<?php echo $value['email'] ?>"><?php echo $value['email'] ?></a>
                                        <?php } ?>
                                        <?php if (!empty($value['phone'])) { ?>
                                            <a href="telp:<?php echo $value['phone'] ?>"><?php echo $value['phone'] ?></a>
                                        <?php } ?>
                                    </div>
                                </figcaption>
                            </div>
                        </div>
                    <?php } ?>

                </div>
                <nav class="pagination pagination-cs pagination-sm justify-content-center">
                    <?php echo $pagination ?>
                </nav>
                <!--                <nav aria-label="...">-->
                <!--                    <ul class="pagination pagination-cs pagination-sm justify-content-center">-->
                <!--                        <li class="page-item active" aria-current="page">-->
                <!--                            <span class="page-link">1</span>-->
                <!--                        </li>-->
                <!--                        <li class="page-item"><a class="page-link" href="#"><span>2</span></a></li>-->
                <!--                        <li class="page-item"><a class="page-link" href="#"><span>3</span></a></li>-->
                <!--                        <li class="page-item"><a class="page-link" href="#"><span>4</span></a></li>-->
                <!--                        <li class="page-item"><a class="page-link" href="#"><span>5</span></a></li>-->
                <!--                    </ul>-->
                <!--                </nav>-->
            </div>
        </div>
    </div>
</div>
