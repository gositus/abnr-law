<?php
header('Content-Type: text/xml');
echo '<?xml version="1.0" encoding="UTF-8"?>';
echo '<urlset
      xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
            http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">';
?>

<url>
  <loc><?php echo base_url(); ?></loc>
  <changefreq>weekly</changefreq>
  <priority>1.00</priority>
</url>
<?php
$static = array(
  'news', 'abnr', 'indonesia-overview', 'abnr-foundation', 'practice', 'profiles',
  'global-reach', 'achievements', 'publication', 'contact', 'career',
  'profiles/partners', 'profiles/foreign-counsel', 'profiles/counsel', 'profiles/of-counsel',
  'profiles/associates', 'profiles/assistant-law',
); // your static url

for ($i = 0; $i < count($static); $i++) {
  echo '<url>
          <loc>' . base_url($static[$i]) . '</loc>
          <changefreq>monthly</changefreq>
          <priority>0.5</priority>
        </url>';
}
// dynamic url

foreach ($news as $item) :
  echo '<url>';
  echo '<loc>' . base_url() . 'news/' . strtolower($item['seo_url']) . '</loc>';
  echo '<changefreq>monthly</changefreq>';
  echo '<priority>0.8</priority>';
  echo '</url>';
endforeach;

foreach ($profile as $item) :
  echo '<url>';
  echo '<loc>' . base_url() . 'profiles/' . strtolower($item['seo_url']) . '</loc>';
  echo '<changefreq>monthly</changefreq>';
  echo '<priority>0.8</priority>';
  echo '</url>';
endforeach;

foreach ($practice as $item) :
  echo '<url>';
  echo '<loc>' . base_url() . 'practice/' . strtolower($item['seo_url']) . '</loc>';
  echo '<changefreq>monthly</changefreq>';
  echo '<priority>0.8</priority>';
  echo '</url>';
endforeach;
?>

<?php echo '</urlset>';
?>